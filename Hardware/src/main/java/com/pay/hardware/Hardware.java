package com.pay.hardware;

import com.google.common.primitives.Ints;
import com.google.common.primitives.UnsignedInteger;
import com.pay.base.*;
import com.pay.base.config.xml.XmlDocument;
import com.pay.base.config.xml.XmlElement;
import com.pay.base.dataengine.enums.ConfigType;
import com.pay.base.devicestate.DeviceState;
import com.pay.base.devicestate.PrinterDeviceState;
import com.pay.base.event.EventListener;
import com.pay.base.event.EventManager;
import com.pay.base.event.types.CashReceivedEvent;
import com.pay.base.event.types.DeviceChangedEvent;
import com.pay.base.event.types.SoftwareActionToDeviceEvent;
import com.pay.base.vending.Vending;
import com.pay.base.watchdog.WatchDog;
import com.pay.helper.enums.LogType;
import org.apache.commons.lang3.mutable.MutableInt;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;


import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by dito on 4/5/2017.
 */
public class Hardware implements IHardware {
    private static class PayboxDeviceTypeInfo {

        private String deviceName = "";

        public final String getDeviceName() {
            return this.deviceName;
        }

        public final void setDeviceName(String value) {
            this.deviceName = value;
        }

        private IHardwareBase device;

        public final IHardwareBase getDevice() {
            return this.device;
        }

        public final void setDevice(IHardwareBase value) {
            this.device = value;
        }

        private boolean eventsSubscribed;

        public final boolean getEventsSubscribed() {
            return this.eventsSubscribed;
        }

        public final void setEventsSubscribed(boolean value) {
            this.eventsSubscribed = value;
        }

        public PayboxDeviceTypeInfo(String deviceName, IHardwareBase device, boolean eventsSubscribed) {
            this.deviceName = deviceName;
            this.device = device;
            this.eventsSubscribed = eventsSubscribed;
        }
    }

    private IServerContainer container;
    private XmlDocument hardwareConfig;
    private HashMap<String, PayboxDeviceTypeInfo> payboxDeviceTypeInfoDictionary = new HashMap<String, PayboxDeviceTypeInfo>(); // <Key: Paybox Device Type Name, Value: Paybox Device Type Info>
    public final String payboxName;

    public Hardware(IServerContainer pContainer) {
        this.payboxName = pContainer.getGeneralDataEngine().getPayboxName();
        try {
            this.container = pContainer;
            this.hardwareConfig = this.container.getConfigurator().GetConfig("Hardware", ConfigType.GeneralConfig);


        } catch (Exception ex) {
            this.container.getLogger().Log("Hardware", "Hardware(): " + ex.getMessage(), LogType.Error);
            this.container.getLogger().LogException(ex);
        }
    }


    public final boolean Initialize() {
        boolean result = true;

        try {
            for (String payboxDeviceTypeName : payboxDeviceTypeInfoDictionary.keySet()) {
                if (!this.Initialize(payboxDeviceTypeName)) {
                    result = false;
                }
            }
        } catch (Exception ex) {
            this.container.getLogger().Log("Hardware", "Initialize(): " + ex.getMessage(), LogType.Error);
            this.container.getLogger().LogException(ex);
            result = false;
        }

        return result;
    }


    public final boolean Deinitialize() {
        boolean result = true;

        try {
            for (String payboxDeviceTypeName : payboxDeviceTypeInfoDictionary.keySet()) {
                if (!this.Deinitialize(payboxDeviceTypeName)) {
                    result = false;
                }
            }
        } catch (Exception ex) {
            this.container.getLogger().Log("Hardware", "Deinitialize(): " + ex.getMessage(), LogType.Error);
            this.container.getLogger().LogException(ex);
            result = false;
        }

        return result;
    }

    public final boolean Reset() {
        boolean result = true;

        try {
            for (String payboxDeviceTypeName : payboxDeviceTypeInfoDictionary.keySet()) {
                if (!this.Reset(payboxDeviceTypeName)) {
                    result = false;
                }
            }
        } catch (RuntimeException ex) {
            this.container.getLogger().Log("Hardware", "Reset(): " + ex.getMessage(), LogType.Error);
            this.container.getLogger().LogException(ex);
            result = false;
        }

        return result;
    }

    public final DeviceState GetState(boolean force) {
        throw new NotImplementedException();
    }


    public final void SoftwareActionToDeviceHandler(SoftwareActionToDeviceEvent event) {
        if (payboxDeviceTypeInfoDictionary.containsKey(event.getValue().getPayboxDeviceTypeName())) {
            IHardwareBase device = payboxDeviceTypeInfoDictionary.get(event.getValue().getPayboxDeviceTypeName()).getDevice();

            if (device != null) {
                this.container.getLogger().Log("Hardware", String.format("SoftwareActionToDeviceHandler(): %1$s Action Event Occured For Device %2$s ", event.getValue().getSoftwareAction(), event.getValue().getPayboxDeviceTypeName()), LogType.Information);
                EventManager<SoftwareActionToDevice> softwareActionToDeviceEventHanler = new EventManager<>();
                softwareActionToDeviceEventHanler.registerListener(device::SoftwareActionToDeviceHandler);
                softwareActionToDeviceEventHanler.beginInvoke(event);
            } else {
                this.container.getLogger().Log("Hardware", String.format("SoftwareActionToDeviceHandler(): %1$s Device Instance Is NULL", event.getValue().getPayboxDeviceTypeName()), LogType.Warning);
            }
        } else {
            this.container.getLogger().Log("Hardware", String.format("SoftwareActionToDeviceHandler(): Unknown PayboxDeviceTypeName: %1$s", event.getValue().getPayboxDeviceTypeName()), LogType.Warning);
        }
    }

    public final HashMap<String, Integer> GetDeviceCountersDictionary() {
        throw new UnsupportedOperationException();
    }


    private EventListener<DeviceChangedEvent> stateChangedDelegate = this::OnStateChanged;
    private EventListener<CashReceivedEvent> cashReceivedDelegate = this::OnCashReceived;

    private EventManager<DeviceState> stateChangedEventHandler = new EventManager<>();
    private EventManager<CashMoney> cashReceivedEventHandler = new EventManager<>();


    public final void OnStateChanged(DeviceChangedEvent event) {
        if (stateChangedEventHandler != null) {
            stateChangedEventHandler.beginInvoke(event);
        } else {
            this.container.getLogger().Log("Hardware", "OnStateChanged() IS NULL", LogType.Warning);
        }

        this.container.getLogger().Log("Hardware", "OnStateChanged, DeviceState: " + event.getValue(), LogType.Information);
    }

    public final void OnCashReceived(CashReceivedEvent cashReceivedEvent) {
        if (cashReceivedEventHandler != null) {
            cashReceivedEventHandler.beginInvoke(cashReceivedEvent);
        } else {
            this.container.getLogger().Log("Hardware", "OnCashReceived() IS NULL", LogType.Warning);
        }
    }

    @Override
    public void addCashReceivedHandler(EventListener<CashReceivedEvent> listener) {
        cashReceivedEventHandler.registerListener(listener);
    }

    @Override
    public void removeCashReceivedHandler(EventListener<CashReceivedEvent> listener) {
        cashReceivedEventHandler.unregisterListener(listener);
    }


    @Override
    public void addStateChangedHandler(EventListener<DeviceChangedEvent> listener) {
        stateChangedEventHandler.registerListener(listener);
    }

    @Override
    public void removeStateChangedHandler(EventListener<DeviceChangedEvent> listener) {
        stateChangedEventHandler.unregisterListener(listener);

    }

    public final boolean Initialize(String payboxDeviceTypeName) {
        boolean result = false;

        try {
            if (payboxDeviceTypeInfoDictionary.containsKey(payboxDeviceTypeName)) {
                PayboxDeviceTypeInfo payboxDeviceTypeInfo = payboxDeviceTypeInfoDictionary.get(payboxDeviceTypeName);

                if (payboxDeviceTypeInfo.getDevice() != null) {
                    if (!payboxDeviceTypeInfo.getEventsSubscribed()) {
                        payboxDeviceTypeInfo.getDevice().addStateChangedHandler(stateChangedDelegate);
                        if (payboxDeviceTypeInfo.getDevice() instanceof CashIn) {
                            ((CashIn) payboxDeviceTypeInfo.getDevice()).addCashReceivedHandler(cashReceivedDelegate);
                        }
                        payboxDeviceTypeInfo.setEventsSubscribed(true);
                    }

                    IHardwareBase tempVar = payboxDeviceTypeInfo.getDevice();
                    NetworkConnection networkConnectionTemp = (NetworkConnection) ((tempVar instanceof NetworkConnection) ? tempVar : null);
                    //Set active network connection
                    if (networkConnectionTemp != null && this.networkConnectionUsagePriority == networkConnectionTemp.getUsagePriority()) {
                        ((NetworkConnection) payboxDeviceTypeInfo.getDevice()).setIsActive(true);
                    }

                    if (payboxDeviceTypeInfo.getDevice().Initialize()) {
                        this.container.getLogger().Log("Hardware", String.format("Initialize(payboxDeviceTypeName): Successfully Initialized Device: %1$s", payboxDeviceTypeInfo.getDeviceName()), LogType.Information);
                        result = true;
                    } else {
                        this.container.getLogger().Log("Hardware", String.format("Initialize(payboxDeviceTypeName): Failed To Initialize Device: %1$s", payboxDeviceTypeInfo.getDeviceName()), LogType.Warning);
                    }
                } else {
                    this.container.getLogger().Log("Hardware", String.format("Initialize(payboxDeviceTypeName): %1$s Device Instance Is NULL", payboxDeviceTypeInfo.getDeviceName()), LogType.Warning);
                }
            } else {
                this.container.getLogger().Log("Hardware", String.format("Initialize(payboxDeviceTypeName): Unknown PayboxDeviceTypeName: %1$s", payboxDeviceTypeName), LogType.Warning);
            }
        } catch (RuntimeException ex) {
            this.container.getLogger().Log("Hardware", "Initialize(payboxDeviceTypeName): " + ex.getMessage(), LogType.Error);
            this.container.getLogger().LogException(ex);
            result = false;
        }

        return result;
    }

    public final boolean Deinitialize(String payboxDeviceTypeName) {
        boolean result = false;

        try {
            if (payboxDeviceTypeInfoDictionary.containsKey(payboxDeviceTypeName)) {
                PayboxDeviceTypeInfo payboxDeviceTypeInfo = payboxDeviceTypeInfoDictionary.get(payboxDeviceTypeName);

                if (payboxDeviceTypeInfo.getDevice() != null) {
//C# TO JAVA CONVERTER TODO TASK: Java has no equivalent to C#-style event wireups:
                    payboxDeviceTypeInfo.getDevice().removeStateChangedHandler(stateChangedDelegate);
                    ;
                    if (payboxDeviceTypeInfo.getDevice() instanceof CashIn) {
//C# TO JAVA CONVERTER TODO TASK: Java has no equivalent to C#-style event wireups:
                        ((CashIn) payboxDeviceTypeInfo.getDevice()).removeCashReceivedHandler(cashReceivedDelegate);
                    }
                    payboxDeviceTypeInfo.setEventsSubscribed(false);

                    if (payboxDeviceTypeInfo.getDevice().Deinitialize()) {
                        this.container.getLogger().Log("Hardware", String.format("Deinitialize(payboxDeviceTypeName): Successfully Deinitialized Device: %1$s", payboxDeviceTypeInfo.getDeviceName()), LogType.Information);
                        result = true;
                    } else {
                        this.container.getLogger().Log("Hardware", String.format("Deinitialize(payboxDeviceTypeName): Failed To Deinitialize Device: %1$s", payboxDeviceTypeInfo.getDeviceName()), LogType.Warning);
                    }
                } else {
                    this.container.getLogger().Log("Hardware", String.format("Deinitialize(payboxDeviceTypeName): %1$s Device Instance Is NULL", payboxDeviceTypeInfo.getDeviceName()), LogType.Warning);
                }
            } else {
                this.container.getLogger().Log("Hardware", String.format("Deinitialize(payboxDeviceTypeName): Unknown PayboxDeviceTypeName: %1$s", payboxDeviceTypeName), LogType.Warning);
            }
        } catch (RuntimeException ex) {
            this.container.getLogger().Log("Hardware", "Deinitialize(payboxDeviceTypeName): " + ex.getMessage(), LogType.Error);
            this.container.getLogger().LogException(ex);
            result = false;
        }

        return result;
    }

    public final boolean Reset(String payboxDeviceTypeName) {
        boolean result = false;

        try {
            if (payboxDeviceTypeInfoDictionary.containsKey(payboxDeviceTypeName)) {
                PayboxDeviceTypeInfo payboxDeviceTypeInfo = payboxDeviceTypeInfoDictionary.get(payboxDeviceTypeName);

                if (payboxDeviceTypeInfo.getDevice() != null) {
                    if (payboxDeviceTypeInfo.getDevice().Reset()) {
                        this.container.getLogger().Log("Hardware", String.format("Reset(payboxDeviceTypeName): Successfully Reseted Device: %1$s", payboxDeviceTypeInfo.getDeviceName()), LogType.Information);
                        result = true;
                    } else {
                        this.container.getLogger().Log("Hardware", String.format("Reset(payboxDeviceTypeName): Failed To Reset Device: %1$s", payboxDeviceTypeInfo.getDeviceName()), LogType.Warning);
                    }

                    if (!payboxDeviceTypeInfo.getEventsSubscribed()) {
//C# TO JAVA CONVERTER TODO TASK: Java has no equivalent to C#-style event wireups:
                        payboxDeviceTypeInfo.getDevice().addStateChangedHandler(stateChangedDelegate);
                        if (payboxDeviceTypeInfo.getDevice() instanceof CashIn) {
//C# TO JAVA CONVERTER TODO TASK: Java has no equivalent to C#-style event wireups:
                            ((CashIn) payboxDeviceTypeInfo.getDevice()).addCashReceivedHandler(cashReceivedDelegate);
                            ;
                        }
                        payboxDeviceTypeInfo.setEventsSubscribed(true);
                    }
                } else {
                    this.container.getLogger().Log("Hardware", String.format("Reset(payboxDeviceTypeName): %1$s Device Instance Is NULL", payboxDeviceTypeInfo.getDeviceName()), LogType.Warning);
                }
            } else {
                this.container.getLogger().Log("Hardware", String.format("Reset(payboxDeviceTypeName): Unknown PayboxDeviceTypeName: %1$s", payboxDeviceTypeName), LogType.Warning);
            }
        } catch (RuntimeException ex) {
            this.container.getLogger().Log("Hardware", "Reset(payboxDeviceTypeName): " + ex.getMessage(), LogType.Error);
            this.container.getLogger().LogException(ex);
            result = false;
        }

        return result;
    }


    public final DeviceState GetState(boolean force, String payboxDeviceTypeName) {
        if (payboxDeviceTypeInfoDictionary.containsKey(payboxDeviceTypeName) && payboxDeviceTypeInfoDictionary.get(payboxDeviceTypeName).getDevice() != null) {
            return payboxDeviceTypeInfoDictionary.get(payboxDeviceTypeName).getDevice().GetState(force);
        } else {
            return null;
        }
    }

    public final DeviceState[] GetStates(boolean force) {
        DeviceState[] deviceStates = new DeviceState[payboxDeviceTypeInfoDictionary.size()];
        int index = 0;

        for (PayboxDeviceTypeInfo payboxDeviceTypeInfo : payboxDeviceTypeInfoDictionary.values()) {
            try {
                if (payboxDeviceTypeInfo.getDevice() != null) {
                    deviceStates[index++] = payboxDeviceTypeInfo.getDevice().GetState(force);
                }
            } catch (RuntimeException ex) {
                this.container.getLogger().Log("Hardware", "GetStates(): " + ex.getMessage(), LogType.Error);
                this.container.getLogger().LogException(ex);
            }
        }

        return deviceStates;
    }


    public final boolean AllowCashReceive(int... allowCashIDList) {
        boolean result = false;
        this.container.getLogger().Log("Hardware", "AllowCashReceive(params) Start...", LogType.Information);

        if (allowCashIDList.length > 0) {
            try {
                for (PayboxDeviceTypeInfo payboxDeviceTypeInfo : payboxDeviceTypeInfoDictionary.values()) {
                    if (payboxDeviceTypeInfo.getDevice() != null && payboxDeviceTypeInfo.getDevice() instanceof CashIn) {
                        if (!((CashIn) payboxDeviceTypeInfo.getDevice()).AllowCashReceive(allowCashIDList)) {
                            result = false;
                            break;
                        }
                    }
                }

                result = true;
            } catch (RuntimeException ex) {
                this.container.getLogger().Log("Hardware", "AllowCashReceive(params): " + ex.getMessage(), LogType.Error);
                this.container.getLogger().LogException(ex);
                result = false;
            }
        }

        return result;
    }

    public final boolean AllowCashReceive() {
        boolean result;
        this.container.getLogger().Log("Hardware", "AllowCashReceive() Start...", LogType.Information);

        try {
            for (PayboxDeviceTypeInfo payboxDeviceTypeInfo : payboxDeviceTypeInfoDictionary.values()) {
                if (payboxDeviceTypeInfo.getDevice() != null && payboxDeviceTypeInfo.getDevice() instanceof CashIn) {
                    if (!((CashIn) payboxDeviceTypeInfo.getDevice()).AllowCashReceive()) {
                        result = false;
                        break;
                    }
                }
            }

            result = true;
        } catch (RuntimeException ex) {
            this.container.getLogger().Log("Hardware", "AllowCashReceive(): " + ex.getMessage(), LogType.Error);
            this.container.getLogger().LogException(ex);
            result = false;
        }

        return result;
    }

    public final boolean DenyCashReceive() {
        boolean result;
        this.container.getLogger().Log("Hardware", "DenyCashReceive() Start...", LogType.Information);

        try {
            for (PayboxDeviceTypeInfo payboxDeviceTypeInfo : payboxDeviceTypeInfoDictionary.values()) {
                if (payboxDeviceTypeInfo.getDevice() != null && payboxDeviceTypeInfo.getDevice() instanceof CashIn) {
                    if (!((CashIn) payboxDeviceTypeInfo.getDevice()).DenyCashReceive()) {
                        result = false;
                        break;
                    }
                }
            }

            result = true;
        } catch (RuntimeException ex) {
            this.container.getLogger().Log("Hardware", "DenyCashReceive(): " + ex.getMessage(), LogType.Error);
            this.container.getLogger().LogException(ex);
            result = false;
        }

        return result;
    }

    public final int[] GetAllowedCashIDList() {
        List<Integer> resultCashIDList = new ArrayList<>();

        try {
            for (PayboxDeviceTypeInfo payboxDeviceTypeInfo : payboxDeviceTypeInfoDictionary.values()) {
                if (payboxDeviceTypeInfo.getDevice() != null && payboxDeviceTypeInfo.getDevice() instanceof CashIn) {
                    int[] deviceAllowedCashIDs = ((CashIn) payboxDeviceTypeInfo.getDevice()).GetAllowedCashIDList();

                    //Collect Distinct ID s
                    for (int i = 0; i < deviceAllowedCashIDs.length; i++) {
                        if (!resultCashIDList.contains(deviceAllowedCashIDs[i])) {
                            resultCashIDList.add(deviceAllowedCashIDs[i]);
                        }
                    }
                }
            }
        } catch (RuntimeException ex) {
            this.container.getLogger().Log("Hardware", "GetAllowedCashIDList(): " + ex.getMessage(), LogType.Error);
            this.container.getLogger().LogException(ex);
            resultCashIDList = new ArrayList<Integer>();
        }

        return Ints.toArray(resultCashIDList);
    }

    public final void GetAcceptRejectCounters(MutableInt accepted, MutableInt rejected) {
        ((CashIn) payboxDeviceTypeInfoDictionary.get("BanknoteReceiver").getDevice()).GetAcceptRejectCounters(accepted, rejected);
    }

    public final String getPrashivkaVersion() {
        return ((CashIn) payboxDeviceTypeInfoDictionary.get("BanknoteReceiver").getDevice()).getPrashivkaVersion();
    }
//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#endregion

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#region Printer Functionaliyy

    public final DeviceState PrintSynchronously(String payboxPrinterTypeName, String taggedText) {
        return PrintSynchronously(payboxPrinterTypeName, taggedText, Printer.Timeout_Infinite);
    }

    //C# TO JAVA CONVERTER NOTE: Java does not support optional parameters. Overloaded method(s) are created above:
//ORIGINAL LINE: public DeviceState PrintSynchronously(string payboxPrinterTypeName, string taggedText, int timeoutIntervalInMiliSeconds = Timeout.Infinite)
    public final DeviceState PrintSynchronously(String payboxPrinterTypeName, String taggedText, int timeoutIntervalInMiliSeconds) {
        return this.PrintSynchronously(payboxPrinterTypeName, taggedText, Printer.PrintMode.Default, -1, true, timeoutIntervalInMiliSeconds);
    }


    public final DeviceState PrintSynchronously(String payboxPrinterTypeName, String taggedText, Printer.PrintMode printMode, int lineFeedCount, boolean fullCut) {
        return PrintSynchronously(payboxPrinterTypeName, taggedText, printMode, lineFeedCount, fullCut, Printer.Timeout_Infinite);
    }

    //C# TO JAVA CONVERTER NOTE: Java does not support optional parameters. Overloaded method(s) are created above:
//ORIGINAL LINE: public DeviceState PrintSynchronously(string payboxPrinterTypeName, string taggedText, Printer.PrintMode printMode, int lineFeedCount, bool fullCut, int timeoutIntervalInMiliSeconds = Timeout.Infinite)
    public final DeviceState PrintSynchronously(String payboxPrinterTypeName, String taggedText, Printer.PrintMode printMode, int lineFeedCount, boolean fullCut, int timeoutIntervalInMiliSeconds) {
        PrinterDeviceState printerDeviceState = null;

        try {
            if (payboxDeviceTypeInfoDictionary.containsKey(payboxPrinterTypeName)) {
                IHardwareBase printer = payboxDeviceTypeInfoDictionary.get(payboxPrinterTypeName).getDevice();

                if (printer != null && printer instanceof Printer) {
                    printerDeviceState = ((Printer) printer).PrintSynchronously(taggedText, printMode, lineFeedCount, fullCut, timeoutIntervalInMiliSeconds);
                } else {
                    this.container.getLogger().Log("Hardware", String.format("PrintSynchronously(): %1$s Device Instance Is NULL Or Is Not Printer Type", payboxDeviceTypeInfoDictionary.get(payboxPrinterTypeName).getDeviceName()), LogType.Warning);
                }
            } else {
                this.container.getLogger().Log("Hardware", String.format("PrintSynchronously(): Unknown PayboxDeviceTypeName: %1$s", payboxPrinterTypeName), LogType.Warning);
            }
        } catch (Exception ex) {
            this.container.getLogger().Log("Hardware", "PrintSynchronously(): " + ex.getMessage(), LogType.Error);
            this.container.getLogger().LogException(ex);
        }

        return printerDeviceState;
    }

    public final void PrintAsynchronously(String payboxPrinterTypeName, String taggedText) {
        this.PrintAsynchronously(payboxPrinterTypeName, taggedText, Printer.PrintMode.Default, -1, true);
    }

    public final void PrintAsynchronously(String payboxPrinterTypeName, String taggedText, Printer.PrintMode printMode, int lineFeedCount, boolean fullCut) {
        try {
            if (payboxDeviceTypeInfoDictionary.containsKey(payboxPrinterTypeName)) {
                IHardwareBase printer = payboxDeviceTypeInfoDictionary.get(payboxPrinterTypeName).getDevice();

                if (printer != null && printer instanceof Printer) {
                    ((Printer) printer).PrintAsynchronously(taggedText, printMode, lineFeedCount, fullCut);
                } else {
                    this.container.getLogger().Log("Hardware", String.format("PrintAsynchronously(): %1$s Device Instance Is NULL Or Is Not Printer Type", payboxDeviceTypeInfoDictionary.get(payboxPrinterTypeName).getDeviceName()), LogType.Warning);
                }
            } else {
                this.container.getLogger().Log("Hardware", String.format("PrintAsynchronously(): Unknown PayboxDeviceTypeName: %1$s", payboxPrinterTypeName), LogType.Warning);
            }
        } catch (RuntimeException ex) {
            this.container.getLogger().Log("Hardware", "PrintAsynchronously(): " + ex.getMessage(), LogType.Error);
            this.container.getLogger().LogException(ex);
        }
    }
//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#endregion

    //C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#region Network Functionality
    private int networkConnectionUsagePriority = -1;

    public final boolean NetworkConnect(boolean tryOtherConnection) {
        boolean result = false;

        try {
            if (tryOtherConnection) {
                this.networkConnectionUsagePriority++;
            }

            PayboxDeviceTypeInfo payboxDeviceTypeInfoTemp = null;
            List<PayboxDeviceTypeInfo> values = payboxDeviceTypeInfoDictionary.values().stream().filter(a -> (a.getDevice() instanceof NetworkConnection))

                    .filter(c -> ((NetworkConnection) c.getDevice()).getUsagePriority() >= networkConnectionUsagePriority)
                    .sorted(((Comparator<PayboxDeviceTypeInfo>) (o1, o2) -> {
                        return new Integer(((NetworkConnection) o1.getDevice()).getUsagePriority()).compareTo(new Integer(((NetworkConnection) o2.getDevice()).getUsagePriority()));
                    }).reversed())

                    .collect(Collectors.toList());
            if (!values.isEmpty())
                payboxDeviceTypeInfoTemp = (PayboxDeviceTypeInfo) values.get(0).getDevice();

//                    .sorted(
//
//                    (o1, o2) -> new Integer(((NetworkConnection)o2.getDevice()).getUsagePriority()).compareTo(new Integer(((NetworkConnection)o1.getDevice()).getUsagePriority()))).c
//
//
//
//                    Where(x -> x.Device != null && x.Device instanceof NetworkConnection && ((NetworkConnection) x.Device).getUsagePriority() >= networkConnectionUsagePriority).OrderByDescending(x -> ((NetworkConnection) x.Device).getUsagePriority()).FirstOrDefault();

            NetworkConnection priorityNetworkConnection = payboxDeviceTypeInfoTemp != null ? (NetworkConnection) payboxDeviceTypeInfoTemp.getDevice() : null;

            if (priorityNetworkConnection != null) {
                priorityNetworkConnection.setIsActive(true);

                this.container.getLogger().Log("Hardware", String.format("NetworkConnect(): Priority Network Connection Found, Device: %1$s, InternetConnection: %2$s, UsagePriority:%3$s", payboxDeviceTypeInfoTemp.getDeviceName(), priorityNetworkConnection.getInternetConnection(), priorityNetworkConnection.getUsagePriority()), LogType.Information);

                this.networkConnectionUsagePriority = priorityNetworkConnection.getUsagePriority();

                if (priorityNetworkConnection.Connect()) {
                    this.container.getLogger().Log("Hardware", "NetworkConnect(): Successfully Connected", LogType.Information);
                    result = true;
                } else {
                    this.container.getLogger().Log("Hardware", "NetworkConnect(): Failed To Connect", LogType.Warning);
                }
            } else {
                this.container.getLogger().Log("Hardware", "NetworkConnect(): Priority Network Connection Not Found", LogType.Warning);

                if (this.networkConnectionUsagePriority != 0) {
                    this.networkConnectionUsagePriority--;
                }
            }
        } catch (RuntimeException ex) {
            this.container.getLogger().Log("Hardware", "NetworkConnect(): " + ex.getMessage(), LogType.Error);
            this.container.getLogger().LogException(ex);
            result = false;
        }

        return result;
    }

    public final boolean NetworkDisconnect() {
        boolean result = false;

        try {
            for (PayboxDeviceTypeInfo payboxDeviceTypeInfo : payboxDeviceTypeInfoDictionary.values()) {
                if (payboxDeviceTypeInfo.getDevice() != null && payboxDeviceTypeInfo.getDevice() instanceof NetworkConnection) {
                    if (((NetworkConnection) payboxDeviceTypeInfo.getDevice()).Disconnect()) {
                        this.container.getLogger().Log("Hardware", String.format("NetworkDisconnect(): Successfully Disconnected, Device: %1$s", payboxDeviceTypeInfo.getDeviceName()), LogType.Information);
                        result = true;
                    } else {
                        this.container.getLogger().Log("Hardware", String.format("NetworkDisconnect(): Failed To Disconnect, Device: %1$s", payboxDeviceTypeInfo.getDeviceName()), LogType.Warning);
                    }
                }
            }
        } catch (RuntimeException ex) {
            this.container.getLogger().Log("Hardware", "NetworkDisconnect(): " + ex.getMessage(), LogType.Error);
            this.container.getLogger().LogException(ex);
            result = false;
        }

        return result;
    }

    public final boolean NetworkReconnect() {
        boolean result = false;

        try {
            for (PayboxDeviceTypeInfo payboxDeviceTypeInfo : payboxDeviceTypeInfoDictionary.values()) {
                if (payboxDeviceTypeInfo.getDevice() != null && payboxDeviceTypeInfo.getDevice() instanceof NetworkConnection && ((NetworkConnection) payboxDeviceTypeInfo.getDevice()).getIsActive()) {
                    if (((NetworkConnection) payboxDeviceTypeInfo.getDevice()).Reconnect()) {
                        this.container.getLogger().Log("Hardware", String.format("NetworkReconnect(): Successfully Reconnected, Device: %1$s", payboxDeviceTypeInfo.getDeviceName()), LogType.Information);
                        result = true;
                    } else {
                        this.container.getLogger().Log("Hardware", String.format("NetworkReconnect(): Failed To Reconnect, Device: %1$s", payboxDeviceTypeInfo.getDeviceName()), LogType.Warning);
                    }
                }
            }
        } catch (RuntimeException ex) {
            this.container.getLogger().Log("Hardware", "NetworkReconnect(): " + ex.getMessage(), LogType.Error);
            this.container.getLogger().LogException(ex);
            result = false;
        }

        return result;
    }
//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#endregion

    //C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#region WatchDog Fuctionality
    public final int GetWatchDogPollingInterval(String payboxDeviceTypeName) {
        int result = 240;

        try {
            if (payboxDeviceTypeInfoDictionary.containsKey(payboxDeviceTypeName)) {
                IHardwareBase watchDogDevice = payboxDeviceTypeInfoDictionary.get(payboxDeviceTypeName).getDevice();

                if (watchDogDevice != null && watchDogDevice instanceof WatchDog) {
                    result = ((WatchDog) watchDogDevice).GetPollingInterval();
                } else {
                    this.container.getLogger().Log("Hardware", String.format("GetWatchDogPollingInterval(): %1$s Device Instance Is NULL Or Is Not WatchDog Type", payboxDeviceTypeInfoDictionary.get(payboxDeviceTypeName).getDeviceName()), LogType.Warning);
                }
            } else {
                this.container.getLogger().Log("Hardware", String.format("GetWatchDogPollingInterval(): Unknown PayboxDeviceTypeName: %1$s", payboxDeviceTypeName), LogType.Warning);
            }
        } catch (RuntimeException ex) {
            this.container.getLogger().Log("Hardware", "GetWatchDogPollingInterval(): " + ex.getMessage(), LogType.Error);
            this.container.getLogger().LogException(ex);
            result = 5;
        }

        return result;
    }

    public final Date GetWatchDogDateTime(String payboxDeviceTypeName) {
        Date watchDogDateTime = new Date(0L);

        try {
            if (payboxDeviceTypeInfoDictionary.containsKey(payboxDeviceTypeName)) {
                IHardwareBase watchDogDevice = payboxDeviceTypeInfoDictionary.get(payboxDeviceTypeName).getDevice();

                if (watchDogDevice != null && watchDogDevice instanceof WatchDog) {
                    watchDogDateTime = ((WatchDog) watchDogDevice).GetDateTime();
                } else {
                    this.container.getLogger().Log("Hardware", String.format("GetWatchDogDateTime(): %1$s Device Instance Is NULL Or Is Not WatchDog Type", payboxDeviceTypeInfoDictionary.get(payboxDeviceTypeName).getDeviceName()), LogType.Warning);
                }
            } else {
                this.container.getLogger().Log("Hardware", String.format("GetWatchDogDateTime(): Unknown PayboxDeviceTypeName: %1$s", payboxDeviceTypeName), LogType.Warning);
            }
        } catch (RuntimeException ex) {
            this.container.getLogger().Log("Hardware", "GetWatchDogDateTime(): " + ex.getMessage(), LogType.Error);
            this.container.getLogger().LogException(ex);
            watchDogDateTime = new Date(0L);
        }

        return watchDogDateTime;
    }

    public final boolean SetWatchDogDateTime(String payboxDeviceTypeName, Date dateTime) {
        boolean result = false;

        try {
            if (payboxDeviceTypeInfoDictionary.containsKey(payboxDeviceTypeName)) {
                IHardwareBase watchDogDevice = payboxDeviceTypeInfoDictionary.get(payboxDeviceTypeName).getDevice();

                if (watchDogDevice != null && watchDogDevice instanceof WatchDog) {
                    result = ((WatchDog) watchDogDevice).SetDateTime(dateTime);
                } else {
                    this.container.getLogger().Log("Hardware", String.format("SetWatchDogDateTime(): %1$s Device Instance Is NULL Or Is Not WatchDog Type", payboxDeviceTypeInfoDictionary.get(payboxDeviceTypeName).getDeviceName()), LogType.Warning);
                }
            } else {
                this.container.getLogger().Log("Hardware", String.format("SetWatchDogDateTime(): Unknown PayboxDeviceTypeName: %1$s", payboxDeviceTypeName), LogType.Warning);
            }
        } catch (RuntimeException ex) {
            this.container.getLogger().Log("Hardware", "SetWatchDogDateTime(): " + ex.getMessage(), LogType.Error);
            this.container.getLogger().LogException(ex);
            result = false;
        }

        return result;
    }

    public final boolean PollWatchDog(String payboxDeviceTypeName) {
        boolean result = false;

        try {
            if (payboxDeviceTypeInfoDictionary.containsKey(payboxDeviceTypeName)) {
                IHardwareBase watchDogDevice = payboxDeviceTypeInfoDictionary.get(payboxDeviceTypeName).getDevice();

                if (watchDogDevice != null && watchDogDevice instanceof WatchDog) {
                    result = ((WatchDog) watchDogDevice).Poll();
                } else {
                    this.container.getLogger().Log("Hardware", String.format("PollWatchDog(): %1$s Device Instance Is NULL Or Is Not WatchDog Type", payboxDeviceTypeInfoDictionary.get(payboxDeviceTypeName).getDeviceName()), LogType.Warning);
                }
            }
            //else
            //    this.container.Logger.Log("Hardware", string.Format("PollWatchDog(): Unknown PayboxDeviceTypeName: {0}", payboxDeviceTypeName), LogType.Warning);
        } catch (RuntimeException ex) {
            this.container.getLogger().Log("Hardware", "PollWatchDog(): " + ex.getMessage(), LogType.Error);
            this.container.getLogger().LogException(ex);
            result = false;
        }

        return result;
    }

    @Override
    public final boolean ComputerReset(String payboxDeviceTypeName) {
        return this.PerformActionOnWatchDogConnector(payboxDeviceTypeName, WatchDog.ConnectorName.ComputerReset, 0);
    }

    public final boolean ComputerShutdown(String payboxDeviceTypeName, int startUpTimeOut) {
        return this.PerformActionOnWatchDogConnector(payboxDeviceTypeName, WatchDog.ConnectorName.ComputerShutdown, startUpTimeOut);
    }

    @Override
    public final boolean ModemReset(String payboxDeviceTypeName) {
        return this.PerformActionOnWatchDogConnector(payboxDeviceTypeName, WatchDog.ConnectorName.ModemReset, 0);
    }

    //C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: public bool ModemShutdown(string payboxDeviceTypeName, uint startUpTimeOut)
    public final boolean ModemShutdown(String payboxDeviceTypeName, int startUpTimeOut) {
        return this.PerformActionOnWatchDogConnector(payboxDeviceTypeName, WatchDog.ConnectorName.ModemShutdown, startUpTimeOut);
    }

    public final boolean WatchDogReset(String payboxDeviceTypeName) {
        return this.PerformActionOnWatchDogConnector(payboxDeviceTypeName, WatchDog.ConnectorName.WatchDogReset, 0);
    }

    public final boolean PowerAdapterReset(String payboxDeviceTypeName) {
        return this.PerformActionOnWatchDogConnector(payboxDeviceTypeName, WatchDog.ConnectorName.PowerAdapterReset, 0);
    }

    public final boolean PowerAdapterOn(String payboxDeviceTypeName) {
        return this.PerformActionOnWatchDogConnector(payboxDeviceTypeName, WatchDog.ConnectorName.PowerAdapterOn, 0);
    }

    public final boolean PowerAdapterOff(String payboxDeviceTypeName) {
        return this.PerformActionOnWatchDogConnector(payboxDeviceTypeName, WatchDog.ConnectorName.PowerAdapterOff, 0);
    }
//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#endregion

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#region Vending Fuctionality

    public final boolean DropItem(int slotId, int timeoutIntervalInMiliSeconds) {
        return DropItem(slotId, timeoutIntervalInMiliSeconds, false);
    }

    public final boolean DropItem(int slotId) {
        return DropItem(slotId, 5000, false);
    }

    //C# TO JAVA CONVERTER NOTE: Java does not support optional parameters. Overloaded method(s) are created above:
//ORIGINAL LINE: public bool DropItem(int slotId, int timeoutIntervalInMiliSeconds = 5000, bool test = false)
    public final boolean DropItem(int slotId, int timeoutIntervalInMiliSeconds, boolean test) {
        boolean result = false;

        try {
            for (PayboxDeviceTypeInfo payboxDeviceTypeInfo : payboxDeviceTypeInfoDictionary.values()) {
                if (payboxDeviceTypeInfo.getDevice() != null && payboxDeviceTypeInfo.getDevice() instanceof Vending) {
                    result = ((Vending) payboxDeviceTypeInfo.getDevice()).DropItem(slotId, timeoutIntervalInMiliSeconds, test);
                    break;
                }
            }
        } catch (RuntimeException ex) {
            this.container.getLogger().Log("Hardware", "DropItem(): " + ex.getMessage(), LogType.Error);
            this.container.getLogger().LogException(ex);
            result = false;
        }

        return result;
    }

    public final boolean ChangeItemsInSlots(HashMap<Integer, Integer> slotItemsDictionary) {
        boolean result = false;

        try {
            for (PayboxDeviceTypeInfo payboxDeviceTypeInfo : payboxDeviceTypeInfoDictionary.values()) {
                if (payboxDeviceTypeInfo.getDevice() != null && payboxDeviceTypeInfo.getDevice() instanceof Vending) {
                    result = ((Vending) payboxDeviceTypeInfo.getDevice()).ChangeItemsInSlots(slotItemsDictionary);
                    break;
                }
            }

            result = true;
        } catch (RuntimeException ex) {
            this.container.getLogger().Log("Hardware", "ChangeItemsInSlots(): " + ex.getMessage(), LogType.Error);
            this.container.getLogger().LogException(ex);
            result = false;
        }

        return result;
    }
//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#endregion

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#region MRZReader

//C# TO JAVA CONVERTER TODO TASK: This event cannot be converted to Java:
//	public event MRZReaderEventHandler ReadDataListener
//		{
//			add
//			{
//				foreach(PayboxDeviceTypeInfo payboxDeviceTypeInfo in payboxDeviceTypeInfoDictionary.Values)
//				{
//					if(payboxDeviceTypeInfo.Device != null && payboxDeviceTypeInfo.Device is MRZReader)
//					{
//						((MRZReader)payboxDeviceTypeInfo.Device).ReadDataListener += value;
//						break;
//					}
//				}
//			}
//			remove
//			{
//				foreach(PayboxDeviceTypeInfo payboxDeviceTypeInfo in payboxDeviceTypeInfoDictionary.Values)
//				{
//					if(payboxDeviceTypeInfo.Device != null && payboxDeviceTypeInfo.Device is MRZReader)
//					{
//						((MRZReader)payboxDeviceTypeInfo.Device).ReadDataListener -= value;
//						break;
//					}
//				}
//			}
//		}

    public final void StartListening() {
        for (PayboxDeviceTypeInfo payboxDeviceTypeInfo : payboxDeviceTypeInfoDictionary.values()) {
            if (payboxDeviceTypeInfo.getDevice() != null && payboxDeviceTypeInfo.getDevice() instanceof MRZReader) {
                ((MRZReader) payboxDeviceTypeInfo.getDevice()).StartListening();
                break;
            }
        }
    }

    public final void StopListening() {
        for (PayboxDeviceTypeInfo payboxDeviceTypeInfo : payboxDeviceTypeInfoDictionary.values()) {
            if (payboxDeviceTypeInfo.getDevice() != null && payboxDeviceTypeInfo.getDevice() instanceof MRZReader) {
                ((MRZReader) payboxDeviceTypeInfo.getDevice()).StopListening();
                break;
            }
        }
    }

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#endregion

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#region Card Dispenser

//C# TO JAVA CONVERTER TODO TASK: This event cannot be converted to Java:
//	public event CardGivenEventHandler CardGivenEvent
//		{
//			add
//			{
//				foreach(PayboxDeviceTypeInfo payboxDeviceTypeInfo in payboxDeviceTypeInfoDictionary.Values)
//				{
//					if(payboxDeviceTypeInfo.Device != null && payboxDeviceTypeInfo.Device is CardDispenser)
//					{
//						((CardDispenser)payboxDeviceTypeInfo.Device).CardGivenEvent += value;
//						break;
//					}
//				}
//			}
//			remove
//			{
//				foreach(PayboxDeviceTypeInfo payboxDeviceTypeInfo in payboxDeviceTypeInfoDictionary.Values)
//				{
//					if(payboxDeviceTypeInfo.Device != null && payboxDeviceTypeInfo.Device is CardDispenser)
//					{
//						((CardDispenser)payboxDeviceTypeInfo.Device).CardGivenEvent -= value;
//						break;
//					}
//				}
//			}
//		}

//C# TO JAVA CONVERTER TODO TASK: This event cannot be converted to Java:
//	public event CardMovedCaptureBoxEventHandler MoveCaptureBoxEvent
//		{
//			add
//			{
//				foreach(PayboxDeviceTypeInfo payboxDeviceTypeInfo in payboxDeviceTypeInfoDictionary.Values)
//				{
//					if(payboxDeviceTypeInfo.Device != null && payboxDeviceTypeInfo.Device is CardDispenser)
//					{
//						((CardDispenser)payboxDeviceTypeInfo.Device).MoveCaptureBoxEvent += value;
//						break;
//					}
//				}
//			}
//			remove
//			{
//				foreach(PayboxDeviceTypeInfo payboxDeviceTypeInfo in payboxDeviceTypeInfoDictionary.Values)
//				{
//					if(payboxDeviceTypeInfo.Device != null && payboxDeviceTypeInfo.Device is CardDispenser)
//					{
//						((CardDispenser)payboxDeviceTypeInfo.Device).MoveCaptureBoxEvent -= value;
//						break;
//					}
//				}
//			}
//		}

    public final CardDataInformation ReadCard(int tryCount) {
        for (PayboxDeviceTypeInfo payboxDeviceTypeInfo : payboxDeviceTypeInfoDictionary.values()) {
            if (payboxDeviceTypeInfo.getDevice() != null && payboxDeviceTypeInfo.getDevice() instanceof CardDispenser) {
                return ((CardDispenser) payboxDeviceTypeInfo.getDevice()).ReadCard(tryCount);
            }
        }
        return null;
    }

    public final CardDataInformation ReadCard(int tryCount, String dispenser) {
        for (PayboxDeviceTypeInfo payboxDeviceTypeInfo : payboxDeviceTypeInfoDictionary.values()) {
            if (payboxDeviceTypeInfo.getDevice() != null && payboxDeviceTypeInfo.getDevice() instanceof CardDispenser && payboxDeviceTypeInfo.getDeviceName().equals(dispenser)) {
                return ((CardDispenser) payboxDeviceTypeInfo.getDevice()).ReadCard(tryCount);
            }
        }
        return null;
    }

    public final boolean DispenseCard(int timeOutSec) {
        for (PayboxDeviceTypeInfo payboxDeviceTypeInfo : payboxDeviceTypeInfoDictionary.values()) {
            if (payboxDeviceTypeInfo.getDevice() != null && payboxDeviceTypeInfo.getDevice() instanceof CardDispenser) {
                return ((CardDispenser) payboxDeviceTypeInfo.getDevice()).DispenseCard(timeOutSec);
            }
        }
        return false;
    }

    public final boolean DispenseCard(int timeOutSec, String dispenser) {
        for (PayboxDeviceTypeInfo payboxDeviceTypeInfo : payboxDeviceTypeInfoDictionary.values()) {
            if (payboxDeviceTypeInfo.getDevice() != null && payboxDeviceTypeInfo.getDevice() instanceof CardDispenser && payboxDeviceTypeInfo.getDeviceName().equals(dispenser)) {
                return ((CardDispenser) payboxDeviceTypeInfo.getDevice()).DispenseCard(timeOutSec);
            }
        }
        return false;
    }

    public final void CDEmptyCardBox() {
        for (PayboxDeviceTypeInfo payboxDeviceTypeInfo : payboxDeviceTypeInfoDictionary.values()) {
            if (payboxDeviceTypeInfo.getDevice() != null && payboxDeviceTypeInfo.getDevice() instanceof CardDispenser) {
                ((CardDispenser) payboxDeviceTypeInfo.getDevice()).EmptyCardBox();
            }
        }
    }

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#endregion

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#region CardReader

//C# TO JAVA CONVERTER TODO TASK: This event cannot be converted to Java:
//	public event CardInsertedEventHandler SKCardInsertedEvent
//		{
//			add
//			{
//				foreach(PayboxDeviceTypeInfo payboxDeviceTypeInfo in payboxDeviceTypeInfoDictionary.Values)
//				{
//					if(payboxDeviceTypeInfo.Device != null && payboxDeviceTypeInfo.Device is CardReader)
//					{
//						((CardReader)payboxDeviceTypeInfo.Device).CardInsertedEvent += value;
//						break;
//					}
//				}
//			}
//			remove
//			{
//				foreach(PayboxDeviceTypeInfo payboxDeviceTypeInfo in payboxDeviceTypeInfoDictionary.Values)
//				{
//					if(payboxDeviceTypeInfo.Device != null && payboxDeviceTypeInfo.Device is CardReader)
//					{
//						((CardReader)payboxDeviceTypeInfo.Device).CardInsertedEvent -= value;
//						break;
//					}
//				}
//			}
//		}

//C# TO JAVA CONVERTER TODO TASK: This event cannot be converted to Java:
//	public event CardRemovedEventHandler SKCardRemovedEvent
//		{
//			add
//			{
//				foreach(PayboxDeviceTypeInfo payboxDeviceTypeInfo in payboxDeviceTypeInfoDictionary.Values)
//				{
//					if(payboxDeviceTypeInfo.Device != null && payboxDeviceTypeInfo.Device is CardReader)
//					{
//						((CardReader)payboxDeviceTypeInfo.Device).CardRemovedEvent += value;
//						break;
//					}
//				}
//			}
//			remove
//			{
//				foreach(PayboxDeviceTypeInfo payboxDeviceTypeInfo in payboxDeviceTypeInfoDictionary.Values)
//				{
//					if(payboxDeviceTypeInfo.Device != null && payboxDeviceTypeInfo.Device is CardReader)
//					{
//						((CardReader)payboxDeviceTypeInfo.Device).CardRemovedEvent -= value;
//						break;
//					}
//				}
//			}
//		}

    public final void SKStartReading() {
        for (PayboxDeviceTypeInfo payboxDeviceTypeInfo : payboxDeviceTypeInfoDictionary.values()) {
            if (payboxDeviceTypeInfo.getDevice() != null && payboxDeviceTypeInfo.getDevice() instanceof CardReader) {
                ((CardReader) payboxDeviceTypeInfo.getDevice()).StartReading();
                break;
            }
        }
    }

    public final void SKStopReading() {
        for (PayboxDeviceTypeInfo payboxDeviceTypeInfo : payboxDeviceTypeInfoDictionary.values()) {
            if (payboxDeviceTypeInfo.getDevice() != null && payboxDeviceTypeInfo.getDevice() instanceof CardReader) {
                ((CardReader) payboxDeviceTypeInfo.getDevice()).StopReading();
                break;
            }
        }
    }

    public final CardReadStatus SKReadCard(MRZPerson person) {
        for (PayboxDeviceTypeInfo payboxDeviceTypeInfo : payboxDeviceTypeInfoDictionary.values()) {
            if (payboxDeviceTypeInfo.getDevice() != null && payboxDeviceTypeInfo.getDevice() instanceof CardReader) {
                return ((CardReader) payboxDeviceTypeInfo.getDevice()).ReadCard(person);
            }
        }
        return CardReadStatus.Error;
    }

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#endregion

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#region Digital Signature

//C# TO JAVA CONVERTER TODO TASK: This event cannot be converted to Java:
//	public event SignedEventHandler DSSignedEvent
//		{
//			add
//			{
//				foreach(PayboxDeviceTypeInfo payboxDeviceTypeInfo in payboxDeviceTypeInfoDictionary.Values)
//				{
//					if(payboxDeviceTypeInfo.Device != null && payboxDeviceTypeInfo.Device is DigitalSignature)
//					{
//						((DigitalSignature)payboxDeviceTypeInfo.Device).SignedEventHandler += value;
//						break;
//					}
//				}
//			}
//			remove
//			{
//				foreach(PayboxDeviceTypeInfo payboxDeviceTypeInfo in payboxDeviceTypeInfoDictionary.Values)
//				{
//					if(payboxDeviceTypeInfo.Device != null && payboxDeviceTypeInfo.Device is DigitalSignature)
//					{
//						((DigitalSignature)payboxDeviceTypeInfo.Device).SignedEventHandler -= value;
//						break;
//					}
//				}
//			}
//		}

    public final void DSStartPaint() {
        for (PayboxDeviceTypeInfo payboxDeviceTypeInfo : payboxDeviceTypeInfoDictionary.values()) {
            if (payboxDeviceTypeInfo.getDevice() != null && payboxDeviceTypeInfo.getDevice() instanceof DigitalSignature) {
                ((DigitalSignature) payboxDeviceTypeInfo.getDevice()).StartPaint();
                break;
            }
        }
    }

    public final Object getDSControl() {
        for (PayboxDeviceTypeInfo payboxDeviceTypeInfo : payboxDeviceTypeInfoDictionary.values()) {
            if (payboxDeviceTypeInfo.getDevice() != null && payboxDeviceTypeInfo.getDevice() instanceof DigitalSignature) {
                return ((DigitalSignature) payboxDeviceTypeInfo.getDevice()).getControl();
            }
        }
        return null;
    }

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#endregion

    public final Object GetDeviceResource(String name) {
        return Resources.getResourceManager().GetObject(name);
    }

    public final void Log(String facility, String value, LogType type) {
        this.container.getLogger().Log(facility, value, type);
    }

    public final void LogException(RuntimeException ex) {
        this.container.getLogger().LogException(ex);
    }

    public final XmlDocument GetConfig(String configFacility, String additionalParameter) {
        XmlDocument resultConfig = null;

        try {
            XmlElement tempNode = null;

            if (configFacility.equals("InternetConnections")) {
                tempNode = (XmlElement) hardwareConfig.getDocumentElement().SelectSingleNode("/HardwareConfig/InternetConnections");
            } else {
                tempNode = (XmlElement) hardwareConfig.getDocumentElement().SelectSingleNode("/HardwareConfig/Devices/Device[@name='" + configFacility + "' and @payboxDeviceType='" + additionalParameter + "']");
            }

            if (tempNode != null) {
                resultConfig = new XmlDocument();
                /* TODO CreateXmlDeclaration*/
//                resultConfig.appendChild(resultConfig.CreateXmlDeclaration("1.0", "UTF-8", null));
//                resultConfig.appendChild(resultConfig.ImportNode(tempNode, true));
            }

            //If facility does not exists in hardware config ask to configurator
            if (resultConfig == null) {
                resultConfig = this.container.getConfigurator().GetConfig(configFacility, ConfigType.GeneralConfig);
            }
        } catch (Exception ex) {
            this.container.getLogger().Log("Hardware", "GetConfig(): " + ex.getMessage(), LogType.Error);
            this.container.getLogger().LogException(ex);
        }

        return resultConfig;
    }

    public final String GetStaticValue(String key) {
        return this.container.getConfigurator().GetStaticValue(key);
    }

    public final void SaveStaticValue(String key, String value) {
        this.container.getConfigurator().SaveStaticValue(key, value);
    }

    EventManager<SoftwareActionToDevice> softwareActionToDeviceEventHanler = new EventManager<>();


    public final void SubscribeSoftwareActionEvents(IGeneralLogicModule generalModule) {
        if (this.container.getGeneralModule() == generalModule) {
            this.container.getLogger().Log("Hardware", "SubscribeSoftwareActionEvents(): Event Subscribed", LogType.Information);

//C# TO JAVA CONVERTER TODO TASK: Java has no equivalent to C#-style event wireups:
            this.container.getGeneralModule().addStateChangedHandler(this::SoftwareActionToDeviceHandler);
        } else {
            this.container.getLogger().Log("Hardware", "SubscribeSoftwareActionEvents(): General Logic Module Is Not Same", LogType.Warning);
        }
    }

    public final Map<String, Map<String, Integer>> GetAllDeviceCountersDictionary() {
        Map<String, Map<String, Integer>> allDeviceCountersDictionary = new HashMap<>();

        for (Map.Entry<String, PayboxDeviceTypeInfo> keyValuePair : payboxDeviceTypeInfoDictionary.entrySet()) {
            if (keyValuePair.getValue().getDevice() != null) {
                Map<String, Integer> deviceCountersDictionaryTemp = keyValuePair.getValue().getDevice().GetDeviceCountersDictionary();

                if (deviceCountersDictionaryTemp != null) {
                    //this.container.Logger.Log("Hardware", string.Format("GetAllDeviceCountersDictionary(): {0}: counters dictionary elements count:{1}", keyValuePair.Key, deviceCountersDictionaryTemp.Count), LogType.Information);
                    allDeviceCountersDictionary.put(keyValuePair.getKey(), deviceCountersDictionaryTemp);
                }
                //else
                //    this.container.Logger.Log("Hardware", string.Format("GetAllDeviceCountersDictionary(): {0}'s counters dictionary is null", keyValuePair.Key), LogType.Warning);
            }
        }

        return allDeviceCountersDictionary;
    }

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#endregion

    private boolean PerformActionOnWatchDogConnector(String payboxDeviceTypeName, WatchDog.ConnectorName connectorName, Object parameter) {
        boolean result = false;

        try {
            if (payboxDeviceTypeInfoDictionary.containsKey(payboxDeviceTypeName)) {
                IHardwareBase watchDogDevice = payboxDeviceTypeInfoDictionary.get(payboxDeviceTypeName).getDevice();

                if (watchDogDevice != null && watchDogDevice instanceof WatchDog) {
                    result = ((WatchDog) watchDogDevice).PerformActionOnConnector(connectorName, parameter);
                } else {
                    this.container.getLogger().Log("Hardware", String.format("PerformActionOnWatchDogConnector(): %1$s Device Instance Is NULL Or Is Not WatchDog Type", payboxDeviceTypeInfoDictionary.get(payboxDeviceTypeName).getDeviceName()), LogType.Warning);
                }
            } else {
                this.container.getLogger().Log("Hardware", String.format("PerformActionOnWatchDogConnector(): Unknown PayboxDeviceTypeName: %1$s", payboxDeviceTypeName), LogType.Warning);
            }
        } catch (RuntimeException ex) {
            this.container.getLogger().Log("Hardware", "PerformActionOnWatchDogConnector(): " + ex.getMessage(), LogType.Error);
            this.container.getLogger().LogException(ex);
            result = false;
        }

        return result;
    }


    public final void LogException(Exception ex) {
        this.container.getLogger().LogException(ex);
    }


    @Override
    public boolean ChangeItemsInSlots(Map<Integer, Integer> slotItemsDictionary) {
        boolean result = false;

        try {
            for (PayboxDeviceTypeInfo payboxDeviceTypeInfo : payboxDeviceTypeInfoDictionary.values()) {
                if (payboxDeviceTypeInfo.getDevice() != null && payboxDeviceTypeInfo.getDevice() instanceof Vending) {
                    result = ((Vending) payboxDeviceTypeInfo.getDevice()).ChangeItemsInSlots(slotItemsDictionary);
                    break;
                }
            }

            result = true;
        } catch (RuntimeException ex) {
            this.container.getLogger().Log("Hardware", "ChangeItemsInSlots(): " + ex.getMessage(), LogType.Error);
            this.container.getLogger().LogException(ex);
            result = false;
        }

        return result;
    }

    @Override
    public boolean ModemShutdown(String payboxDeviceTypeName, UnsignedInteger startUpTimeOut) {
        return this.PerformActionOnWatchDogConnector(payboxDeviceTypeName, WatchDog.ConnectorName.ModemShutdown, startUpTimeOut);
    }

    @Override
    public boolean ComputerShutdown(String payboxDeviceTypeName, UnsignedInteger startUpTimeOut) {
        return this.PerformActionOnWatchDogConnector(payboxDeviceTypeName, WatchDog.ConnectorName.ComputerShutdown, startUpTimeOut);
    }
}
