package com.pay.hardware;

import com.pay.base.CashIn;
import com.pay.base.CashMoney;
import com.pay.base.Error;
import com.pay.base.config.xml.XmlComment;
import com.pay.base.config.xml.XmlDocument;
import com.pay.base.config.xml.XmlNode;
import com.pay.base.config.xml.XmlNodeList;
import com.pay.base.devicestate.CashInDeviceState;
import com.pay.base.devicestate.DeviceState;
import com.pay.base.devicestate.enums.CashInState;
import com.pay.base.enums.MoneyType;
import com.pay.base.enums.StateCategory;
import com.pay.helper.Application;
import com.pay.helper.BitConverter;
import com.pay.helper.DateTime;
import com.pay.helper.enums.LogType;
import com.pay.io.serial.SerialPort;

import java.util.*;


/**
 * Created by dito on 4/5/2017.
 */
public class CashCode extends CashIn {
    private static SerialPort CashComPort = new SerialPort();
    private String comPort = "";

    private static volatile CashCode instance;
    private static Object syncRoot = new Object();

    private int pendingBill;

    public static CashCode GetInstance(Hardware pHardware, String payboxDeviceTypeName) {
        if (instance == null) {
            synchronized (syncRoot) {
                if (instance == null) {
                    instance = new CashCode(pHardware, payboxDeviceTypeName);
                }
            }
        }

        return instance;
    }

    //C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#region IHardwareBase Members
    @Override
    public boolean Initialize() {
        try {
            if (this.configIsValid && !this.initialized) {
                Connect(this.comPort);
            }

            return initialized;
        } catch (Exception ex) {
            hardware.Log(typeName, "Initialize()" + ex.getMessage(), LogType.Error);
            hardware.LogException(ex);
            return false;
        }
    }

    @Override
    public boolean Deinitialize() {
        if (this.initialized) {
            this.runMainThread = false;

            while (this.mainThread.isAlive()) {
                ;
            }

            try {
                if (CashComPort.isOpend()) {
                    CashComPort.close();
                }
            } catch (RuntimeException ex) {
                hardware.LogException(ex);
            }

            this.initialized = false;
        }

        try {
            if (virtualCassette != null) {
                virtualCassette.Deinitialize();
            }
        } catch (RuntimeException e) {
            hardware.LogException(e);
        }

        return true;
    }

    @Override
    public boolean Reset() {
        if (this.Deinitialize()) {
            return this.Initialize();
        } else {
            return false;
        }
    }

    @Override
    public DeviceState GetState(boolean force) {
        CashInState cashInState = CashInState.NotWorking;
        Map<Integer, Error> errors = new HashMap<>();

        try {
            if (this.initialized) {
                switch (ValState.State) {
                    case CassetteSet:
                        //if (CasseteChange)
                        //{
                        cashInState = CashInState.CassetteSet;
                        //CasseteChange = false;
                        //}
                        break;
                    case CassetteGet:
                        //if (!CasseteChange)
                        //{
                        cashInState = CashInState.CassetteGet;
                        //    CasseteChange = true;
                        //}
                        break;
                    case PowerUp:
                        cashInState = CashInState.PowerUp;
                        break;
                    case Accepting:
                        cashInState = CashInState.InProcess;
                        break;
                    case Error:
                        cashInState = CashInState.NotWorking;
                        break;
                    case Holding:
                        cashInState = CashInState.InProcess;
                        break;
                    case Idle:
                        cashInState = CashInState.Idle;
                        break;
                    case UnitDisable:
                        cashInState = CashInState.UnitDisable;
                        break;
                    case Rejecting:
                        cashInState = CashInState.InProcess;
                        break;
                    case Returned:
                        cashInState = CashInState.InProcess;
                        break;
                    case Returning:
                        cashInState = CashInState.InProcess;
                        break;
                    case Stacked:
                        cashInState = CashInState.InProcess;
                        break;
                    case Staking:
                        cashInState = CashInState.InProcess;
                        break;
                }
            } else {
                cashInState = CashInState.Uninitialized;
            }

            int cashCodeStateTemp = this.getCashCodeState();
            Error errorTemp = null;


            if (cashInState == CashInState.CassetteGet) {
                errorTemp = errorDictionary.get(96); //cassetteGet error
            } else if (errorDictionary.containsKey(cashCodeStateTemp) && cashInState != CashInState.Uninitialized) {
                errorTemp = errorDictionary.get(cashCodeStateTemp);
            } else {
                errorTemp = errorDictionary.get(-1); //Default Error, Must Exists In errorDictionary
            }

            errors.put(errorTemp.getGeneralCode(), errorTemp);
        } catch (RuntimeException ex) {
            hardware.Log(typeName, "GetState(): " + ex.getMessage(), LogType.Error);
            hardware.LogException(ex);
        }

        return new CashInDeviceState(this.getClass(), this.payboxDeviceTypeName, errors, cashInState, GetAllowedCashIDList());
    }

    private volatile boolean runMainThread = true;
    private Thread mainThread = null;

    //C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#region StaticFields
    private static Object PortLockObject = new Object();
    private static PCCommandSturct PCStruct = new PCCommandSturct();
    private static ValidatorState ValState;
    private static ValidatorState OldValState;
    //static EventWaitHandle CCControlHNDL;
    //private static bool BillInHold;
    //private static bool NeedSend;
    //private static bool CasseteChange;
//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#endregion

    private Object CashCodeLockObject;
    //private static CashCodeHistory CCHistory;

    private int getCashCodeState() {
        synchronized (CashCodeLockObject) {
            return ValState.ErrorCode;
        }
    }
    //set
    //{
    //    if (mCashCodeState != value)
    //    {
    //        lock (CashCodeLockObject)
    //        {
    //            mCashCodeState = value;
    //        }
    //    }
    //}

    private static boolean getBillProcessingComplete() {
        if (ValState.State != VState.Accepting && ValState.State != VState.Holding && ValState.State != VState.Staking && ValState.State != VState.Stacked) {
            return true;
        } else {
            return false;
        }
    }

    private CashCode(Hardware pHardware, String payboxDeviceTypeName) {
        try {
            this.payboxDeviceTypeName = payboxDeviceTypeName;
            this.typeName = this.getClass().getSimpleName();
            hardware = pHardware;
            LoadConfigs();

            //CasseteChange = true;
            //BillInHold = false;
            //NeedSend = false;

            //CCHistory = new CashCodeHistory();
            //CCControlHNDL = new EventWaitHandle(true, EventResetMode.ManualReset, "CCControlEventWaitHandle");
            ////CCControlHNDL = EventWaitHandle.OpenExisting("CCControlEventWaitHandle", System.Security.AccessControl.EventWaitHandleRights.FullControl);

            CashComPort.setReadTimeout(2000);

            CashCodeLockObject = new Object();
            PortLockObject = new Object();

            PCStruct = new PCCommandSturct(false);
            ValState = new ValidatorState();
            OldValState = new ValidatorState();
        } catch (RuntimeException ex) {
            hardware.Log(typeName, "CashCode(): " + ex.getMessage(), LogType.Error);
            hardware.LogException(ex);
        }
    }

    private void LoadConfigs() {
        try {
            XmlDocument generalErrorsConfig = hardware.GetConfig("GeneralErrors", "");
            XmlDocument cashCodeConfig = hardware.GetConfig(typeName, this.payboxDeviceTypeName);
            XmlDocument cashInfoConfig = hardware.GetConfig("CashInfo", "");

            XmlNodeList paramsNodeList = cashCodeConfig.getDocumentElement().getElementsByTagName("Parameters").get(0).getChildNodes();
            //Get Parameters
            for (XmlNode paramNode : paramsNodeList) {
                switch (paramNode.getAttributes().get("name").getValue()) {
                    case "ComPort":
                        this.comPort = paramNode.getInnerText();
                        break;
                    case "HasCassetteFullError":
                        this.hasCassetteFullError = Boolean.parseBoolean(paramNode.getInnerText());
                        break;
                    case "CashLimitInCassette":
                        this.cashLimitInCassette = Integer.parseInt(paramNode.getInnerText());
                        break;
                }
            }

            XmlNodeList errorsNodeList = cashCodeConfig.getDocumentElement().getElementsByTagName("ErrorInfo").get(0).getChildNodes();

            //Get Error Info
            for (XmlNode errorNode : errorsNodeList) {
                if (errorNode instanceof XmlComment) {
                    int code = Integer.parseInt(errorNode.getAttributes().get("code").getValue());
                    String description = errorNode.getInnerText();
                    int generalCode = Integer.parseInt(errorNode.getAttributes().get("generalCode").getValue());
                    String generalDescription = generalErrorsConfig.getDocumentElement().SelectSingleNode("/GeneralErrors/Error[@code='" + generalCode + "']").getInnerText();
                    StateCategory category = StateCategory.valueOf(errorNode.getAttributes().get("category").getValue());

                    //Temporarly, until config update
                    if (code == -2) //Exception In Poll
                    {
                        category = StateCategory.Problem;
                        generalCode = 3;
                        generalDescription = generalErrorsConfig.getDocumentElement().SelectSingleNode("/GeneralErrors/Error[@code='" + generalCode + "']").getInnerText();
                    }

                    errorDictionary.put(code, new Error(code, description, generalCode, generalDescription, category));
                }
            }

            errorDictionary.put(96, new Error(96, "Cassette Get error", 3, "Failure", StateCategory.Problem));

            //Check Necessary (For Manualy Describe) Errors
            if (!errorDictionary.containsKey(-1) || !errorDictionary.containsKey(-2) || (!this.hasCassetteFullError && !errorDictionary.containsKey(65))) //Cassette Full Error - Poll Error - Default Error
            {
                return;
            }

            XmlNodeList cashNodeList = cashCodeConfig.getDocumentElement().getElementsByTagName("CashInfo").get(0).getChildNodes();;
            XmlNodeList cashInfoNodeList = cashInfoConfig.getDocumentElement().getChildNodes();
            //Get Cash Info
            for (XmlNode cashNode : cashNodeList) {
                for (XmlNode cashInfoNode : cashInfoNodeList) {
                    if (cashNode.getAttributes().get("id").getValue().equals(cashInfoNode.getAttributes().get("id").getValue())) {
                        int id = Integer.parseInt(cashNode.getAttributes().get("id").getValue());
                        int code = Integer.parseInt(cashNode.getAttributes().get("code").getValue());

                        if (code < 0 || code > 7) {
                            break;
                        }

                        String name = cashInfoNode.SelectSingleNode("Name").getInnerText();
                        int nominal = Integer.parseInt(cashInfoNode.SelectSingleNode("Nominal").getInnerText());
                        MoneyType moneyType = MoneyType.valueOf(cashInfoNode.SelectSingleNode("Type").getInnerText());
                        com.pay.base.enums.Currency currency =  com.pay.base.enums.Currency.valueOf(cashInfoNode.SelectSingleNode("Currency").getInnerText());

                        cashDictionary.put(code, new CashMoney(currency, moneyType, id, name, nominal));
                        break;
                    }
                }
            }

            this.configIsValid = true;
        } catch (Exception ex) {
            this.configIsValid = false;
            hardware.Log(typeName, "LoadConfigs(): " + ex.getMessage(), LogType.Error);
            hardware.LogException(ex);
        }
    }

    private void Connect(String comPort) throws Exception {
        this.virtualCassette = new VirtualCassette(this);

        CashComPort.setPortName(comPort);
        CashComPort.Open();

        CCMessage msg = new CCMessage((byte) 0x33, null, 0); //Poll
        SendCommand(msg.toBytesArray());
        Thread.sleep(300);
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: byte[] ByteArray = new byte[CashComPort.BytesToRead];


        CCMessage LastReceived = new CCMessage();
        LastReceived.fromByteArray(CashComPort.ReadWholeLock());

        //Detect Power up with bill in acceptor
        if (!LastReceived.isComplete()|| LastReceived.getData() == null || LastReceived.getData().length == 0 || LastReceived.getData()[0] != 0x11) {
            SendCommand("RESET");
            Thread.sleep(3000);
        } else {
            hardware.Log("CashBytesLogger", "Detect Power up with bill in acceptor: 0x11", LogType.Warning);
        }

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
        ///#region Log Device Information
        try {
            SendCommand("IDENTIFICATION");

            Thread.sleep(3000);
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: ByteArray = new byte[CashComPort.BytesToRead];


            LastReceived = new CCMessage();
            LastReceived.fromByteArray(CashComPort.ReadWholeLock());

            if (LastReceived.isComplete() && LastReceived.getData() != null && LastReceived.getData().length > 15) {
                String str_msg = "";
                for (int i = 0; i < 15; i++) {
                    str_msg += (char) LastReceived.getData()[i];
                }
                prashivkaVersion = "CashInType=" + typeName + "; Version=" + str_msg.trim();
                hardware.Log("CashInVersion", "CashInType=" + typeName + "; Version=" + str_msg.trim(), LogType.Information);
            }
        } catch (RuntimeException ex) {
            hardware.Log(typeName, "Exception while reading Version: " + ex.getMessage(), LogType.Error);
        }
//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
        ///#endregion

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
        ///#region Get allowed cash list from device and change cashDictionary
        HashMap<Integer, CashMoney> cashDictionaryFromDevice = new HashMap<>();
        int tryCount = 3;
        int denomWaitIntervalInMiliSeconds = 1000;

        while (tryCount != 0) {
            try {
                SendCommand("DENOM");
                Thread.sleep(denomWaitIntervalInMiliSeconds);
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: ByteArray = new byte[CashComPort.BytesToRead];

                LastReceived = new CCMessage();
                LastReceived.fromByteArray(CashComPort.ReadWholeLock());

                cashDictionaryFromDevice.clear();

                //Fill cashDictionaryFromDevice
                if (LastReceived.isComplete() && LastReceived.getData() != null && LastReceived.getData().length > 0) {
                    for (int i = 0; i < LastReceived.getData().length; i += 5) {
                        int p = (int) Math.pow((double) 10, (double) LastReceived.getData()[i + 4]);

//C# TO JAVA CONVERTER TODO TASK: There is no Java equivalent to LINQ queries:
                        int normal = LastReceived.getData()[i] * p * 100;
                        Integer key = this.cashDictionary.entrySet().stream().
                                filter(x -> x.getValue().getNominal() == normal).map(Map.Entry::getKey)
                                .findFirst()
                                .orElse(null);
                        CashMoney value = key == null ? null : this.cashDictionary.get(key);

//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: if(!keyValuePairTemp.Equals(default(KeyValuePair<int, CashMoney>)) && LastReceived.Data[i + 1] == (byte)'G' && LastReceived.Data[i + 2] == (byte)'E' && LastReceived.Data[i + 3] == (byte)'O')
                        if (value != null && LastReceived.getData()[i + 1] == (byte) 'G' && LastReceived.getData()[i + 2] == (byte) 'E' && LastReceived.getData()[i + 3] == (byte) 'O') {
                            cashDictionaryFromDevice.put(i / 5, value);
                            initialized = true;
                        }
                    }

                    if (initialized) {
                        hardware.Log(typeName, "Connect(): Successfully read cash list from device", LogType.Information);
                        break;
                    } else {
                        hardware.Log(typeName, "Connect(): Incorrect cash list from device", LogType.Error);
                    }
                } else {
                    hardware.Log(typeName, "Connect(): Could not read cash list from device", LogType.Error);
                }
            } catch (RuntimeException ex) {
                cashDictionaryFromDevice.clear();
                hardware.Log(typeName, "Connect(): Exception while reading cash list from device: " + ex.getMessage(), LogType.Error);
            }

            tryCount--;
            denomWaitIntervalInMiliSeconds *= 2;
        }
//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
        ///#endregion

        this.cashDictionary = cashDictionaryFromDevice;

        if (initialized) {
            this.runMainThread = true;
            mainThread = new Thread(() -> {
                try {
                    CashCodeThreadFunction();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            mainThread.start();
        }
    }

    @Override
    protected void StartCashReceive() {
        PCStruct.setAllowBills(true);
    }

    //public void StartAmountReceive(string pn)
    //{
    //    PCStruct.AllowBills = true;
    //    PCStruct.PhoneNumber = pn;
    //}

    @Override
    protected void StopCashReceive() {
        PCStruct.setAllowBills(false);
    }

    //private bool BillAllowDenyComplete
    //{
    //    get
    //    {
    //        return PCStruct.AllowBills == ValState.AllowBills;
    //    }
    //}

    private void CashCodeThreadFunction() throws Exception{
        while (this.runMainThread) {
            try {
                Poll();

                GetValidatorState();

                GetBillAllowState();

                if (!ValState.equals(OldValState)) {
                    if (ValState.State == VState.Rejecting) {
                        IncrementRejected();
                    } else if (ValState.State == VState.Stacked) {
                        IncrementAccepted();
                    }

                    String billName = String.valueOf(ValState.Bill);

                    if (cashDictionary.keySet().contains(ValState.Bill) && (ValState.State == VState.Holding || ValState.State == VState.Stacked || ValState.State == VState.Staking)) {
                        billName = cashDictionary.get(ValState.Bill).getName();
                    }

                    hardware.Log("CashBytesLogger", "BillName='" + billName + "'; " + ValState.toString(), LogType.Information);
                    //Log.Log("CashCodeBytesLogger", ValState.ToString(), 0);

                    DeviceState stateArgs = this.GetState(false);
                    OnStateChanged(null);

                    OldValState.Copy(ValState);
                }

                //if (NeedSend)
                //{
                //    NeedSend = false;
                //    DataEventArgs de;
                //    de = new DataEventArgs();
                //    de.Data = CCHistory.ToString();

                //    Log.Log("CashCodeError", de.Data, 0);
                //    onLogSendReceived(null, de);
                //}

                //if (!BillProcessingComplete)
                //    CCControlHNDL.Reset();
                //else
                //    CCControlHNDL.Set();

                if (!ValState.AllowError && PCStruct.getAllowBills() != ValState.AllowBills) {
                    if (PCStruct.getAllowBills()) {
                        SendCommand("StartAmount");
                    } else {
                        if (getBillProcessingComplete()) {
                            SendCommand("StopAmount");
                        }
                    }
                }

                if (ValState.State == VState.Holding) {
                    if (ValState.AllowBills) {
                        SendCommand("Stack");
                    } else {
                        SendCommand("Return");
                    }
                }

                if (ValState.State == VState.PowerUp && ValState.ErrorCode != 0x11) //Power up with bill in acceptor error code
                {
                    SendCommand("RESET");
                }

                Thread.sleep(100);
            } catch (RuntimeException ex) {
                //hardware.Log("CashCodeBytesLogger", "CashCodeThreadFunction(): " + ex.Message, LogType.Error);

                Thread.sleep(1000);
            }
        }
    }

    private void GetValidatorState() {
        try {
            if (ValState.State == VState.Stacked) //need more check... check if on error and if holded bill equals to stacked bill; in future ValState.State = VState.Error on poll error
            {
                if (ValState.Bill != pendingBill) {
                    hardware.Log("AmountReceive", String.format("AmountReceive ignored, pending bill (%1$s) and stacked bill (%2$s) mismatch", pendingBill, ValState.Bill), LogType.Warning);
                    return;
                }
                if (cashDictionary.containsKey(ValState.Bill)) {
                    hardware.Log("AmountReceive", String.valueOf(ValState.Bill), LogType.Information);

                    CashMoney cashArgs = (CashMoney) cashDictionary.get(ValState.Bill);
                    OnCashReceived(null, new CashMoney(cashArgs.getCurrency(), cashArgs.getType(), cashArgs.getId(), cashArgs.getName(), cashArgs.getNominal()));

                    pendingBill = -1;
                } else {
                    hardware.Log("AmountReceive (Unknown)", String.valueOf(ValState.Bill), LogType.Information);
                }
            }

            //DataEventArgs de;

            //if (ValState.AllowBills)
            //{
            //    de = new DataEventArgs();
            //    de.Data = "AmountReceiveStarted";
            //    //onStateReceived(null, de);
            //}
            //else if (ValState.AllowError || ValState.PollError)
            //{
            //    de = new DataEventArgs();
            //    de.Data = "Error : " + ValState.ErrorCode;

            //    if (BillInHold)
            //    {
            //        BillInHold = false;
            //        NeedSend = true;
            //    }
            //    //onStateReceived(null, de);
            //}

            //switch (ValState.State)
            //{
            //    case VState.CassetteSet:
            //        if (CasseteChange)
            //        {
            //            OnCassetteInitializeRecieved(null);
            //            CasseteChange = false;
            //        }
            //        break;
            //    case VState.CassetteGet:
            //        if (!CasseteChange)
            //        {
            //            OnCassetteChangeRecieved(null);
            //            CasseteChange = true;
            //        }
            //        break;
            //    case VState.Accepting:
            //        de = new DataEventArgs();
            //        de.Data = "Accepting";
            //        //onStateReceived(null, de);
            //        break;
            //    case VState.Error:
            //        de = new DataEventArgs();
            //        de.Data = "Error : " + ValState.ErrorCode;
            //        //onStateReceived(null, de);
            //        break;
            //    case VState.Holding:
            //        de = new DataEventArgs();
            //        de.Data = "Holding";
            //        BillInHold = true;
            //        //onStateReceived(null, de);
            //        break;
            //    case VState.Idle:
            //        de = new DataEventArgs();
            //        de.Data = "Idle";
            //        //onStateReceived(null, de);
            //        break;
            //    case VState.Rejecting:
            //        de = new DataEventArgs();
            //        de.Data = "Rejecting";
            //        //onStateReceived(null, de);
            //        break;
            //    case VState.Returned:
            //        de = new DataEventArgs();
            //        de.Data = "Returned";
            //        //onStateReceived(null, de);
            //        break;
            //    case VState.Returning:
            //        de = new DataEventArgs();
            //        de.Data = "Returning";
            //        //onStateReceived(null, de);
            //        break;
            //    case VState.Stacked:
            //        de = new DataEventArgs();
            //        de.Data = "Stacked";
            //        BillInHold = false;
            //        //onStateReceived(null, de);

            //        AmountEventArgs Aea = new AmountEventArgs();
            //        Aea.Amount = ValState.Bill;
            //        Log.Log("AmountReceive", ValState.Bill.ToString() + " : " + PCStruct.PhoneNumber, 0);
            //        OnAmountReceived(Aea);
            //        break;
            //    case VState.Staking:
            //        de = new DataEventArgs();
            //        de.Data = "Staking";
            //        //onStateReceived(null, de);
            //        break;
            //}

            //if (BillInHold && (ValState.State != VState.Holding && ValState.State != VState.Staking && ValState.State != VState.Stacked))
            //{
            //    BillInHold = false;
            //    NeedSend = true;
            //}
        } catch (RuntimeException ex) {
            hardware.Log("CashBytesLogger", "GetValidatorState(): " + ex.getMessage(), LogType.Error);
        }
    }

    private void GetBillAllowState() throws Exception{
        try {
            CCMessage msg2 = new CCMessage((byte) 0x31, null, 0);
            SendCommand(msg2.toBytesArray());

//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: byte[] ByteArray2 = new byte[CashComPort.BytesToRead];

            CCMessage LastReceived2 = new CCMessage();

            LastReceived2.fromByteArray(CashComPort.ReadWholeLock());

            if (LastReceived2.isComplete()) {
                BitSet ar = BitSet.valueOf(LastReceived2.getData());

                if (ar.get(16) || ar.get(17) || ar.get(18) || ar.get(19) || ar.get(20) || ar.get(21) || ar.get(22) || ar.get(23)) {
                    ValState.AllowBills = true;

                    synchronized (allowedCashDeviceCodeListFromDeviceLock) {
                        allowedCashDeviceCodeListFromDevice.clear();

                        for (int i = 16; i < 24; i++) {
                            if (ar.get(i)) {
                                allowedCashDeviceCodeListFromDevice.add(i - 16);
                            }
                        }
                    }
                } else {
                    ValState.AllowBills = false;

                    synchronized (allowedCashDeviceCodeListFromDeviceLock) {
                        allowedCashDeviceCodeListFromDevice.clear();
                    }
                }
            }

            ValState.AllowError = false;
        } catch (Exception ex) {
            ValState.AllowError = true;

            //hardware.Log("CashCodeBytesLogger", "GetBillAllowState(): " + ex.Message, LogType.Error);
        }
    }

    private void Poll() throws Exception {
        SendPoll();
        GetPoll();
    }

    private void SendPoll() throws Exception {
        try {
            CCMessage msg = new CCMessage((byte) 0x33, null, 0);
            SendCommand(msg.toBytesArray());

            ValState.PollError = false;
        } catch (RuntimeException ex) {
            ValState.PollError = true;
            ValState.Bill = -1;
            ValState.ErrorCode = -2;

            //hardware.Log("CashCodeBytesLogger", "SendPoll(): " + ex.Message, LogType.Error);
        }
    }

    private void GetPoll() throws Exception {
        try {



            CCMessage LastReceived = new CCMessage();

            LastReceived.fromByteArray(CashComPort.ReadWholeLock());

            if (LastReceived.isComplete() && LastReceived.getData() != null && LastReceived.getData().length > 0) {
                StringBuffer de = new StringBuffer();

                de.append(DateTime.now().toStringWithMsc("HH-mm-ss") + ":\t");
                String str = "";
                for (int i = 0; i < LastReceived.getData().length; i++) {
                    str = "0x" + String.format("%02x", LastReceived.getData()[i]) + " ";
                    de.append(str);
                    Application.DoEvents();
                }
                de.append("\n");


                //CCHistory.Add(de.Data);

                //OnDataReceived(de);

                ValState.ErrorCode = 0;
                ValState.ReturnCode = 0;

                if (LastReceived.getData()[0] == 0x14) {
                    ValState.State = VState.Idle;
                    ValState.ErrorCode = 0;
                    ValState.Bill = -1;
                }

                if (LastReceived.getData()[0] == 0x19) {
                    ValState.State = VState.UnitDisable;
                    ValState.ErrorCode = 0;
                    ValState.Bill = -1;
                }

                if (LastReceived.getData()[0] == 0x10 || LastReceived.getData()[0] == 0x11 || LastReceived.getData()[0] == 0x12) {
                    ValState.State = VState.PowerUp;
                    ValState.Bill = -1;
                    ValState.ErrorCode = LastReceived.getData()[0];
                }

                if (LastReceived.getDataSize() == 2 && (LastReceived.getData()[0] == 0x80 || LastReceived.getData()[0] == 0x81 || LastReceived.getData()[0] == 0x82)) {
                    if (LastReceived.getData()[0] == 0x81) {
                        ValState.State = VState.Stacked;
                    } else if (LastReceived.getData()[0] == 0x80) {
                        ValState.State = VState.Holding;
                    } else {
                        ValState.State = VState.Returned;
                    }

                    int recievedBill = LastReceived.getData()[1];

                    ValState.Bill = recievedBill;

                    if (ValState.State == VState.Holding) {
                        pendingBill = recievedBill;
                    }

                    //int Amount = 0;

                    //switch (recievedBill)
                    //{
                    //    case 0:
                    //        {
                    //            Amount += 1;
                    //            break;
                    //        }
                    //    case 1:
                    //        {
                    //            Amount += 2;
                    //            break;
                    //        }
                    //    case 2:
                    //        {
                    //            Amount += 5;
                    //            break;
                    //        }
                    //    case 3:
                    //        {
                    //            Amount += 10;
                    //            break;
                    //        }
                    //    case 4:
                    //        {
                    //            Amount += 20;
                    //            break;
                    //        }
                    //    case 5:
                    //        {
                    //            Amount += 50;
                    //            break;
                    //        }
                    //    case 6:
                    //        {
                    //            Amount += 100;
                    //            break;
                    //        }
                    //    case 7:
                    //        {
                    //            Amount += 200;
                    //            break;
                    //        }
                    //}
                    //ValState.Bill = Amount;
                }

                if (LastReceived.getData()[0] == 0x42) {
                    ValState.State = VState.CassetteGet;
                    ValState.Bill = -1;
                }

                if (LastReceived.getData()[0] == 0x13) //Initialize
                {
                    ValState.State = VState.CassetteSet;
                    PCStruct.setAllowBills(false);
                    ValState.Bill = -1;
                }

                if (LastReceived.getData()[0] == 0x15) {
                    ValState.State = VState.Accepting;
                    ValState.Bill = -1;
                }

                if (LastReceived.getData()[0] == 0x17) {
                    ValState.State = VState.Staking;
                }

                if (LastReceived.getDataSize() == 2 && LastReceived.getData()[0] == 0x1C) {
                    ValState.State = VState.Rejecting;
                    ValState.ReturnCode = LastReceived.getData()[1];
                }

                if (LastReceived.getData()[0] == 0x18) {
                    ValState.State = VState.Returning;
                    ValState.ReturnCode = 0;
                }

                if (LastReceived.getData()[0] == 0x41 || LastReceived.getData()[0] == 0x43 || LastReceived.getData()[0] == 0x44 || LastReceived.getData()[0] == 0x47) {
                    ValState.State = VState.Error;
                    ValState.Bill = -1;

                    if (LastReceived.getDataSize() == 1) {
                        ValState.ErrorCode = LastReceived.getData()[0];
                    } else {
                        ValState.ErrorCode = LastReceived.getData()[1];
                    }
                }

                if ((!(LastReceived.getData() != null && LastReceived.getData().length == 1 && LastReceived.getData()[0] == 0))) {
                    SendCommand(LastReceived.getACKBytesArray());
                }

                //if (PCStruct.PhoneNumber != "")
                //{
                //Log.Log("AmountReceive", de.Data + "; " + ValState.ToString() + "; " + PCStruct.PhoneNumber, 0);
                //}

                if (PCStruct.getAllowBills() && !ValState.equals(OldValState)) {
                    hardware.Log("AmountReceive", de + "; " + ValState.toString(), LogType.Information);
                }

                if (this.cassetteFullError) {
                    ValState.ErrorCode = 0x41; //Drop Cassette Full Error, Must Exist In errorDictionary (65 In Decimal)
                    ValState.Bill = -1;
                    ValState.State = VState.Error;
                }
            } else {
                throw (new RuntimeException());
            }

            ValState.PollError = false;
        } catch (RuntimeException ex) {
            ValState.PollError = true;
            ValState.Bill = -1;
            ValState.ErrorCode = -2;

            //hardware.Log("CashCodeBytesLogger", "GetPoll(): " + ex.Message, LogType.Error);
        }
    }

    private void SendCommand(String Command) throws Exception {
//        DataEventArgs de = new DataEventArgs();
//        DateTime DT = DateTime.now();
//        de.setData(DT.toStringWithMsc("HH-mm-ss") + ":\t" + Command);
        //onCommandReceived(null, de);

        if (Command.equals("StartAmount")) {
            //Create Bytes Structure For Enable Bill Types
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: byte[] allowedCashBytesToSend = new byte[6];
            byte[] allowedCashBytesToSend = new byte[6];
//            BitArray allowedCashBitsFlags = new BitArray(48, false);
            BitSet allowedCashBitsFlags = BitSet.valueOf(new byte[allowedCashBytesToSend.length]);

            //Example For Enable All Bill Types (0-23) And Escrow:
            //
            //Bill Type      Byte 1              Byte 2          byte 3
            //Position:  7 6 5 4 3 2 1 0   7 6 5 4 3 2 1 0   7 6 5 4 3 2 1 0
            //Values:    0 0 0 0 0 0 0 0   0 0 0 0 0 0 0 0   1 1 1 1 1 1 1 1
            //
            //Escrow         Byte 4              Byte 5          byte 6
            //Position:  7 6 5 4 3 2 1 0   7 6 5 4 3 2 1 0   7 6 5 4 3 2 1 0
            //Values:    0 0 0 0 0 0 0 0   0 0 0 0 0 0 0 0   1 1 1 1 1 1 1 1
            
            try {
                allowedCashIDDictionaryLock.lock();
                for (int code : allowedCashIDDictionary.values()) {
                    //BitArray 0 index is least significant bit
                    allowedCashBitsFlags.set(16 + code, true); //Bill Type Enable
                    allowedCashBitsFlags.set(40 + code, true); //Escrow Enable
                }
            } finally {
                allowedCashIDDictionaryLock.unlock();
            }
                    
            allowedCashBytesToSend=allowedCashBitsFlags.toByteArray();
//            allowedCashBitsFlags.toByteArray()CopyTo(allowedCashBytesToSend, 0);

            //CCMessage mgs = new CCMessage(mADDR, 0x34, new byte[] { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff }, 6);
            CCMessage mgs = new CCMessage((byte) 0x34, allowedCashBytesToSend, 6);
            SendCommand(mgs.toBytesArray());
            ClearPortBuffer();
        }

        if (Command.equals("StopAmount")) {
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: CCMessage mgs = new CCMessage(mADDR, 0x34, new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 6);
            CCMessage mgs = new CCMessage((byte) 0x34, new byte[]{0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 6);
            SendCommand(mgs.toBytesArray());
            ClearPortBuffer();
        }

        if (Command.equals("Stack")) {
            CCMessage mgs = new CCMessage((byte) 0x35, null, 0);
            SendCommand(mgs.toBytesArray());
            ClearPortBuffer();
        }

        if (Command.equals("Return")) {
            CCMessage mgs = new CCMessage((byte) 0x36, null, 0);
            SendCommand(mgs.toBytesArray());
            ClearPortBuffer();
        }

        if (Command.equals("RESET")) {
            CCMessage mgs = new CCMessage((byte) 0x30, null, 0);
            SendCommand(mgs.toBytesArray());
            ClearPortBuffer();
        }

        if (Command.equals("DENOM")) {
            CCMessage mgs = new CCMessage((byte) 0x41, null, 0);
            SendCommand(mgs.toBytesArray());
            //ClearPortBuffer();
        }

        if (Command.equals("IDENTIFICATION")) {
            CCMessage mgs = new CCMessage((byte) 0x37, null, 0);
            SendCommand(mgs.toBytesArray());
        }
    }

    private void ClearPortBuffer() throws Exception {
        try {
            if (CashComPort.ReadWholeLock().length> 0) {


                CCMessage LastReceived = new CCMessage();
                SendCommand(LastReceived.getACKBytesArray());
            }
        } catch (RuntimeException ex) {
        }
    }

    //C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: private void SendCommand(byte[] cmd)
    private void SendCommand(byte[] cmd) throws Exception {
        synchronized (PortLockObject) {
            if (!CashComPort.isOpend()) {
                return;
            }
            CashComPort.Write(cmd, 0, cmd.length);
        }

        Thread.sleep(50);
    }


//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#region Helper Types

    private static class ValidatorState {
        public VState State = VState.values()[0];
        public int Bill;
        public boolean AllowBills;
        public int ErrorCode;
        public boolean PollError = false;
        public boolean AllowError = false;
        public int ReturnCode;

        @Override
        public boolean equals(Object obj) {
            ValidatorState OldSt = (ValidatorState) obj;

            if (OldSt.State == State && OldSt.Bill == Bill && OldSt.AllowBills == AllowBills && OldSt.ErrorCode == ErrorCode && OldSt.PollError == PollError && OldSt.AllowError == AllowError && OldSt.ReturnCode == ReturnCode) {
                return true;
            }

            return false;
        }

        @Override
        public int hashCode() {
            return super.hashCode();
        }

        @Override
        public String toString() {
            String Returnstr = "Bill = " + Bill + "; AllowBills = " + AllowBills + "; ErrorCode = " + ErrorCode + ", 0x" + Integer.toHexString(ErrorCode).toUpperCase() + "; PollError = " + PollError + "; AllowError = " + AllowError + "; ReturnCode = " + ReturnCode + ", 0x" + Integer.toHexString(ReturnCode).toUpperCase() + "; State = " + State;

            return Returnstr;
        }

        public final void Copy(ValidatorState ValSt) {
            State = ValSt.State;
            Bill = ValSt.Bill;
            AllowBills = ValSt.AllowBills;
            ErrorCode = ValSt.ErrorCode;
            PollError = ValSt.PollError;
            AllowError = ValSt.AllowError;
            ReturnCode = ValSt.ReturnCode;
        }
    }

    private enum VState {
        Idle, //Allow Cash
        UnitDisable, //Deny Cash
        Accepting,
        Holding,
        Staking,
        Returned,
        Returning,
        Rejecting,
        Stacked,
        Error,
        CassetteGet,
        CassetteSet,
        PowerUp;

        public static final int SIZE = java.lang.Integer.SIZE;

        public int getValue() {
            return this.ordinal();
        }

        public static VState forValue(int value) {
            return values()[value];
        }
    }

    //C# TO JAVA CONVERTER WARNING: Java does not allow user-defined value types. The behavior of this class will differ from the original:
//ORIGINAL LINE: private struct PCCommandSturct
    private final static class PCCommandSturct {
        public PCCommandSturct() {
        }

        public PCCommandSturct(boolean pStartStopAmount) {
            mStartStopAmount = pStartStopAmount;
            mPhoneNumber = "";
        }

        private boolean mStartStopAmount;

        public boolean getAllowBills() {
            return mStartStopAmount;
        }

        public void setAllowBills(boolean value) {
            mStartStopAmount = value;
        }

        private String mPhoneNumber;

        public String getPhoneNumber() {
            return mPhoneNumber;
        }

        public void setPhoneNumber(String value) {
            mPhoneNumber = value;
        }

        public PCCommandSturct clone() {
            PCCommandSturct varCopy = new PCCommandSturct();

            varCopy.mStartStopAmount = this.mStartStopAmount;
            varCopy.mPhoneNumber = this.mPhoneNumber;

            return varCopy;
        }
    }



    private static class CashCodeHistory {
        private List<String> HistoryList;

        public CashCodeHistory() {
            HistoryList = new ArrayList<>();
        }

        public final void Add(String Value) {
            try {
                HistoryList.add(0, Value);

                while (HistoryList.size() > 240) {
                    HistoryList.remove(HistoryList.size() - 1);
                }
            } catch (RuntimeException ex) {

            }
        }

        public final void Remove(String Value) {
            HistoryList.remove(Value);
        }

        @Override
        public String toString() {
            String StrReturn = "";
            try {
                for (String Str : HistoryList) {
                    StrReturn += Str;
                }
            } catch (RuntimeException ex) {

            }

            return StrReturn;
        }
    }

    private class CCMessage {
        private final int POLYNOMINAL = 0x08408;
        private byte sync = 0x02;
        private byte addr = 0x03;
        private byte lng;
        private byte cmd;
        private byte[] data;
        private int dataSize;
        private byte[] crc;
        private boolean complete = false;
        private int bytesComplete = 0;

        public CCMessage(byte cmd, byte[] data, int dataSize) {
            this.lng = (byte) (dataSize + 6);
            this.cmd = cmd;
            this.data = data;
            this.dataSize = dataSize;
            complete = true;
            bytesComplete = lng;
        }

        public CCMessage() {
            this.addr = 0;
            this.lng = (byte) 0;
            this.cmd = 0;
            this.data = null;
            this.dataSize = 0;
            complete = false;
            bytesComplete = 0;
        }

        private short getCrc16(byte[] bytes, int size) {
            int crc16 = 0;

            for (int i = 0; i < size; i++) {
                crc16 ^= bytes[i] & 0xFF;
                for (int j = 0; j < 8; j++) {
                    if ((crc16 & 0x1) != 0) {
                        crc16 >>>= 1;
                        crc16 ^= POLYNOMINAL;
                    } else {
                        crc16 >>>= 1;
                    }
                }
            }
            return (short) crc16;
        }

        private short getCrc16() {
            byte[] arr = new byte[dataSize + 6];
            arr[0] = sync;
            arr[1] = addr;
            arr[2] = lng;
            arr[3] = cmd;
            if (dataSize > 0) {
                System.arraycopy(data, 0, arr, 4, dataSize);
            }
            return getCrc16(arr, dataSize + 4);
        }

        public byte[] toBytesArray(byte addr, byte cmd, byte[] data, int dataSize) {
            byte[] arr = new byte[dataSize + 6];
            arr[0] = sync;
            arr[1] = addr;
            arr[2] = (byte) (dataSize + 6);
            arr[3] = cmd;
            if (dataSize > 0) {
                System.arraycopy(data, 0, arr, 4, dataSize);
            }
            short crc16 = getCrc16(arr, dataSize + 4);
            byte[] crc16bytes = BitConverter.GetBytes(crc16);
            arr[dataSize + 4] = crc16bytes[1];
            arr[dataSize + 5] = crc16bytes[0];
            return arr;
        }

        public byte[] toBytesArray() {
            return toBytesArray(addr, cmd, data, dataSize);
        }

        public byte[] getACKBytesArray() {
            return toBytesArray(addr, (byte) 0, null, 0);
        }

        public byte[] fromByteArray(byte[] bytes) {
            if (complete) {
                return bytes;
            }
            for (int i = 0; i < bytes.length; i++) {
                if (bytesComplete > 2 && bytesComplete == lng) {
                    complete = true;
                    byte[] tmp = new byte[bytes.length - i];
                    for (int j = i; j < bytes.length; j++) {
                        tmp[j - i] = bytes[j];
                    }
                    return tmp;
                }
                if (bytesComplete == 0) {
                    sync = bytes[i];
                    bytesComplete++;
                    continue;
                }
                if (bytesComplete == 1) {
                    addr = bytes[i];
                    bytesComplete++;
                    continue;
                }
                if (bytesComplete == 2) {
                    lng = bytes[i];
                    bytesComplete++;
                    dataSize = 0;
                    data = new byte[lng - 5];
                    continue;
                }
//                if (bytesComplete == 3) {
//                    cmd = bts[i];
//                    bytesComplete++;
//                    continue;
//                }
                if (bytesComplete > 2 && bytesComplete < (lng - 2)) {
                    data[dataSize] = bytes[i];
                    dataSize++;
                    bytesComplete++;
                    continue;
                }
                if (bytesComplete > 2 && bytesComplete == (lng - 2)) {
                    crc = new byte[2];
                    crc[0] = bytes[i];
                    bytesComplete++;
                    continue;
                }
                if (bytesComplete > 2 && bytesComplete == (lng - 1)) {
                    crc[1] = bytes[i];
                    bytesComplete++;
                    continue;
                }
            }
            if (bytesComplete > 2 && bytesComplete == lng) {
                complete = true;
            }
            byte[] tmp2 = new byte[0];
            return tmp2;
        }

        public byte getAddr() {
            return addr;
        }

        public byte getLng() {
            return lng;
        }

        public byte getCmd() {
            return cmd;
        }

        public byte[] getData() {
            return data;
        }

        public int getDataSize() {
            return dataSize;
        }

        public byte[] getCrc() {
            return crc;
        }

        public boolean isComplete() {
            return complete;
        }

        public int getBytesComplete() {
            return bytesComplete;
        }
        
        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder("{ ");
            sb.append(String.format("sync: 0x%02x, addr: 0x%02x, lng: 0x%02x, cmd: 0x%02x", sync, addr, lng, cmd));
            if (dataSize > 0) {
                sb.append(", data: [ ");
                for (int i = 0; i < dataSize; i++) {
                    sb.append(String.format("0x%02x", data[i]));
                    if (i < dataSize - 1) {
                        sb.append(", ");
                    }
                }
                sb.append(" ]");
            }
            sb.append(", crc: ").append(String.format("0x%04x", getCrc16()));
            sb.append(" }");
            return sb.toString();
        }
    }
}
