package com.pay.helper;

/**
 * Created by dito on 3/31/2017.
 */
public class StringHelper {
    public static boolean isStringNullOrWhiteSpace(String value) {
        if (value == null){
            return true;
        }

        for (Character chars : value.toCharArray()){
            if (!Character.isWhitespace(chars)){
                return false;
            }
        }

        return true;
    }
}
