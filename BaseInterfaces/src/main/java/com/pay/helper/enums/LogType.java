package com.pay.helper.enums;

/**
 * Created by dito on 3/31/2017.
 */
public enum LogType {


    Information(0),
    Warning(1),
    Error(2);
    private int value;

    LogType(int value) {
        this.value = value;
    }
}
