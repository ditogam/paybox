package com.pay.helper;

import java.util.Base64;

/**
 * Created by dito on 4/7/2017.
 */
public class Convert {
    public static String ToBase64String(byte[] bytesForSend) {
        return Base64.getEncoder().encodeToString(bytesForSend);
    }
}
