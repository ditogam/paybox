package com.pay.helper;

/**
 * Created by dito on 4/4/2017.
 */
public class IntegerHelper {
    public static Integer TryParse(String str, int default_value){
        try {
            return Integer.valueOf(str);
        }catch (Exception e){
            return default_value;
        }
    }
}
