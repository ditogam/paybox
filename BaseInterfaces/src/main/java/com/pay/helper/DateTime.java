package com.pay.helper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by dito on 4/5/2017.
 */
public class DateTime {
    private Calendar calendar;

    public static DateTime now() {
        return new DateTime(new GregorianCalendar());
    }

    public DateTime(Calendar calendar) {
        this.calendar = calendar;
    }

    public int getMillisecond() {
        return calendar.get(Calendar.MILLISECOND);
    }

    public String toString(String format) {
        return new SimpleDateFormat(format).format(calendar.getTime());
    }

    public String toStringWithMsc(String format) {
        return toString(format)+ "-" + this.getMillisecond();
    }

    public long ToBinary() {
        return this.calendar.getTimeInMillis();
    }
}
