package com.pay.helper;

import com.pay.helper.enums.LogType;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Hashtable;

/**
 * Created by dito on 3/31/2017.
 */
public class Logger {
    private static Hashtable FacilityHt = new Hashtable();

    private static String logDir = "";

    public static String getLogDir() {
        return StringHelper.isStringNullOrWhiteSpace(logDir) ? AppDomain.getBaseDirectory() + "\\Logs\\" : logDir;
    }

    public static void setLogDir(String logDir) {
        Logger.logDir = logDir;
    }


    public static void Log(String logFacility, String logValue, LogType type) {
        if (!(new java.io.File(getLogDir())).isDirectory()) {
            (new java.io.File(getLogDir())).mkdirs();
        }
        try {
            LogValue par = new LogValue();
            par.setFacility(logFacility);
            par.setType(type);
            par.setValue(logValue);
            par.setLogDirectory(getLogDir());
            Thread t = new Thread() {
                void run() {
                    LogThread();
                }
            };
            t.Start(par);
        } catch (java.lang.Exception e) {
        }
    }

    public static void Log(String logDir, String logFacility, String logValue, LogType type) {
        if (!(new java.io.File(logDir)).isDirectory()) {
            (new java.io.File(logDir)).mkdirs();
        }
        try {
            final LogValue par = new LogValue();
            par.setFacility(logFacility);
            par.setType(type);
            par.setValue(logValue);
            par.setLogDirectory(logDir);
            Thread t = new Thread(() -> LogThread(par));
        } catch (java.lang.Exception e) {
        }
    }

    private static void LogThread(Object par) {
        java.io.OutputStreamWriter writer = null;
        try {
            if (par == null || !(par instanceof LogValue)) {
                return;
            }

            LogValue parameter = (LogValue) ((par instanceof LogValue) ? par : null);

            if (!FacilityHt.containsKey(parameter.getFacility())) {
                FacilityHt.put(parameter.getFacility(), new Object());
            }

            synchronized (FacilityHt.get(parameter.getFacility())) {
                String logValue = parameter.toString();
                String filename = parameter.getLogDirectory() + parameter.getFacility() + "_" + String.format("%d", java.time.LocalDateTime.now()) + ".log";
                if (!(new java.io.File(filename)).isFile()) {
                    File.Create(filename).Close();
                }
//C# TO JAVA CONVERTER WARNING: The java.io.OutputStreamWriter constructor does not accept all the arguments passed to the System.IO.StreamWriter constructor:
//ORIGINAL LINE: writer = new StreamWriter(filename, true);
                writer = new java.io.OutputStreamWriter(new FileOutputStream(filename));
                writer.write(logValue + System.lineSeparator());
                writer.flush();
                writer.close();
            }
        } catch (java.lang.Exception e) {
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (java.lang.Exception e2) {
                }
            }
        }

    }
}
