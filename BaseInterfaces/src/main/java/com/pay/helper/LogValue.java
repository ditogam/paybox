package com.pay.helper;

import com.pay.helper.enums.LogType;

import java.util.Date;

/**
 * Created by dito on 3/31/2017.
 */
public class LogValue {
    private String logDirectory;
    public String facility ;
    public String value ;
    public LogType type ;
    private Date _logDate =new Date();

    public String getLogDirectory() {
        return logDirectory;
    }

    public void setLogDirectory(String logDirectory) {
        this.logDirectory = logDirectory;
    }

    public String getFacility() {
        return StringHelper.isStringNullOrWhiteSpace(facility)?Logger.getLogDir():logDirectory;
    }

    public void setFacility(String facility) {

        this.facility = facility;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public LogType getType() {
        return type;
    }

    public void setType(LogType type) {
        this.type = type;
    }
}
