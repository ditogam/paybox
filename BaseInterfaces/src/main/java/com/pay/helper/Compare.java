package com.pay.helper;

import java.util.Arrays;

/**
 * Created by dito on 4/3/2017.
 */
public class Compare {

    public static boolean compare(int[] source, int[] dest) {
        int[] sourceArr = Arrays.copyOf(source, source.length);
        int[] destArr = Arrays.copyOf(dest, dest.length);
        Arrays.sort(sourceArr);
        Arrays.sort(destArr);
        return Arrays.equals(sourceArr, destArr);
    }
}
