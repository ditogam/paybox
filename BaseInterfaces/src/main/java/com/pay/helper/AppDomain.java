package com.pay.helper;

/**
 * Created by dito on 3/31/2017.
 */
public class AppDomain {
    public static String getBaseDirectory() {
        return System.getProperty("user.dir");
    }

    public static AppDomain CurrentDomain;
    public String BaseDirectory = getBaseDirectory();

    static {
        CurrentDomain = new AppDomain();
    }
}
