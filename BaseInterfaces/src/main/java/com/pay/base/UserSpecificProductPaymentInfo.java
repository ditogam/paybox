package com.pay.base;

/**
 * Created by dito on 4/5/2017.
 */
public class UserSpecificProductPaymentInfo
{
    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [DataMember] public int ProductID {get;set;}
    private int ProductID;
    public final int getProductID()
    {
        return ProductID;
    }
    public final void setProductID(int value)
    {
        ProductID = value;
    }

    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [DataMember] public string Name {get;set;}
    private String Name;
    public final String getName()
    {
        return Name;
    }
    public final void setName(String value)
    {
        Name = value;
    }

    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [DataMember] public int Type {get;set;}
    private int Type;
    public final int getType()
    {
        return Type;
    }
    public final void setType(int value)
    {
        Type = value;
    }

    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [DataMember] public double Value {get;set;}
    private double Value;
    public final double getValue()
    {
        return Value;
    }
    public final void setValue(double value)
    {
        Value = value;
    }
}