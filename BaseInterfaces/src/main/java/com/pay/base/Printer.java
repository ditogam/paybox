package com.pay.base;

import com.google.common.primitives.Bytes;
import com.pay.base.devicestate.DeviceState;
import com.pay.base.devicestate.PrinterDeviceState;
import com.pay.base.devicestate.enums.PrinterState;
import com.pay.base.event.EventManager;
import com.pay.base.event.types.Delegates;
import com.pay.base.event.types.DeviceChangedEvent;
import com.pay.helper.*;
import com.pay.helper.enums.LogType;
import com.pay.io.serial.SerialPort;
import com.sun.javafx.binding.StringFormatter;

import javax.activation.CommandInfo;
import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

/**
 * Created by dito on 3/31/2017.
 */
public abstract class Printer {


    public static final int Timeout_Infinite = -1;

    public static class CommandInfo {
        //Parametrized Command Names Starts With "P-" Simbols,
        //ContentFormat Type Commands (That Are Used In TaggedText) Are In Tags (<command>, </command>)

        //Order In Wich Commands Must Send To Printer

        //Parameter Values Separated By "~D~"

        //NonParameterized - byte, Parameterized - 256 {N}(Numeric), 257 {S}(String), 258 {B}(Bitmap)

        public CommandInfo(CommandType type, int orderNumber, String name, ArrayList<Short> shortsList, String parameterValues) {
            Type = type;
            OrderNumber = orderNumber;
            setName(name);
            setShortsList(shortsList);
            setParameterValues(parameterValues);
        }

        private CommandType Type = CommandType.values()[0];

        public final CommandType getType() {
            return Type;
        }

        private int OrderNumber;

        public final int getOrderNumber() {
            return OrderNumber;
        }

        private String Name = "";

        public final String getName() {
            return Name;
        }

        private ArrayList<Short> ShortsList = new ArrayList<Short>();

        public final ArrayList<Short> getShortsList() {
            return ShortsList;
        }

        private String ParameterValues = "";

        public final String getParameterValues() {
            return ParameterValues;
        }

        public final void setParameterValues(String value) {
            ParameterValues = value;
        }

        public void setType(CommandType type) {
            Type = type;
        }

        public void setOrderNumber(int orderNumber) {
            OrderNumber = orderNumber;
        }

        public void setName(String name) {
            Name = name;
        }

        public void setShortsList(ArrayList<Short> shortsList) {
            ShortsList = shortsList;
        }
    }

    public enum PrintMode {
        Default,
        Textual,
        Graphical;

        public static PrintMode forValue(int value) {
            return values()[value];
        }
    }

    public enum CommandType {
        State,
        ContentFormat,
        Initialize,
        PrintPrepare,
        PaperPresent;

        public static final int SIZE = java.lang.Integer.SIZE;

        public int getValue() {
            return this.ordinal();
        }

        public static CommandType forValue(int value) {
            return values()[value];
        }
    }

    protected final String tempFilePath = System.getProperty("java.io.tmpdir") + "\\PrinterTemp";

    protected boolean autoPaperError = false;
    //Informs about how to get paper errors ('Paper end', 'Paper near end'), from printer or from program counters

    protected Barcode barcodeGenerator = new Barcode(); //For graphical mode
    protected int baudRate;
    protected int bufferSize = 0; //For texual mode (printer buffer size)
    //<Key: Command Name, Value: CommandInfo>
    protected HashMap<String, CommandInfo> commandDictionary = new HashMap<String, CommandInfo>();
    protected boolean configIsValid = false;
    protected PrinterDeviceState currentPrinterDeviceState;
    protected Semaphore currentPrinterDeviceStateLock = new Semaphore(1);

    protected HashMap<String, String> customTaggesDictionary = new HashMap<String, String>() {{
        put("<P-BarCode>", "</P-BarCode>");
        put("<P-Image>", "</P-Image>");
    }};


    //<Key: Device Specific Code, Value: Error Info>
    protected HashMap<Integer, Error> errorDictionary = new HashMap<Integer, Error>();
    protected int fontSizeLarge = 0; //For graphical mode
    protected int fontSizeNormal = 0; //For graphical mode
    protected IHardware hardware;
    protected boolean initialized;
    protected AtomicLong lastPrintTimeInBinary = new AtomicLong();
    protected int lineCount;
    protected int lineHeightInPixels = 0; //For graphical mode, temporarly use it for minimum paper height in pixels
    protected AtomicInteger monitoringCycleCount = new AtomicInteger();
    protected volatile boolean monitorPrinterState = true;
    protected String paperName = "TG2480-H Roll"; //Temp...  //For print with windows driver and PrintDocument class
    protected int paperWidthInPixels = 0; //For graphical mode
    protected String payboxDeviceTypeName = "";

    protected int portionedBitmapBytesSize = 0;
    //For graphical mode (must fit with bitmap bytes structure (regard bytes direction, multiple of width/8...))

    protected int portionPrintingIntervalInMiliseconds = 0;
    //Interval beatween portion bytes sending, this is for max bytes size send, less bytes size will have adequately less inreval

    protected Object printerLock = new Object();
    protected Thread printerStateMonitoringThread;

    protected PrintMode printMode = PrintMode.values()[0];
    protected int quittanceCountLimit;

    protected SerialPort serialPort = new SerialPort();
    protected String serialPortName;

    protected int stateMonitoringIntervalInMiliSeconds;
    //<Key: Logo Name, Value: Index In Printer> (this logo names and existing bitmap file names in resources must be unique)
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: protected Dictionary<string, byte> storedLogosDictionary = new Dictionary<string, byte>();
    protected HashMap<String, Byte> storedLogosDictionary = new HashMap<String, Byte>();
    protected int symbolCount;
    protected int symbolWidth = 1;

    protected String typeName = "";
    protected boolean usesHardwareHandshake = false;


    protected final boolean InitializePrinter() {
        boolean result = false;

        try {
            synchronized (currentPrinterDeviceStateLock) {
                currentPrinterDeviceState = new PrinterDeviceState(this.getClass(), payboxDeviceTypeName, new HashMap<Integer, Error>(), PrinterState.Uninitialized);
            }

//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: var initializeCommandsBytesList = GetSequentialCommandsByType(CommandType.Initialize, 0, true);
            ArrayList<Byte> initializeCommandsBytesList = GetSequentialCommandsByType(CommandType.Initialize, 0, true);

            if (!initializeCommandsBytesList.isEmpty()) {
                synchronized (printerLock) {
                    if (serialPortName.startsWith("COM")) {
                        serialPort.Open();
                        serialPort.Write(ByteLists.toArray(initializeCommandsBytesList), 0, initializeCommandsBytesList.size());
                        serialPort.close();
                    } else {
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: RawPrinterHelper.SendBytesToPrinter(serialPortName, initializeCommandsBytesList.ToArray());
                        RawPrinterHelper.SendBytesToPrinter(serialPortName, ByteLists.toArray(initializeCommandsBytesList));
                    }
                }
            }

            monitorPrinterState = true;
            printerStateMonitoringThread = new Thread(() -> PrinterStateMonitoring());
            printerStateMonitoringThread.start();

            result = true;
        } catch (Exception ex) {
            hardware.Log(typeName, "InitializePrinter(): " + ex.getMessage(), LogType.Error);
            hardware.LogException(ex);
        }

        return result;
    }

    protected abstract DeviceState GetStateForced();

    protected final void PrinterStateMonitoring() {
        while (monitorPrinterState) {
            PrinterDeviceState stateArgs = (PrinterDeviceState) GetStateForced();

            synchronized (currentPrinterDeviceStateLock) {
                if (!stateArgs.equals(currentPrinterDeviceState)) {
                    OnStateChanged(null, stateArgs);

                    currentPrinterDeviceState = stateArgs;
                }
            }
            monitoringCycleCount.incrementAndGet();
            try {
                Thread.sleep(stateMonitoringIntervalInMiliSeconds);
            } catch (Exception e) {

            }
        }
    }

    private EventManager<DeviceState> stateChangedEventHandler = new EventManager<>();

    public final void OnStateChanged(Object sender, DeviceState stateArgs) {
        if (stateChangedEventHandler != null) {
            hardware.Log("Printer", "OnStateChanged called", LogType.Information);
            stateChangedEventHandler.invoke(new DeviceChangedEvent(sender, stateArgs));
        }
    }

    public final void PrintAsynchronously(String taggedText, PrintMode printMode, int lineFeedCount, boolean fullCut) {
        //Execute in STA apartment thread, need for WebBrowser ActiveX component, wich we are using in graphical mode print
        final Object[] objArr = {taggedText, printMode, lineFeedCount, fullCut};
        Thread printThread = new Thread(() -> PrintProxy(objArr));
// TODO SetApartmentState
//        printThread.SetApartmentState(ApartmentState.STA);
        printThread.start();

        //Obsolate
        //PrintDelegate printDelagate = new PrintDelegate(this.Print);
        //printDelagate.BeginInvoke(taggedText, printMode, lineFeedCount, fullCut, null, null);
    }

    private void PrintProxy(Object obj) {
        Object[] objArr = (Object[]) obj;
        Print((String) objArr[0], PrintMode.forValue((Integer) objArr[1]), (Integer) objArr[2], (Boolean) objArr[3]);
    }


    public final PrinterDeviceState PrintSynchronously(String taggedText, PrintMode printMode, int lineFeedCount, boolean fullCut) throws Exception {
        return PrintSynchronously(taggedText, printMode, lineFeedCount, fullCut, Timeout_Infinite);
    }

    //C# TO JAVA CONVERTER NOTE: Java does not support optional parameters. Overloaded method(s) are created above:
//ORIGINAL LINE: public PrinterDeviceState PrintSynchronously(string taggedText, PrintMode printMode, int lineFeedCount, bool fullCut, int timeoutIntervalInMiliseconds = Timeout.Infinite)
    public final PrinterDeviceState PrintSynchronously(String taggedText, PrintMode printMode, int lineFeedCount, boolean fullCut, int timeoutIntervalInMiliseconds) throws Exception {
        return null;
        /*
        TODO PrintSynchronously
        if (this.printMode == PrintMode.Graphical) //Print graphical mode printers in STA apartment thread
        {
            //Execute in STA apartment thread, need for WebBrowser ActiveX component, wich we are using in graphical mode print
            Object[] objArr = {taggedText, printMode, lineFeedCount, fullCut};
            Thread printThread = new Thread(() -> PrintProxy(objArr));

            printThread.SetApartmentState(ApartmentState.STA);
            printThread.start();

            while (printThread.isAlive() && (timeoutIntervalInMiliseconds == -1 || timeoutIntervalInMiliseconds > 0)) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if (timeoutIntervalInMiliseconds != Timeout_Infinite) {
                    timeoutIntervalInMiliseconds -= 100;
                }

                hardware.Log(typeName, "PrintSynchronously(): Still printing...", LogType.Warning);
            }
        } else {
            PrintDelegate printDelagate = (String taggedText, PrintMode printMode, int lineFeedCount, boolean fullCut) -> Print(taggedText, printMode, lineFeedCount, fullCut);
//C# TO JAVA CONVERTER TODO TASK: There is no equivalent to implicit typing in Java:
            var printIAsyncResult = printDelagate.BeginInvoke(taggedText, printMode, lineFeedCount, fullCut, null, null);

            if (printIAsyncResult.AsyncWaitHandle.WaitOne(timeoutIntervalInMiliseconds)) {
                printDelagate.EndInvoke(printIAsyncResult);
                hardware.Log(typeName, "PrintSynchronously(): Ended", LogType.Information);
            } else {
                hardware.Log(typeName, "PrintSynchronously(): Timedout, throwing exception, interval: " + timeoutIntervalInMiliseconds, LogType.Error);
                throw new TimeoutException();
            }
        }

        //Obsolate
        //this.Print(taggedText, printMode, lineFeedCount, fullCut);

        return (PrinterDeviceState) GetState(true);
        */
    }

    protected void Print(String taggedText, PrintMode printMode, int lineFeedCount, boolean fullCut) {
        try {
            if (initialized) {
                int portionedBytesSize = 0;
                boolean isEmbeddedBitmapBytes = false;
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: var commandBytesLists = new List<List<byte>>();
                ArrayList<ArrayList<Byte>> commandBytesLists = new ArrayList<ArrayList<Byte>>();

                //Text is converted from text to pixel lines and stored in the line buffer when an LF is received,
                //We must add LF (if ther is not) in the end of the last line to print last line
                if (typeName.equals("PrinterSwecoin") && !taggedText.substring(taggedText.length() - 1).equals("\n")) {
                    taggedText += "\n";
                }

                //Get commands bytes lists by TranslateText (Textual) or GetBitmapBytes (Graphical)
                //This Functions may return more than one bytes list, this means thet we must send bytes to printer portionaly
                if (printMode == PrintMode.Textual || printMode == PrintMode.Default && this.printMode == PrintMode.Textual) {
                    commandBytesLists = TranslateText(taggedText, bufferSize);
                    portionedBytesSize = bufferSize;
                } else {
                    commandBytesLists = GetBitmapBytes("<P-Bitmap>", taggedText);
                    portionedBytesSize = portionedBitmapBytesSize;
                }

                hardware.Log(typeName, "Print(): CommandBytesLists Count: " + commandBytesLists.size(), LogType.Information);

                synchronized (printerLock) {
                    //First send 'PrintPrepare', then content by portion, then 'PaperPresent' commands bytes,
                    //Iterate through commandBytesLists and additionaly (-1 and last index) for 'PrintPrepare' and 'PaperPresent' commands
                    for (int i = -1; !commandBytesLists.isEmpty() && i <= commandBytesLists.size(); i++) {
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: var bytesForSend = new byte[0];
                        byte[] bytesForSend = new byte[0];

                        if (i == -1) {
                            bytesForSend = ByteLists.toArray(GetSequentialCommandsByType(CommandType.PrintPrepare, lineFeedCount, fullCut));
                        } else if (i < commandBytesLists.size()) {
                            //Embedded bitmap bytes sign
                            if (commandBytesLists.get(i) == null) {
                                isEmbeddedBitmapBytes = true;
                                continue;
                            }

                            bytesForSend = ByteLists.toArray(commandBytesLists.get(i));
                        } else if (i == commandBytesLists.size()) {
                            bytesForSend = ByteLists.toArray(GetSequentialCommandsByType(CommandType.PaperPresent, lineFeedCount, fullCut));
                        }

                        if (bytesForSend.length != 0) {
                            if (serialPortName.startsWith("COM")) {
                                serialPort.Open();
                                serialPort.Write(bytesForSend, 0, bytesForSend.length);
                                serialPort.close();
                            } else {
                                RawPrinterHelper.SendBytesToPrinter(serialPortName, bytesForSend);
                                //Print through windows driver, not used yet
                            }

                            hardware.Log(typeName, "Print(): " + Convert.ToBase64String(bytesForSend), LogType.Information);

                            //Embedded bitmaps bytes size that does not participate in portion bytes sending logic when is textual mode, because those bitmap are predetermined(known size)
                            if (!usesHardwareHandshake && !isEmbeddedBitmapBytes) {
                                //Calcultate sleep interval
                                int sleepInterval = (int) (portionPrintingIntervalInMiliseconds / (float) portionedBytesSize * bytesForSend.length);
                                hardware.Log(typeName, String.format("Print(): sleepInterval:%1$s portionPrintingIntervalInMiliseconds:%2$s; portionedBytesSize:%3$s; bytesForSend.Length:%4$s;", sleepInterval, portionPrintingIntervalInMiliseconds, portionedBytesSize, bytesForSend.length), LogType.Information);
                                Thread.sleep(sleepInterval);
                            }
                        } else {
                            hardware.Log(typeName, "Print(): Appropirate Commands Does Not Exists, Index: " + i, LogType.Information);
                        }

                        isEmbeddedBitmapBytes = false;
                    }

                    if ((commandBytesLists == null || commandBytesLists.isEmpty()) && !paperName.equals("")) {
                        hardware.Log(typeName, "Print(): PrintWithDotNetAndWindowsDriver", LogType.Information);
                        PrintWithDotNetAndWindowsDriver(taggedText); //Use .Net PrintDocument class
                    }

                    String quittanceCountStr = hardware.GetStaticValue(payboxDeviceTypeName + ".QuittanceCount");
                    int quittanceCounter = 0;
                    try {
                        quittanceCounter = Integer.parseInt(quittanceCountStr);
                    } catch (Exception e) {

                    }
                    //Increment Quittance Counter
                    hardware.Log(typeName, "QuittanceCount: " + quittanceCountStr, LogType.Information);
                    hardware.SaveStaticValue(payboxDeviceTypeName + ".QuittanceCount", (++quittanceCounter) + "");
                    hardware.Log(typeName, "QuittanceCount New: " + quittanceCounter, LogType.Information);
                }
            }
        } catch (Exception ex) {
            hardware.Log(typeName, "Print(): " + ex.getMessage(), LogType.Error);
            hardware.LogException(ex);
        }


        //Set Print Time Atomicaly
        lastPrintTimeInBinary.set(DateTime.now().ToBinary());
    }


    protected final ArrayList<ArrayList<Byte>> TranslateText(String taggedText, int bufferSize) {
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: var translatedBytesLists = new List<List<byte>>();
        ArrayList<ArrayList<Byte>> translatedBytesLists = new ArrayList<ArrayList<Byte>>();
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: var translatedBytesList = new List<byte>();
        ArrayList<Byte> translatedBytesList = new ArrayList<Byte>();
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: byte[] bytesArrayTemp;
        byte[] bytesArrayTemp;
        char simbol;

        StringBuilder formatCommand = new StringBuilder();

        for (int i = 0; i < taggedText.length(); i++) {
            simbol = taggedText.charAt(i);

            if (simbol == '<') {
                while (simbol != '>') {
                    simbol = taggedText.charAt(i++);
                    formatCommand.append(simbol);
                }

                i--;

                if (formatCommand.charAt(1) == 'P' && formatCommand.charAt(2) == '-') //If Command Is Parameterized
                {
                    String parameters = taggedText.substring(++i, ++i + taggedText.indexOf(formatCommand.insert(1, '/').toString(), i) - i);

                    int originalParameterLength = parameters.length();

                    //Modify paramaters string if barcode command has two parameters (first data length, second data)
                    if (formatCommand.toString().equals("</P-BarCode>") && commandDictionary.get("<P-BarCode>").getShortsList().contains(256) && commandDictionary.get("<P-BarCode>").getShortsList().contains(257)) {
                        //256 - numeric parameter, 257 - string parameter
                        parameters = parameters.length() + "~D~" + parameters;
                    }

                    /* TODO Replace
                    * bytesArrayTemp = GetCommandBytes(formatCommand.Replace("/", "").toString(), parameters);
                    * */
                    bytesArrayTemp = GetCommandBytes(formatCommand.toString().replaceAll("/", "").toString(), parameters);
                    i += formatCommand.length() + originalParameterLength;
                } else {
                    bytesArrayTemp = GetCommandBytes(formatCommand.toString());
                }

                //Embedded bitmaps does not need portion sending, this bytes size may exceed limit
                if (formatCommand.toString().equals("<P-Image>") || formatCommand.toString().equals("<P-GLCBarCodeAndSignatureBitmap>")) {
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: translatedBytesLists.Add(new List<byte>(translatedBytesList));
                    translatedBytesLists.add(new ArrayList<Byte>(translatedBytesList));
                    translatedBytesLists.add(null);
                    //Sign for sending logic, that next list item is embedded bitmap bytes
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: translatedBytesLists.Add(new List<byte>(bytesArrayTemp));
                    translatedBytesLists.add(new ArrayList<Byte>(Bytes.asList(bytesArrayTemp)));
                    translatedBytesList.clear();
                } else {
                    //If adding this command bytes does not fits buffer size, add bytes in another bytes list
                    if (translatedBytesList.size() + bytesArrayTemp.length > bufferSize) {
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: translatedBytesLists.Add(new List<byte>(translatedBytesList));
                        translatedBytesLists.add(new ArrayList<Byte>(translatedBytesList));
                        translatedBytesList.clear();
                    }

                    ByteLists.addPrimitiveArrayToList(bytesArrayTemp, translatedBytesList);
                }

                formatCommand.setLength(0);
            } else if (simbol != 13) {
                //If adding byte does not fits buffer size, add byte in another bytes list
                if (translatedBytesList.size() + 1 > bufferSize) {
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: translatedBytesLists.Add(new List<byte>(translatedBytesList));
                    translatedBytesLists.add(new ArrayList<Byte>(translatedBytesList));
                    translatedBytesList.clear();
                }

                translatedBytesList.add(TranslateSimbolToByte(simbol));
            }
        }

        if (!translatedBytesList.isEmpty()) {
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: translatedBytesLists.Add(new List<byte>(translatedBytesList));
            translatedBytesLists.add(new ArrayList<Byte>(translatedBytesList));
        }

        return translatedBytesLists;
    }

    protected byte TranslateSimbolToByte(int simbolValue) {
        if (simbolValue >= 4304 && simbolValue <= 4336) {
            //Unicode Georgian Simbols (stored beatween 128 - 160 ASCI codes)
            simbolValue -= 4176;
        }

        if (simbolValue == 10 || symbolCount == 48) {
            lineCount++;
            symbolCount = 0;
        } else {
            symbolCount += symbolWidth;
        }

//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: return (byte) simbolValue;
        return (byte) simbolValue;
    }


    protected final void DrawTextForGraphicalMode(Graphics graphics, String taggedText) {
        /*
            TODO DrawTextForGraphicalMode

        Font font = new Font("Sylfaen", fontSizeNormal);
        StringFormat strFormat = new StringFormat();
        Image image = null;
        int lineCount = 1;
        int x = 5;
        int y = 0;
        String chunkText = "";
        String textForPrinting = "";

        taggedText = taggedText.replaceAll("<ru>", "");
        taggedText = taggedText.replaceAll("</ru>", "");

        while (taggedText.length() != 0) {
            boolean isBarCode = false;
            boolean isImage = false;
            int commandStartIndex = taggedText.indexOf("<");

            if (commandStartIndex != -1) {
                int commandEndIndex = taggedText.indexOf(">");
                String command = taggedText.substring(commandStartIndex, commandEndIndex + 1);

                if (commandStartIndex != 0) {
                    textForPrinting = taggedText.substring(0, commandStartIndex);
                    taggedText = taggedText.substring(commandStartIndex);
                    font = new Font("Sylfaen", fontSizeNormal);
                    strFormat.Alignment = StringAlignment.Near;
                } else {
                    if (command.contains("/")) {
                        taggedText = DotNetToJavaStringHelper.remove(taggedText, commandStartIndex, commandEndIndex - commandStartIndex + 1);
                        continue;
                    }
                    String endCommand = command.insert(1, "/");
                    int chunkTextEndIndex = taggedText.indexOf(endCommand) + endCommand.length() - 1;

                    if (chunkTextEndIndex == endCommand.length() - 2) //if end command not detected
                    {
                        chunkTextEndIndex = taggedText.length() - 1;
                    }

                    chunkText = taggedText.substring(commandStartIndex, chunkTextEndIndex + 1);
                    taggedText = taggedText.substring(chunkTextEndIndex + 1);

                    if (chunkText.contains("<P-BarCode>") && chunkText.contains("</P-BarCode>")) {
                        //Barcode tags must be parent tags for chunk text, (example: this will not work, <center> <P-BarCode>18743921749279428</P-BarCode> </center>
                        chunkText = DotNetToJavaStringHelper.substring(chunkText, chunkText.indexOf("<P-BarCode>") + 11, chunkText.indexOf("</P-BarCode>") - (chunkText.indexOf("<P-BarCode>") + 11));
                        //chunkText = chunkText.Replace("<P-BarCode>", "");
                        //chunkText = chunkText.Replace("</P-BarCode>", "");
                        textForPrinting = chunkText;
                        isBarCode = true;
                    } else if (chunkText.contains("<P-Image>") && chunkText.contains("</P-Image>")) {
                        chunkText = chunkText.replace("<P-Image>", "");
                        chunkText = chunkText.replace("</P-Image>", "");
                        textForPrinting = chunkText;
                        isImage = true;
                    } else {
                        RefObject<Font> tempRef_font = new RefObject<Font>(font);
                        RefObject<StringFormat> tempRef_strFormat = new RefObject<StringFormat>(strFormat);
                        textForPrinting = FormatTaggedStringForGraphicalMode(chunkText, tempRef_font, tempRef_strFormat);
                        strFormat = tempRef_strFormat.argValue;
                        font = tempRef_font.argValue;
                    }
                }
            } else {
                textForPrinting = taggedText;
                taggedText = "";
                font = new Font("Sylfaen", fontSizeNormal);
                strFormat.Alignment = StringAlignment.Near;
            }

            if (isBarCode) {
                BarcodeLib.Barcode barcodeGenerator = new Barcode();
                image = barcodeGenerator.Encode(TYPE.CODE128, textForPrinting, paperWidthInPixels - paperWidthInPixels / 4, lineHeightInPixels * 4);
                graphics.DrawImage(image, (paperWidthInPixels - image.Width) / 2, y, image.Width, image.Height);
            } else if (isImage) {
                image = (Bitmap) hardware.GetDeviceResource(textForPrinting);
                graphics.DrawImage(image, (paperWidthInPixels - image.Width) / 2, y, image.Width, image.Height);
            } else {
                lineCount = textForPrinting.split(new String[]{"\n"}, StringSplitOptions.None).length;
                graphics.DrawString(textForPrinting, font, Brushes.Black, new RectangleF(x, y, paperWidthInPixels, lineCount * lineHeightInPixels), strFormat);
            }

            if (isBarCode || isImage) {
                y += image.Height;
            } else {
                y += lineCount * lineHeightInPixels;
            }
        }
         */
    }

    //Obsolete
    protected final String FormatTaggedStringForGraphicalMode(String chunkText, RefObject<Font> font, RefObject<StringFormatter> format) {
        return null;
         /*
            TODO FormatTaggedStringForGraphicalMode
        format.argValue.Alignment = StringAlignment.Near;
        font.argValue = new Font("Sylfaen", fontSizeNormal);

        if (chunkText.contains("<center>")) {
            format.argValue.Alignment = StringAlignment.Center;

            chunkText = chunkText.replace("<center>", "");
            chunkText = chunkText.replace("</center>", "");
        }

        if (chunkText.contains("<right>")) {
            format.argValue.Alignment = StringAlignment.Center;

            chunkText = chunkText.replace("<right>", "");
            chunkText = chunkText.replace("</right>", "");
        }

        if (chunkText.contains("<left>")) {
            format.argValue.Alignment = StringAlignment.Near;

            chunkText = chunkText.replace("<left>", "");
            chunkText = chunkText.replace("</left>", "");
        }

        if (chunkText.contains("<b>")) {
            font.argValue = new Font("Sylfaen", fontSizeNormal, FontStyle.Bold);

            chunkText = chunkText.replace("<b>", "");
            chunkText = chunkText.replace("</b>", "");
        }

        if (chunkText.contains("<l>")) {
            if (font.argValue.Bold) {
                font.argValue = new Font("Sylfaen", fontSizeLarge, FontStyle.Bold);
            } else {
                font.argValue = new Font("Sylfaen", fontSizeLarge);
            }

            chunkText = chunkText.replace("<l>", "");
            chunkText = chunkText.replace("</l>", "");
        }

        return chunkText;
        */
    }


    protected final Bitmap DrawTextForGraphicalModeWithWebBrowser(String taggedText) {
        return DrawTextForGraphicalModeWithWebBrowser(taggedText, null);
    }

    //C# TO JAVA CONVERTER NOTE: Java does not support optional parameters. Overloaded method(s) are created above:
//ORIGINAL LINE: protected Bitmap DrawTextForGraphicalModeWithWebBrowser(string taggedText, Graphics graphics = null)
    protected final Bitmap DrawTextForGraphicalModeWithWebBrowser(String taggedText, Graphics graphics) {
        return null;
         /*
            TODO DrawTextForGraphicalModeWithWebBrowser
        taggedText = ConvertCustomTaggesToHTML(taggedText);

        hardware.Log(typeName, "DrawTextForGraphicalModeWithWebBrowser(): taggedText: " + taggedText, LogType.Information);

        Object outParameter = new Object();
        //Optional out parameter, use for get web browser ReadyState property, document load was sucessfully completed or not
        RefObject<Object> tempRef_outParameter = new RefObject<Object>(outParameter);
        Bitmap htmlCaptureImage = HtmlCapture.Create(taggedText, paperWidthInPixels, lineHeightInPixels, tempRef_outParameter, graphics);
        outParameter = tempRef_outParameter.argValue;
        //Height is minimum length, if taggedText will requare more height it will increase automaticly

        //htmlCaptureImage.Save(@"C:\112233.bmp", ImageFormat.Bmp);

        if (outParameter instanceof WebBrowserReadyState) {
            WebBrowserReadyState wbState = WebBrowserReadyState.forValue((Integer) outParameter);

            if (wbState != WebBrowserReadyState.Complete) {
                hardware.Log(typeName, "DrawTextForGraphicalModeWithWebBrowser(): WebBrowserReadyState: " + wbState, LogType.Warning);
            }
        }

        return htmlCaptureImage;
        */
    }

    protected final String ConvertCustomTaggesToHTML(String taggedText) {
        return null;
        /*
            TODO ConvertCustomTaggesToHTML
        //Convert formating tags
        taggedText = Regex.Replace(taggedText, "<[/]?address>", "", RegexOptions.IgnoreCase); //It makes text italic
        taggedText = Regex.Replace(taggedText, "<[/]?ru>", "", RegexOptions.IgnoreCase); //Russian characters tag
        taggedText = Regex.Replace(taggedText, "\r\n", "</br>", RegexOptions.IgnoreCase);
        //Carriage return and line break tag
        taggedText = Regex.Replace(taggedText, "\n", "</br>", RegexOptions.IgnoreCase); //Line break tag
        taggedText = Regex.Replace(taggedText, "<right>", "<p align=\"right\">", RegexOptions.IgnoreCase);
        //Align right tag
        taggedText = Regex.Replace(taggedText, "</right>", "</p>", RegexOptions.IgnoreCase); //Align right tag
        taggedText = Regex.Replace(taggedText, "<l>", "<big>", RegexOptions.IgnoreCase); //Large font tag
        taggedText = Regex.Replace(taggedText, "</l>", "</big>", RegexOptions.IgnoreCase); //Large font atg

        String htmlBodyStyle = "";
        String customLineSpacingTag = "<lnsp style=\"line-height:"; //For line spacing

        if (taggedText.contains(customLineSpacingTag)) {
            int startIndex = taggedText.indexOf(customLineSpacingTag, 0) + customLineSpacingTag.length();
            String lineHeight = taggedText.substring(startIndex, taggedText.indexOf("%", startIndex));
            htmlBodyStyle = String.format("<html><body leftmargin=\"0\" style=\"font-family:Sylfaen; font-size:%1$spx; line-height:%2$s%%;\">", fontSizeNormal, lineHeight);
        } else {
            htmlBodyStyle = String.format("<html><body leftmargin=\"0\" style=\"font-family:Sylfaen; font-size:%1$spx;\">", fontSizeNormal); //Left margin and font size tags
        }

        taggedText = htmlBodyStyle + taggedText;
        taggedText += "</body></html>";

        //Convert imaging tags
        String taggedTextTemp = taggedText;
        String imageURL = "";
        int imageFileIndex = 1;

        for (Map.Entry<String, String> taggsPair : customTaggesDictionary.entrySet()) {
            while (taggedTextTemp.contains(taggsPair.getKey()) && taggedTextTemp.contains(taggsPair.getValue())) {
                int tagStartIndex = taggedTextTemp.indexOf(taggsPair.getKey());
                int tagEndIndex = taggedTextTemp.indexOf(taggsPair.getValue());

                if (tagStartIndex >= tagEndIndex) {
                    break;
                }

                String tagContent = DotNetToJavaStringHelper.substring(taggedTextTemp, tagStartIndex + taggsPair.getKey().length(), tagEndIndex - tagStartIndex - taggsPair.getKey().length());
                Image image = null;

                hardware.Log(typeName, "ConvertCustomTaggesToHTML(): Imaging tag content: " + tagContent, LogType.Information);
                if (taggsPair.getKey().equals("<P-BarCode>")) {
                    image = barcodeGenerator.Encode(TYPE.CODE128, tagContent, paperWidthInPixels - paperWidthInPixels / 9, fontSizeNormal * 5);
                } else if (taggsPair.getKey().equals("<P-Image>")) {
                    image = (Bitmap) hardware.GetDeviceResource(tagContent);
                }

                //Create temp image file and URL
                imageURL = tempFilePath + imageFileIndex++ + ".bmp";
                image.Save(imageURL, ImageFormat.Bmp);

                //Image tag
//C# TO JAVA CONVERTER TODO TASK: There is no equivalent to implicit typing in Java:
                String customTaggesWithContent = taggsPair.getKey() + tagContent + taggsPair.getValue();
                taggedText = (new StringBuilder(taggedText)).Replace(customTaggesWithContent, "<img src=\"" + imageURL + "\" align=\"middle\" />", tagStartIndex, customTaggesWithContent.getLength()).toString();
                taggedTextTemp = taggedText;
            }
        }

        return taggedText;
        */
    }

    protected final byte[] GetCommandBytes(String command) {
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: var resultCommandBytes = new byte[0];
        byte[] resultCommandBytes = new byte[0];

        if (commandDictionary.containsKey(command)) {
            ArrayList<Short> shortsList = commandDictionary.get(command).getShortsList();

            if (!shortsList.contains((short) 256) && !shortsList.contains((short) 257) && !shortsList.contains((short) 258)) {
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: resultCommandBytes = new byte[shortsList.Count];
                resultCommandBytes = new byte[shortsList.size()];

                for (int i = 0; i < shortsList.size(); i++) {
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: resultCommandBytes[i] = (byte) shortsList[i];
                    resultCommandBytes[i] = shortsList.get(i).byteValue();
                }
            }
        } else //Perceive As Ordinary Text
        {
            //byte[] bytesArrayTemp = Encoding.Unicode.GetBytes(command); //???

//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: resultCommandBytes = new byte[command.Length];
            resultCommandBytes = new byte[command.length()];

            for (int i = 0; i < resultCommandBytes.length; i++) {
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: resultCommandBytes[i] = TranslateSimbolToByte((byte) command[i]);
                resultCommandBytes[i] = TranslateSimbolToByte((byte) command.charAt(i));
            }
        }

        return resultCommandBytes;
    }

    //C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: protected byte[] GetCommandBytes(string command, string parameters)
    protected final byte[] GetCommandBytes(String command, String parameters) {
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: var resultCommandBytes = new byte[0];
        byte[] resultCommandBytes = new byte[0];

        if (command != null && !command.equals("") && commandDictionary.containsKey(command) && parameters != null && !parameters.equals("")) {
            try {
                ArrayList<Short> shortsList = new ArrayList<Short>(commandDictionary.get(command).getShortsList());
//C# TO JAVA CONVERTER TODO TASK: There is no equivalent to implicit typing in Java:
                // TODO StringSplitOptions
//                String[] paramsArray = parameters.split(new String[]{"~D~"}, StringSplitOptions.RemoveEmptyEntries);
                String[] paramsArray = parameters.split("~D~");
                int index = 0, numericIndex = 0, stringIndex = 0, bitmapIndex = 0, paramIndex = 0;

                while (numericIndex != -1 || stringIndex != -1 || bitmapIndex != -1) {
                    if (numericIndex != -1) {
                        numericIndex = shortsList.subList(index, shortsList.size()).indexOf((short) 256);
                    }

                    if (stringIndex != -1) {
                        stringIndex = shortsList.subList(index, shortsList.size()).indexOf((short) 257);
                    }

                    if (bitmapIndex != -1) {
                        bitmapIndex = shortsList.subList(index, shortsList.size()).indexOf((short) 258);
                    }

                    if (paramIndex >= paramsArray.length) {
                        break;
                    }

                    //Perceive As Numeric
                    if (numericIndex != -1 && (numericIndex < stringIndex || stringIndex == -1) && (numericIndex < bitmapIndex || bitmapIndex == -1)) {
                        index = numericIndex;

                        shortsList.remove(index);
                        shortsList.add(index, ((short) Byte.parseByte(paramsArray[paramIndex++])));
                    }
                    //Perceive As String
                    else if (stringIndex != -1 && (stringIndex < numericIndex || numericIndex == -1) && (stringIndex < bitmapIndex || bitmapIndex == -1)) {
                        index = stringIndex;

                        byte[] parameterBytes = paramsArray[paramIndex++].getBytes();
                        short[] shortsTemp = new short[parameterBytes.length];

                        for (int i = 0; i < parameterBytes.length; i++) {
                            shortsTemp[i] = parameterBytes[i];
                        }

                        shortsList.remove(index);
                        ShortLists.addPrimitiveArrayToList(index, shortsTemp, shortsList);
                    }
                    //Perceive As Image (Logo Or Bitmap File), For outer template use <P-Image> tag, with 'empty bytes' command
                    else if (bitmapIndex != -1 && (bitmapIndex < numericIndex || numericIndex == -1) && (bitmapIndex < stringIndex || stringIndex == -1)) {
                        index = bitmapIndex;

                        String imageParameter = paramsArray[paramIndex++];
                        byte[] parameterBytes = new byte[0];

                        //First Time Search In Stored Logos Dictionary, Then In Resources
                        if (storedLogosDictionary.containsKey(imageParameter)) {
                            parameterBytes = GetCommandBytes("<P-Logo>", storedLogosDictionary.get(imageParameter).toString()); //Get Stored Logo Command Bytes
                        } else {
                            parameterBytes = Bytes.toArray(GetBitmapBytes(command, imageParameter).get(0));
                        }
                        //Get Bitmap Bytes, this bitmap byes must fit in one portion

                        short[] shortsTemp = new short[parameterBytes.length];

                        for (int i = 0; i < parameterBytes.length; i++) {
                            shortsTemp[i] = parameterBytes[i];
                        }

                        shortsList.remove(index);
                        ShortLists.addPrimitiveArrayToList(index, shortsTemp, shortsList);
                    }
                }

                if (numericIndex == -1 && stringIndex == -1 && bitmapIndex == -1 && paramIndex == paramsArray.length) {
                    //Evrything Is OK
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: resultCommandBytes = new byte[shortsList.Count];
                    resultCommandBytes = new byte[shortsList.size()];

                    for (int i = 0; i < shortsList.size(); i++) {
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: resultCommandBytes[i] = (byte) shortsList[i];
                        resultCommandBytes[i] = shortsList.get(i).byteValue();
                    }
                }
            } catch (java.lang.Exception e) {
                resultCommandBytes = new byte[0];
            }
        }

        return resultCommandBytes;
    }

    //ORIGINAL LINE: protected List<byte> GetSequentialCommandsByType(CommandType commandType, int lineFeedCount, bool fullCut)
    protected final ArrayList<Byte> GetSequentialCommandsByType(CommandType commandType, int lineFeedCount, boolean fullCut) {
        ArrayList<Byte> sequentialCommandBytesList = new ArrayList<Byte>();

        try {
            //Select Commands By Type And Order
//            Map<Object, Object> filteredCommands = (from x in commandDictionary where x.Value.Type == commandType
//            orderby x.Value.OrderNumber select x).ToDictionary(kv -> kv.Key, kv -> kv.Value);


            Map<Object, CommandInfo> filteredCommands = commandDictionary.entrySet().stream().filter(x -> x.getValue().getType().equals(commandType)).collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));

//C# TO JAVA CONVERTER TODO TASK: There is no equivalent to implicit typing in Java:
            for (CommandInfo commandInfo : filteredCommands.values()) {
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: byte[] commandsBytes = null;
                byte[] commandsBytes = null;

                if (commandInfo.ParameterValues.equals("")) {
                    if (commandInfo.Name.equals("FullCut") && fullCut || commandInfo.Name.equals("PartialCut") && !fullCut || !commandInfo.Name.equals("FullCut") && !commandInfo.Name.equals("PartialCut")) {
                        commandsBytes = GetCommandBytes(commandInfo.Name);
                    }
                } else {
                    if (commandInfo.Name.equals("P-LineFeed") && lineFeedCount != -1) {
                        commandsBytes = GetCommandBytes(commandInfo.Name, String.valueOf(lineFeedCount));
                    } else {
                        commandsBytes = GetCommandBytes(commandInfo.Name, commandInfo.ParameterValues);
                    }
                }

                if (commandsBytes != null) {
                    ByteLists.addPrimitiveArrayToList(commandsBytes, sequentialCommandBytesList);
                }
            }
        } catch (RuntimeException ex) {
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: sequentialCommandBytesList = new List<byte>();
            sequentialCommandBytesList = new ArrayList<Byte>();

            hardware.Log(typeName, "GetSequentialCommandsByType(): " + ex.getMessage(), LogType.Error);
            hardware.LogException(ex);
        }

        return sequentialCommandBytesList;
    }

    //Create Bitmap And Get Its Bytes According commandName And Printer Type
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: protected abstract List<List<byte>> GetBitmapBytes(string commandName, object parameters);
    protected abstract ArrayList<ArrayList<Byte>> GetBitmapBytes(String commandName, Object parameters);

    //Portion bitmap structures bytes into separate lists to fit with one portion bytes size
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: protected List<List<byte>> GetBitmapPortionedBytesLists(byte[] bitmapStructuredBytes, int portionBytesSize)
    protected final ArrayList<ArrayList<Byte>> GetBitmapPortionedBytesLists(byte[] bitmapStructuredBytes, int portionBytesSize) {
        hardware.Log(typeName, "GetBitmapPortionedBytesLists(): bitmapStructuredBytes length:" + bitmapStructuredBytes.length, LogType.Information);

//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: var bitmapPortionedBytesLists = new List<List<byte>>();
        ArrayList<ArrayList<Byte>> bitmapPortionedBytesLists = new ArrayList<ArrayList<Byte>>();
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: var bitmapPortionedBytesList = new List<byte>(portionBytesSize);
        ArrayList<Byte> bitmapPortionedBytesList = new ArrayList<Byte>(portionBytesSize);

        for (int i = 0; i < bitmapStructuredBytes.length; i++) {
            if (portionBytesSize == bitmapPortionedBytesList.size()) {
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: bitmapPortionedBytesLists.Add(new List<byte>(bitmapPortionedBytesList));
                bitmapPortionedBytesLists.add(new ArrayList<Byte>(bitmapPortionedBytesList));
                bitmapPortionedBytesList.clear();
            }

            bitmapPortionedBytesList.add(bitmapStructuredBytes[i]);
        }

        if (!bitmapPortionedBytesList.isEmpty()) {
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: bitmapPortionedBytesLists.Add(new List<byte>(bitmapPortionedBytesList));
            bitmapPortionedBytesLists.add(new ArrayList<Byte>(bitmapPortionedBytesList));
        }

        return bitmapPortionedBytesLists;
    }

    public abstract boolean WriteLogo(String imagePath);

    public abstract boolean WriteFont(String fontPath);

    protected abstract void PrintWithDotNetAndWindowsDriver(Object parameters);

    //Line Feed Count -1 Is Default
    @FunctionalInterface
    private interface PrintDelegate {
        void invoke(String taggedText, PrintMode printMode, int lineFeedCount, boolean fullCut);
    }

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#region IHardwareBase Members

    public final boolean Initialize() {
        if (configIsValid && !initialized) {
            if (InitializePrinter()) {
                initialized = true;
            }
        }

        return initialized;
    }

    public final boolean Deinitialize() {
        if (initialized) {
            monitorPrinterState = false;

            while (printerStateMonitoringThread.isAlive()) {
                ;
            }

            synchronized (currentPrinterDeviceStateLock) {
                currentPrinterDeviceState = new PrinterDeviceState(this.getClass(), payboxDeviceTypeName, new HashMap<Integer, Error>(), PrinterState.Uninitialized);
            }

            try {
                if (serialPort.isOpend()) {
                    serialPort.close();
                }
            } catch (RuntimeException ex) {
                hardware.Log(typeName, "Deinitialize(): " + ex.getMessage(), LogType.Error);
                hardware.LogException(ex);
            }

            initialized = false;
        }

        return true;
    }

    public final boolean Reset() {
        if (Deinitialize()) {
            return Initialize();
        }
        return false;
    }

    public final DeviceState GetState(boolean force) throws Exception {
        if (force) {
            int countTemp = monitoringCycleCount.get();
            int wautTimeoutCount = 0;

            while (monitoringCycleCount.get() == countTemp && printerStateMonitoringThread != null && printerStateMonitoringThread.isAlive()) {
                Thread.sleep(1000);
                wautTimeoutCount++;

                if (wautTimeoutCount == 60) {
                    hardware.Log(typeName, "GetState() forced: Timedout", LogType.Error);
                    return null;
                }
            }
        }

        //Obsolate
        //lock (this.currentPrinterDeviceStateLock)
        //{
        //    return this.currentPrinterDeviceState;
        //}

        if (currentPrinterDeviceStateLock.tryAcquire(60000L, TimeUnit.MILLISECONDS)) {
            try {
                return currentPrinterDeviceState;
            } finally {
                currentPrinterDeviceStateLock.release();
            }
        }
        hardware.Log(typeName, "GetState(): Couldn't acquire lock", LogType.Error);
        return null;
    }


//C# TO JAVA CONVERTER TODO TASK: This event cannot be converted to Java:
//	public event StateChangedEventHandler StateChanged
//		{
//			add
//			{
//				onStateChanged += value;
//			}
//			remove
//			{
//				onStateChanged -= value;
//			}
//		}


    public final void SoftwareActionToDeviceHandler(Object sender, SoftwareActionToDevice actionArgs) {
        switch (actionArgs.getSoftwareAction()) {
            case PrinterPaperReset:
                if (actionArgs.getPayboxDeviceTypeName().equals(payboxDeviceTypeName)) {
                    hardware.SaveStaticValue(payboxDeviceTypeName + ".QuittanceCount", "0");
                    hardware.Log(typeName, String.format("SoftwareActionToDeviceHandler(): Printer %1$s Clear Quittance Counter", actionArgs.getPayboxDeviceTypeName()), LogType.Information);
                }
                break;
        }
    }

    public final HashMap<String, Integer> GetDeviceCountersDictionary() {
        HashMap<String, Integer> countersDictionary = new HashMap<String, Integer>();

        try {
            String quittanceCountStr = hardware.GetStaticValue(payboxDeviceTypeName + ".QuittanceCount");
            int quittanceCounter = 0;
            try {
                quittanceCounter = Integer.parseInt(quittanceCountStr);
            } catch (Exception e) {

            }
            countersDictionary.put("QuittanceCount", quittanceCounter);
        } catch (RuntimeException ex) {
            hardware.Log(typeName, "GetDeviceCountersDictionary(): " + ex.getMessage(), LogType.Error);
        }

        return countersDictionary;
    }


    protected static Bitmap CreateWhiteImage(int w, int h) {
        return null;
        /*
        TODO CreateWhiteImage
        try {
            Bitmap bmp = new Bitmap(w, h);

            int[] RGB = new int[w * h];

            System.Drawing.Imaging.BitmapData data = bmp.LockBits(new Rectangle(0, 0, w, h), ImageLockMode.WriteOnly, PixelFormat.Format32bppRgb);
            System.IntPtr pointer = data.Scan0;
            Marshal.Copy(pointer, RGB, 0, w * h);

            for (var i = 0; i < RGB.length; i++) {
                RGB[i] = (255 << 16) + (255 << 8) + 255;
            }

            Marshal.Copy(RGB, 0, pointer, w * h);

            bmp.UnlockBits(data);

            return bmp;
        } catch (java.lang.Exception e) {
            return null;
        }
        */
    }

    // Structure and API declarions:
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)] public class DOCINFOA
    public static class DOCINFOA {
        //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [MarshalAs(UnmanagedType.LPStr)] public string pDataType;
        public String pDataType;

        //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [MarshalAs(UnmanagedType.LPStr)] public string pDocName;
        public String pDocName;

        //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [MarshalAs(UnmanagedType.LPStr)] public string pOutputFile;
        public String pOutputFile;
    }

}
