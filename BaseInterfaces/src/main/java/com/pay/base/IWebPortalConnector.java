package com.pay.base;

import com.pay.base.event.EventListener;
import com.pay.base.event.types.CashReceivedEvent;


/**
 * Created by dito on 4/4/2017.
 */
public interface IWebPortalConnector {
    void addCashReceivedHandler(EventListener<CashReceivedEvent> listener);

    void removeCashReceivedHandler(EventListener<CashReceivedEvent> listener);

}
