package com.pay.base;

import com.pay.base.config.xml.XmlNode;
import com.pay.base.config.xml.XmlNodeList;
import com.pay.base.devicestate.CashInDeviceState;
import com.pay.base.devicestate.DeviceState;
import com.pay.base.devicestate.enums.CashInState;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.pay.base.event.EventListener;
import com.pay.base.event.EventManager;
import com.pay.base.event.types.CashReceivedEvent;
import com.pay.base.event.types.DeviceChangedEvent;
import com.pay.base.event.types.SoftwareActionToDeviceEvent;
import com.pay.helper.IntegerHelper;
import com.pay.helper.enums.LogType;
import org.apache.commons.lang3.mutable.MutableInt;

/**
 * Created by dito on 3/31/2017.
 */
public abstract class CashIn implements IHardwareBase {
    //#region IHardwareBase Members
    @Override
    public abstract boolean Initialize();

    @Override
    public abstract boolean Deinitialize();

    @Override
    public abstract boolean Reset();

    @Override
    public abstract DeviceState GetState(boolean force);

    private final EventManager<DeviceState> stateChangedEventHandler = new EventManager<>();

    @Override
    public void OnStateChanged(DeviceChangedEvent event) {
        if (stateChangedEventHandler != null) {
            stateChangedEventHandler.invoke(event);
        } else
            hardware.Log(typeName, "OnStateChanged() IS NULL", LogType.Warning);
    }

    @Override
    public void SoftwareActionToDeviceHandler(SoftwareActionToDeviceEvent event) {
        SoftwareActionToDevice actionArgs = event.getValue();
        switch (actionArgs.getSoftwareAction()) {
            case CassetteSet:
                if (!this.hasCassetteFullError)
                    this.virtualCassette.OnCassetteSet();
                break;
            case CassetteGet:
                if (!this.hasCassetteFullError)
                    this.virtualCassette.OnCassetteGet();
                break;
            case CollectionHappend:
                this.virtualCassette.OnCollectionHappend();
                rejected = 0;
                accepted = 0;
                hardware.SaveStaticValue(this.payboxDeviceTypeName + ".RejectedCashCounter", "0");
                hardware.SaveStaticValue(this.payboxDeviceTypeName + ".AcceptedCashCounter", "0");
                break;
            case Reset:
                this.Reset();
                break;
        }
    }


    protected String typeName = "";
    protected String payboxDeviceTypeName = "";
    protected IHardware hardware;
    protected boolean configIsValid = false;
    protected boolean initialized = false;

    public VirtualCassette virtualCassette = null;

    protected boolean hasCassetteFullError = false; //Determines if device has own cassetteFull error, if not must implement this error manualy
    protected int cashLimitInCassette = 0; //Determines cash count limit for cassetteFull error
    protected boolean cassetteFullError = false; //Determines if device has cassetteFull error in that moment
    protected boolean hasNotResetCommand = false; //Determines if device has not reset commnd

    protected Map<Integer, Error> errorDictionary = new HashMap<>();// <Key: Device Specific Code, Value: Error Info>
    protected Map<Integer, CashMoney> cashDictionary = new HashMap<>();// <Key: Device Specific Code, Value: Cash Info>

    protected Map<Integer, Integer> allowedCashIDDictionary = new HashMap<>();// <Key: Cash ID, Value: Device Specific Code (Also Defines Position In Bit Array)> For Set Cash Types By Program
    protected final Lock allowedCashIDDictionaryLock = new ReentrantLock();
    protected List<Integer> allowedCashDeviceCodeListFromDevice = new ArrayList<>(8);
    protected final Lock allowedCashDeviceCodeListFromDeviceLock = new ReentrantLock();

    protected Lock determineCashCounterLock = new ReentrantLock();

    private final EventManager<CashMoney> cashReceivedEventHandler = new EventManager<>();


    public CashIn() {

    }

    public void OnCashReceived(Object sender, CashMoney cashArgs) {
        if (cashReceivedEventHandler != null) {
            cashReceivedEventHandler.invoke(new CashReceivedEvent(sender, cashArgs));
        } else
            hardware.Log(typeName, "OnCashReceived() IS NULL", LogType.Warning);
    }

    public boolean AllowCashReceive(int... allowCashIDList) {
        if (allowCashIDList.length == 0) {
            return false;
        }
        try {
            allowedCashIDDictionaryLock.lock();
            allowedCashIDDictionary.clear();
            for (int i = 0; i < allowCashIDList.length; i++) {
                for (Integer code : cashDictionary.keySet()) {
                    if (cashDictionary.get(code).getId() == allowCashIDList[i]) {
                        allowedCashIDDictionary.put(allowCashIDList[i], code);
                        break;
                    }
                }

            }
        } finally {
            allowedCashIDDictionaryLock.unlock();
        }
        StartCashReceive();
        return true;
    }

    public boolean AllowCashReceive() {
        try {
            allowedCashIDDictionaryLock.lock();
            allowedCashIDDictionary.clear();
            for (Integer code : cashDictionary.keySet()) {
                allowedCashIDDictionary.put(cashDictionary.get(code).getId(), code);
            }
        } finally {
            allowedCashIDDictionaryLock.unlock();
        }
        StartCashReceive();
        return true;
    }

    public boolean DenyCashReceive() {
        try {
            allowedCashIDDictionaryLock.lock();
            allowedCashIDDictionary.clear();
        } finally {
            allowedCashIDDictionaryLock.unlock();
        }
        StopCashReceive();
        return true;
    }

    public int[] GetAllowedCashIDList() {
        int[] allowedCashIDList;

        try {
            allowedCashDeviceCodeListFromDeviceLock.lock();
            allowedCashIDDictionary.clear();
            allowedCashIDList = new int[allowedCashDeviceCodeListFromDevice.size()];
            int index = 0;
            for (Integer code : allowedCashDeviceCodeListFromDevice) {
                if (cashDictionary.containsKey((int) code))
                    allowedCashIDList[index++] = cashDictionary.get((int) code).getId();
            }
        } finally {
            allowedCashDeviceCodeListFromDeviceLock.unlock();
        }
        return allowedCashIDList;
    }

    protected abstract void StartCashReceive();

    protected abstract void StopCashReceive();

    @Override
    public void addStateChangedHandler(EventListener<DeviceChangedEvent> listener) {
        stateChangedEventHandler.registerListener(listener);
    }

    @Override
    public void removeStateChangedHandler(EventListener<DeviceChangedEvent> listener) {
        stateChangedEventHandler.unregisterListener(listener);
    }


    public void addCashReceivedHandler(EventListener<CashReceivedEvent> listener) {
        cashReceivedEventHandler.registerListener(listener);
    }

    public void removeCashReceivedHandler(EventListener<CashReceivedEvent> listener) {
        cashReceivedEventHandler.unregisterListener(listener);
    }

    public class VirtualCassette {
        private CashIn cashInDevice = null;
        private final Lock cashCounterLock = new ReentrantLock();
        public Map<Integer, Integer> cashCounterDictionary = new HashMap<>();// <Key: Cash ID, Value: Count>
        private int cashCount = 0;

        private final EventListener<CashReceivedEvent> cashReceivedEventEventListener = this::CashReceivedCassetteHandler;

        public int getCashCount() {
            return this.cashCount;
        }

        public VirtualCassette(CashIn cashInDevice) {
            this.cashInDevice = cashInDevice;

            this.cashInDevice.cashDictionary.values().forEach(cashInfo -> cashCounterDictionary.put(cashInfo.getId(), 0));
            this.cashInDevice.addCashReceivedHandler(cashReceivedEventEventListener);
            this.DetermineCashCounter(null);
        }

        public void Deinitialize() {
            this.cashInDevice.removeCashReceivedHandler(cashReceivedEventEventListener);
        }

        private void CashReceivedCassetteHandler(CashReceivedEvent cashArgs) {
            EventManager<CashMoney> cashReceivedEventHandler = new EventManager<>();
            cashReceivedEventHandler.registerListener(this::DetermineCashCounter);
            cashReceivedEventHandler.beginInvoke(cashArgs);
        }

        private void DetermineCashCounter(CashReceivedEvent cashArgs) {
            try {
                cashCounterLock.lock();
                String cashCounterStr = this.cashInDevice.hardware.GetStaticValue(this.cashInDevice.payboxDeviceTypeName + ".CashCounter");
                int cashCounter = IntegerHelper.TryParse(cashCounterStr, 0);

                this.cashInDevice.hardware.Log(this.cashInDevice.typeName, "CashCounter: " + cashCounterStr, LogType.Information);

                if (cashArgs != null) {
                    this.cashInDevice.hardware.SaveStaticValue(this.cashInDevice.payboxDeviceTypeName + ".CashCounter", (++cashCounter) + "");
                    this.cashInDevice.hardware.Log(this.cashInDevice.typeName, "CashCounter New: " + cashCounter + "", LogType.Information);

                    //Counters Details
                    String cashIDCounterStr = this.cashInDevice.hardware.GetStaticValue(this.cashInDevice.payboxDeviceTypeName + ".CashCounterID" + cashArgs.getValue().getId());
                    int cashIDCounter = IntegerHelper.TryParse(cashIDCounterStr, 0);
//                    int.TryParse(cashIDCounterStr, out cashIDCounter);
                    this.cashInDevice.hardware.Log(this.cashInDevice.typeName, "CashCounterID" + cashArgs.getValue().getId() + ": " + cashIDCounterStr, LogType.Information);
                    this.cashInDevice.hardware.SaveStaticValue(this.cashInDevice.payboxDeviceTypeName + ".CashCounterID" + cashArgs.getValue().getId(), (++cashIDCounter) + "");
                    this.cashInDevice.hardware.Log(this.cashInDevice.typeName, "CashCounterID New" + cashArgs.getValue().getId() + ": " + cashIDCounter + "", LogType.Information);
                }

                this.cashCount = cashCounter;

                this.cashInDevice.cassetteFullError = !this.cashInDevice.hasCassetteFullError && (cashCounter >= this.cashInDevice.cashLimitInCassette);
            } finally {
                cashCounterLock.unlock();
            }
        }

        public void OnCassetteSet() {
            this.cashInDevice.hardware.Log(this.cashInDevice.typeName, "OnCassetteSet()", LogType.Information);

            CashInDeviceState stateArgs = (CashInDeviceState) this.cashInDevice.GetState(false);
            stateArgs.setCashInState(CashInState.CassetteSet);
            this.cashInDevice.OnStateChanged(new DeviceChangedEvent(this, stateArgs));
        }

        public void OnCassetteGet() {
            this.cashInDevice.hardware.Log(this.cashInDevice.typeName, "OnCassetteGet()", LogType.Information);

            CashInDeviceState stateArgs = (CashInDeviceState) this.cashInDevice.GetState(false);
            stateArgs.setCashInState(CashInState.CassetteGet);
            this.cashInDevice.OnStateChanged(new DeviceChangedEvent(this, stateArgs));
        }

        public void OnCollectionHappend() {
            this.cashInDevice.hardware.Log(this.cashInDevice.typeName, "OnCollectionHappend(), Clearing Counters", LogType.Information);

            try {
                cashCounterLock.lock();
                this.cashInDevice.hardware.SaveStaticValue(this.cashInDevice.payboxDeviceTypeName + ".CashCounter", "0");
                for (CashMoney cashMoney : this.cashInDevice.cashDictionary.values()) {
                    this.cashInDevice.hardware.SaveStaticValue(this.cashInDevice.payboxDeviceTypeName + ".CashCounterID" + cashMoney.getId(), "0");
                }

                //Get counters names from cashDictionary, if cashDictionary is empty by some reason (ex: device initialization failed) get counters names from cash config files

                if (this.cashInDevice.cashDictionary.isEmpty()) {
                    this.cashInDevice.hardware.Log(this.cashInDevice.typeName, "OnCollectionHappend(): cashDictionary is empty, get counters names from config files", LogType.Warning);

                    //Get cash info from configuration files
                    XmlNodeList cashNodeList = this.cashInDevice.hardware.GetConfig(this.cashInDevice.typeName, this.cashInDevice.payboxDeviceTypeName).getDocumentElement().getElementsByTagName("CashInfo").get(0).getChildNodes();
                    XmlNodeList cashInfoNodeList = this.cashInDevice.hardware.GetConfig("CashInfo", "").getDocumentElement().getChildNodes();
                    for (XmlNode cashNode : cashNodeList) {
                        for (XmlNode cashInfoNode : cashInfoNodeList) {
                            String cashID = cashNode.getAttributes().get("id").getValue();
                            if (cashID.equals(cashInfoNode.getAttributes().get("id").getValue())) {
                                this.cashInDevice.hardware.SaveStaticValue(this.cashInDevice.payboxDeviceTypeName + ".CashCounterID" + cashID, "0");
                                break;
                            }
                        }
                    }

                }
            } finally {
                cashCounterLock.unlock();
            }
            this.cashInDevice.Reset();
        }
    }

    @Override
    public Map<String, Integer> GetDeviceCountersDictionary() {
        Map<String, Integer> countersDictionary = new HashMap<>();

        try {
            //hardware.Log(typeName, "GetDeviceCountersDictionary(): cashDictionary elements count: " + this.cashDictionary.Count, LogType.Information);

            for (CashMoney cashMoney : this.cashDictionary.values()) {
                String counterName = "CashCounterID" + cashMoney.getId();
                String cashCounterStr = this.hardware.GetStaticValue(this.payboxDeviceTypeName + "." + counterName);
                int cashCounter = IntegerHelper.TryParse(cashCounterStr, 0);

                countersDictionary.put(counterName, cashCounter);
            }


            //Get counters names from cashDictionary, if cashDictionary is empty by some reason (ex: device initialization failed) get counters names from cash config files

            if (countersDictionary.isEmpty()) {
                hardware.Log(typeName, "GetDeviceCountersDictionary(): cashDictionary is empty, get counters names from config files", LogType.Warning);

                //Get cash info from configuration files
                XmlNodeList cashNodeList = this.hardware.GetConfig(typeName, this.payboxDeviceTypeName).getDocumentElement().getElementsByTagName("CashInfo").get(0).getChildNodes();
                XmlNodeList cashInfoNodeList = this.hardware.GetConfig("CashInfo", "").getDocumentElement().getChildNodes();


                for (XmlNode cashNode : cashNodeList) {
                    for (XmlNode cashInfoNode : cashInfoNodeList) {
                        String cashID = cashNode.getAttributes().get("id").getValue();
                        if (cashID.equals(cashInfoNode.getAttributes().get("id").getValue())) {
                            String counterName = "CashCounterID" + cashID;
                            String cashCounterStr = this.hardware.GetStaticValue(this.payboxDeviceTypeName + "." + counterName);
                            int cashCounter = IntegerHelper.TryParse(cashCounterStr, 0);
                            countersDictionary.put(counterName, cashCounter);
                            break;
                        }
                    }
                }


            }
        } catch (Exception ex) {
            hardware.Log(typeName, "GetDeviceCountersDictionary(): " + ex.getMessage(), LogType.Error);
        }

        return countersDictionary;
    }

    private volatile int accepted = -1;
    private volatile int rejected = -1;

    public void GetAcceptRejectCounters(MutableInt accepted, MutableInt rejected) {
        if (this.accepted == -1) {
            String strAccepted = hardware.GetStaticValue(this.payboxDeviceTypeName + ".AcceptedCashCounter");

//            if (!IntegerHelper.TryParse(strAccepted, out this.accepted))
            this.accepted = IntegerHelper.TryParse(strAccepted, 0);
        }
        if (this.rejected == -1) {
            String strRejected = hardware.GetStaticValue(this.payboxDeviceTypeName + ".RejectedCashCounter");

            this.rejected = IntegerHelper.TryParse(strRejected, 0);
        }

        accepted.setValue(this.accepted);
        rejected.setValue(this.rejected);
    }

    protected void IncrementAccepted() {
        if (this.accepted == -1) {
            String strAccepted = hardware.GetStaticValue(this.payboxDeviceTypeName + ".AcceptedCashCounter");

            this.accepted = IntegerHelper.TryParse(strAccepted, 0);
        }
        hardware.SaveStaticValue(this.payboxDeviceTypeName + ".AcceptedCashCounter", (++accepted) + "");
    }

    protected void IncrementRejected() {
        if (this.rejected == -1) {
            String strRejected = hardware.GetStaticValue(this.payboxDeviceTypeName + ".RejectedCashCounter");

            this.rejected = IntegerHelper.TryParse(strRejected, 0);
        }
        hardware.SaveStaticValue(this.payboxDeviceTypeName + ".RejectedCashCounter", (++rejected) + "");
    }

    protected String prashivkaVersion; //trololol

    public final String getPrashivkaVersion() {
        return prashivkaVersion;
    }


}
