package com.pay.base;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by dito on 4/5/2017.
 */
public class DeviceStateMonitoringWrapper implements Serializable {
    public DeviceStateMonitoringWrapper(String deviceType, int stateEnum, ArrayList<Integer> errorCodes) {
        setDeviceType(deviceType);
        setStateEnum(stateEnum);
        setErrorCodes(errorCodes);
    }

    public DeviceStateMonitoringWrapper() {
    }

    private String DeviceType;

    public final String getDeviceType() {
        return DeviceType;
    }

    public final void setDeviceType(String value) {
        DeviceType = value;
    }

    private int StateEnum;

    public final int getStateEnum() {
        return StateEnum;
    }

    public final void setStateEnum(int value) {
        StateEnum = value;
    }

    private ArrayList<Integer> ErrorCodes;

    public final ArrayList<Integer> getErrorCodes() {
        return ErrorCodes;
    }

    public final void setErrorCodes(ArrayList<Integer> value) {
        ErrorCodes = value;
    }
}