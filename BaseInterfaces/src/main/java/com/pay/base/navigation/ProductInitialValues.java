package com.pay.base.navigation;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dito on 4/4/2017.
 */
public class ProductInitialValues {
    public ProductInitialValues()
    {
        setInputFields(new HashMap<String, Object>());
        setMetaProperties(new ArrayList<ActionStepMetaProperty>());
    }

    private HashMap<String, Object> InputFields;
    public final HashMap<String, Object> getInputFields()
    {
        return InputFields;
    }
    public final void setInputFields(HashMap<String, Object> value)
    {
        InputFields = value;
    }

    private ArrayList<ActionStepMetaProperty> MetaProperties;
    public final ArrayList<ActionStepMetaProperty> getMetaProperties()
    {
        return MetaProperties;
    }
    public final void setMetaProperties(ArrayList<ActionStepMetaProperty> value)
    {
        MetaProperties = value;
    }
}
