package com.pay.base.navigation;

import com.pay.base.navigation.enums.MetaPropertyType;

/**
 * Created by dito on 4/4/2017.
 */
public class ActionStepMetaProperty {
    public ActionStepMetaProperty(String name, Object value, MetaPropertyType propertyType)
    {
        if (name == null)
        {
            throw new IllegalArgumentException("name");
        }

        setName(name);

        setValue(value);

        setPropertyType(propertyType);
    }

    public ActionStepMetaProperty()
    {
    }

    /**
     Gets or sets name of meta property.
     */
    private String Name;
    public final String getName()
    {
        return Name;
    }
    public final void setName(String value)
    {
        Name = value;
    }

    /**
     Gets or sets value of meta property.
     */
    private Object Value;
    public final Object getValue()
    {
        return Value;
    }
    public final void setValue(Object value)
    {
        Value = value;
    }

    private MetaPropertyType PropertyType = MetaPropertyType.values()[0];
    public final MetaPropertyType getPropertyType()
    {
        return PropertyType;
    }
    public final void setPropertyType(MetaPropertyType value)
    {
        PropertyType = value;
    }

    public final ActionStepMetaProperty Clone()
    {
        // ReSharper disable RedundantThisQualifier
        ActionStepMetaProperty tempVar = new ActionStepMetaProperty();
        tempVar.setName(getName());
        tempVar.setPropertyType(getPropertyType());
        tempVar.setValue(getValue());
        return tempVar;
        // ReSharper restore RedundantThisQualifier
    }

    @Override
    public boolean equals(Object value)
    {
        if (value == null)
        {
            return false;
        }

        ActionStepMetaProperty anotherProperty = (ActionStepMetaProperty)((value instanceof ActionStepMetaProperty) ? value : null);

        if (anotherProperty == null)
        {
            return false;
        }

        return getName().equals(anotherProperty.getName()) && getPropertyType() == anotherProperty.getPropertyType() && getValue().equals(anotherProperty.getValue());
    }

    @Override
    public int hashCode()
    {
        return ((getName().hashCode() * 17 + getValue().hashCode()) * 19 + getPropertyType().hashCode()) * 23;
    }
}
