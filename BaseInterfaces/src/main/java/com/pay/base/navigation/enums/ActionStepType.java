package com.pay.base.navigation.enums;

/**
 * Created by dito on 4/4/2017.
 */
public enum ActionStepType
{
    /**
     Root step, or root category.
     */
    Root,

    /**
     General product or provider category.
     */
    Category,

    /**
     Product
     */
    Product,

    /**
     Specifies that step is collecting some meta data.
     */
    MetaPropertyCollection,

    /**
     Product-specific step type.
     */
    ProductSpecific,

    /**
     Step, helping to select a region from predefined configuration tree.
     */
    RegionSelection;

    public static final int SIZE = java.lang.Integer.SIZE;

    public int getValue()
    {
        return this.ordinal();
    }

    public static ActionStepType forValue(int value)
    {
        return values()[value];
    }
}