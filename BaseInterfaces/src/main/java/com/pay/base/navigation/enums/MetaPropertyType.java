package com.pay.base.navigation.enums;

/**
 * Created by dito on 4/4/2017.
 */
public enum MetaPropertyType
{
    Text,

    Region,

    ProviderIndex,

    TermsAgreement,

    NavigationProviderLogo,

    NavigationColumnHeader,

    NavigationCategoryHeader,

    NavigationMetaCategoryInformationImage,

    ProductStepIsVisible,

    AutoProductSelection,

    HideNavigationEntry,

    ProductProviderName,

    AlternativeCategoryPage,

    PortalEnableCardPayment,

    PortalHidden,

    CardMerchantId,

    PortalEnableUserInputSave,

    PortalProductName,

    PortalPaymentTemplateType,

    ExcludeFromStandingOrders,

    AlternateRegionSelector;

    public static final int SIZE = java.lang.Integer.SIZE;

    public int getValue()
    {
        return this.ordinal();
    }

    public static MetaPropertyType forValue(int value)
    {
        return values()[value];
    }
}