package com.pay.base.navigation;

import com.pay.base.navigation.enums.ActionStepType;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dito on 4/4/2017.
 */
public class ActionStep {
    public ActionStep(Object stepId, ActionStepType type)
    {
        setID(stepId);

        MetaProperties = new ArrayList<ActionStepMetaProperty>();

        setLocaleSpecificNames(new HashMap<String, String>());

        setStepType(type);

        setIsVisible(true);
    }

    /**
     Gets ID of the action step.
     */
    private Object ID;
    public final Object getID()
    {
        return ID;
    }
    private void setID(Object value)
    {
        ID = value;
    }

    /**
     Gets the list of meta properties, associated with the step.
     */
    private ArrayList<ActionStepMetaProperty> MetaProperties;
    public final ArrayList<ActionStepMetaProperty> getMetaProperties()
    {
        return MetaProperties;
    }

    /**
     Gets or sets the name of the step.
     */
    private String Name;
    public final String getName()
    {
        return Name;
    }
    public final void setName(String value)
    {
        Name = value;
    }

    private HashMap<String, String> LocaleSpecificNames;
    public final HashMap<String, String> getLocaleSpecificNames()
    {
        return LocaleSpecificNames;
    }
    private void setLocaleSpecificNames(HashMap<String, String> value)
    {
        LocaleSpecificNames = value;
    }

    /**
     Gets the type of action step.
     */
    private ActionStepType StepType = ActionStepType.values()[0];
    public final ActionStepType getStepType()
    {
        return StepType;
    }
    private void setStepType(ActionStepType value)
    {
        StepType = value;
    }

    private boolean IsVisible;
    public final boolean getIsVisible()
    {
        return IsVisible;
    }
    public final void setIsVisible(boolean value)
    {
        IsVisible = value;
    }

    @Override
    public String toString()
    {
        return String.format("ID = %1$s; Name = %2$s; IsVisible = %5$s; StepType = %3$s; MetaProperties = %4$s;", getID(), getName(), getStepType(), getMetaProperties(), getIsVisible());
    }
}
