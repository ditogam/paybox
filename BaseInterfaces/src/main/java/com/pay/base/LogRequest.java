package com.pay.base;

/**
 * Created by dito on 4/5/2017.
 */
public class LogRequest
{
    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [DataMember] public bool IsRequested {get;set;}
    private boolean IsRequested;
    public final boolean getIsRequested()
    {
        return IsRequested;
    }
    public final void setIsRequested(boolean value)
    {
        IsRequested = value;
    }

    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [DataMember] public DateTime LogDate {get;set;}
    private java.time.LocalDateTime LogDate = java.time.LocalDateTime.MIN;
    public final java.time.LocalDateTime getLogDate()
    {
        return LogDate;
    }
    public final void setLogDate(java.time.LocalDateTime value)
    {
        LogDate = value;
    }

    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [DataMember] public int ID {get;set;}
    private int ID;
    public final int getID()
    {
        return ID;
    }
    public final void setID(int value)
    {
        ID = value;
    }
}