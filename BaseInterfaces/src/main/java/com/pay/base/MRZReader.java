package com.pay.base;


import com.pay.base.devicestate.DeviceState;
import com.pay.base.event.EventManager;
import com.pay.base.event.types.Delegates;
import com.pay.base.event.types.DeviceChangedEvent;
import com.pay.base.event.types.MRZReaderEvent;
import com.pay.helper.enums.LogType;

import java.util.HashMap;

public abstract class MRZReader implements IHardwareBase {
    protected boolean _listeningState;
    protected boolean configIsValid = false;

    protected HashMap<Integer, Error> errorDictionary = new HashMap<Integer, Error>();
    protected IHardware hardware;
    protected boolean initialized = false;
    protected String payboxDeviceTypeName = "";

    private EventManager<MRZPerson> mRZReaderEventHandler = new EventManager<>();
    private EventManager<DeviceState> stateChangedEventHandler = new EventManager<>();

    protected String typeName = "";


    public final void OnDataRead(Object sender, MRZPerson person) {
        if (mRZReaderEventHandler != null) {
            if (_listeningState) {
                mRZReaderEventHandler.invoke(new MRZReaderEvent(sender, person));
            }
        }
    }


    public final void OnStateChanged(DeviceChangedEvent event) {
        if (stateChangedEventHandler != null) {
            stateChangedEventHandler.invoke(event);
        } else {
            hardware.Log("StateChange", "MRZ is null", LogType.Warning);
        }
    }

    public final void StartListening() {
        _listeningState = true;
    }

    public final void StopListening() {
        _listeningState = false;
    }

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#region IHardwareBase Members

    public abstract boolean Initialize();

    public abstract boolean Deinitialize();

    public abstract boolean Reset();

    public abstract DeviceState GetState(boolean force);


    public final void SoftwareActionToDeviceHandler(Object sender, SoftwareActionToDevice actionArgs) {
    }

    public final HashMap<String, Integer> GetDeviceCountersDictionary() {
        return null;
    }

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#endregion
}