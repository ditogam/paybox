package com.pay.base;

/**
 * Created by dito on 4/4/2017.
 */
public interface ILightweightUnitedMobilePayment
{
    IServerContainer getContainer();

    String getPhoneNumber();

    int getAmount();

    boolean ReadProductConfig();

    void PerformPayment();
}
