package com.pay.base;

public class MRZPerson
{
	private String FirstName;
	public final String getFirstName()
	{
		return FirstName;
	}
	public final void setFirstName(String value)
	{
		FirstName = value;
	}
	private String LastName;
	public final String getLastName()
	{
		return LastName;
	}
	public final void setLastName(String value)
	{
		LastName = value;
	}
	private String Country;
	public final String getCountry()
	{
		return Country;
	}
	public final void setCountry(String value)
	{
		Country = value;
	}
	private String PassportNumber;
	public final String getPassportNumber()
	{
		return PassportNumber;
	}
	public final void setPassportNumber(String value)
	{
		PassportNumber = value;
	}
	private String Nationality;
	public final String getNationality()
	{
		return Nationality;
	}
	public final void setNationality(String value)
	{
		Nationality = value;
	}
	private String BirthDate;
	public final String getBirthDate()
	{
		return BirthDate;
	}
	public final void setBirthDate(String value)
	{
		BirthDate = value;
	}
	private char Sex;
	public final char getSex()
	{
		return Sex;
	}
	public final void setSex(char value)
	{
		Sex = value;
	}
	private String ExpirationDate;
	public final String getExpirationDate()
	{
		return ExpirationDate;
	}
	public final void setExpirationDate(String value)
	{
		ExpirationDate = value;
	}

	public final String getBirthDateFormatted()
	{
		String yy = getBirthDate().substring(0, 2);
		String mm = getBirthDate().substring(2, 4);
		String dd = getBirthDate().substring(4, 6);
		return dd + "/" + mm + "/" + yy;
	}

	@Override
	public String toString()
	{
		return "FirstName: " + getFirstName() + " " + "LastName: " + getLastName() + " " + "Country: " + getCountry() + " " + "PassportNumber: " + getPassportNumber() + " " + "Nationality: " + getNationality() + " " + "BirthDate: " + getBirthDate() + " " + "Sex: " + getSex() + " " + "ExpirationDate: " + getExpirationDate() + ";";
	}
}