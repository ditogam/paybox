package com.pay.base;

import com.pay.base.navigation.ActionStep;
import com.pay.helper.CultureInfo;

import java.util.HashMap;

/**
 * Created by dito on 4/4/2017.
 */
public interface ITemplateModule {
    IServerContainer getContainer();
    void setContainer(IServerContainer value);

    // template resource module
    ITemplateResource getTemplateResource();

    String getProductName();
    void setProductName(String value);

    /**
     Processing Product ID, used to identify processing route ID.
     */
    int getProcessingProductId();
    void setProcessingProductId(int value);

    ///// <summary>
    ///// Product ID as specified in Navigation tree.
    ///// </summary>
    //int ProductId { get; set; }

    /**
     True, if component is Navigation component, False otherwise.
     */
    boolean getIsNavigation();

    /**
     True, if component is Maintenance componet, False otherwise.
     */
    boolean getIsMaintenance();

    /**
     List of devices, required for product template to proceed.
     */
    HashMap<String, Class> getRequiredDeviceTypes();

    String getConfigName();
    void setConfigName(String value);

    CultureInfo getCurrentLocale();
    void setCurrentLocale(CultureInfo value);

    String getCurrentLogSessionID();

    /**
     Template module initialization routines.
     */
    void Load();

    /**
     Overloaded method, initialise input fields from passed parameters

     @param initialValues Hashtable, which stores initial parameter values
     */
    void Load(HashMap<String, Object> initialValues);

    /**
     Template module clean-up routines.
     */
    void Unload();

    /**
     Template module handler of web page load event.

     @param sender Event originator object.
     @param eargs DocumentCompleted event arguments.
     */
    void OnWebPageLoad(Object sender, WebBrowserDocumentCompletedEventArgs eargs);

    /**
     Launch syncrhonous dynamic call of current service template's method.

     @param methodName Name of method to execute.
     @param arguments Array of arguments, passed to requested method.
     */
    void ExecuteSynchronous(String methodName, Object... arguments);

    /**
     Launch asyncrhonous dynamic call of current service template's method.

     @param methodName Name of method to execute.
     @param arguments Array of arguments, passed to requested method.
     */
    void ExecuteAsynchronous(String methodName, Object... arguments);

    /**
     Get Amount in GEL from cashmoney

     @param args
     @return
     */
    int GetConvertedAmount(CashMoney args);

    /**
     Switch current web form locale

     @param localeString String in form language-Region.
     */
    void SwitchLocale(String localeString);

    void OnActionStepTaken(ActionStep step);
}
