package com.pay.base;

public class SigCaptureEventArgs
{
	public SigCaptureEventArgs(String file)
	{
		FilePath = file;
	}

	private String FilePath;
	public final String getFilePath()
	{
		return FilePath;
	}
}