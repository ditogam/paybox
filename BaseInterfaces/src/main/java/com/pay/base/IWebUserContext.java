package com.pay.base;

import com.pay.base.enums.NavigationMode;
import com.pay.base.enums.PaymentSpecValueType;
import com.pay.base.enums.UserPaymentType;
import com.pay.base.navigation.ProductInitialValues;
import com.pay.helper.RefObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dito on 4/4/2017.
 */
public interface IWebUserContext
{
    int getWebUserId();

    String getGroupName();

    int getBalance();

    int getBalancePaymentLimit();

    int getBlockedAmount();

    String getPtWebClientID();

    String getPhoneNumber();

    String getFirstName();
    void setFirstName(String value);

    String getLastName();
    void setLastName(String value);

    String getPersonalNumber();
    void setPersonalNumber(String value);

    boolean getInfoVerified();
    void setInfoVerified(boolean value);

    boolean getInfoPartiallyFilled();

    double getPoints();

    double getBlockedPoints();

    HashMap<String, ArrayList<ProductInitialValues>> getMyProductsList();

    String getVerificationHash();

    String getSessionID();

    UserPaymentSpecs getDefaultDiscount();
    ArrayList<UserPaymentSpecs> getProductDiscounts();

    UserPaymentSpecs getDefaultPointAddition();
    ArrayList<UserPaymentSpecs> getProductPointAddition();

    UserPaymentHistoryRow[] getPaymentHistory();

    /**
     Offer user to continue browsing cabinet or log out
     */
    boolean getOfferContinue();

    int getLogInCount();
    void setLogInCount(int value);

    boolean getToolTipHidden();
    void setToolTipHidden(boolean value);

    java.time.LocalDateTime getBirthDate();
    void setBirthDate(java.time.LocalDateTime value);

    java.time.LocalDateTime getLastOrderDate();
    void setLastOrderDate(java.time.LocalDateTime value);

    java.time.LocalDateTime getNextOrderDate();
    void setNextOrderDate(java.time.LocalDateTime value);

    NavigationMode getNavigationMode();
    void setNavigationMode(NavigationMode value);

    boolean getIsAlive();
    void setIsAlive(boolean value);

    int getGiftId();

    /**
     defines how much points would be added to user after payment

     @param productId
     @param amount
     @param paymentType
     @return
     */
    double GetAddedPoints(int productId, int amount, UserPaymentType paymentType);

    /**
     gets commission discount value and type for user

     @param productId
     @param discountType
     @param paymentType
     @return
     */
    int GetCommissionDiscount(int productId, RefObject<PaymentSpecValueType> discountType, UserPaymentType paymentType);

    void PerformPayment(int productId, int amount, UserPaymentType paymentType);

    void MyProductListEdit(String configName, String invoiceData, String invoiceDataOld);

    void MyProductListNew(String configName, ProductInitialValues initialValues);

    void MyProductListNewAsync(String configName, ProductInitialValues initialValues);

    void MyProductListDelete(String configName, ProductInitialValues initialValues);

    void MyProductListDeleteAsync(String configName, ProductInitialValues initialValues);

    void GetTransactionHistory();

    int SMSServiceVerify();

    void SMSServiceProlong();

    void SMSServiceExpire();


    boolean Validate();
    //C# TO JAVA CONVERTER NOTE: Java does not support optional parameters. Overloaded method(s) are created above:
//ORIGINAL LINE: bool Validate(string pinNumber = "");
    boolean Validate(String pinNumber);

    String GetPayHash(String input);

    void OrderGift();

    void UserInfoAdded();

    void StartGiftOrder(int giftId, double diftPointPrice);
}