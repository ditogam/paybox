package com.pay.base;

import com.pay.base.enums.PayboxState;
import com.pay.base.event.EventListener;
import com.pay.base.event.types.SoftwareActionToDeviceEvent;
import com.pay.base.navigation.ActionStep;
import com.pay.base.navigation.StepTrace;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dito on 4/3/2017.
 */
public interface IGeneralLogicModule {
    IServerContainer getContainer();
    void setContainer(IServerContainer value);
    int getCurrentAmountOnBalance();
    void setCurrentAmountOnBalance(int value);
    String getCurrentTransactionIRRN();
    void setCurrentTransactionIRRN(String value);
    Object getAmountLockObject();
    // #BEGIN Obsolete
    // bool IsInOutOfServiceMode { get; }
    // #END Obsolete
    HashMap<String, Integer> getUserInterfaceTimeouts();
    StepTrace getUserStepTrace();
    String getDefaultUILocale();
    ArrayList<String> getSupportedUILocales();
    String getCurrentUILocale();
    void setCurrentUILocale(String value);
    String getPayboxGroupID();
    String getTerminalName();
    IWebUserContext getUserContext();
    void setUserContext(IWebUserContext value);
    HashMap<String, Integer> getProductConfigIdMap();
    void setProductConfigIdMap(HashMap<String, Integer> value);
    //C# TO JAVA CONVERTER TODO TASK: This event cannot be converted to Java:
//	event CashReceivedEventHandler CashReceived;
//C# TO JAVA CONVERTER TODO TASK: This event cannot be converted to Java:
//	event StateChangedEventHandler StateChanged;
//C# TO JAVA CONVERTER TODO TASK: This event cannot be converted to Java:
//	event SoftwareActionToDeviceEventHanler SoftwareActionToDevice;
    void Initialize();
    void SendSoftwareActionToDevice(SoftwareActionToDevice actionArgs);
    // #BEGIN Obsolete
    //void NavigateHome();
    //void NavigateToCategory(string categoryId);
    //void NavigateToUrl(string pageUrl);
    //void NavigateLast();
    // #END Obsolete
	/* void MoveToModule(string moduleTypeName); */

    void SelectProduct(String productConfigName);
    //C# TO JAVA CONVERTER NOTE: Java does not support optional parameters. Overloaded method(s) are created above:
//ORIGINAL LINE: void SelectProduct(string productConfigName, int invoiceIndex = -1);
    void SelectProduct(String productConfigName, int invoiceIndex);
    //void StartMaintenance();
    void NavigateToStep(ActionStep step);
    //void ChangeToOfflineMode();
    //void ChangeToOutOfServiceMode();
    //void ChangeToOperationalMode();
    void SwitchMode(PayboxState payboxGlobalState);
    void TakeNavigationStep(int stepId);
    void RegisterWebPortalConnector(IWebPortalConnector webPortalConnector);
    void UnregisterWebPortalConnector();
    void PerformAsyncMobilePayment(ILightweightUnitedMobilePayment mobilePayment);
    boolean InitializeUserContext(String phoneNumber);



    void addStateChangedHandler(EventListener<SoftwareActionToDeviceEvent> listener);

    void removeStateChangedHandler(EventListener<SoftwareActionToDeviceEvent> listener);
}
