package com.pay.base;

import com.pay.helper.CultureInfo;

import java.util.HashMap;

/**
 * Created by dito on 4/4/2017.
 */
public interface ITemplateResource {
    ITemplateModule getTemplateModule();
    void setTemplateModule(ITemplateModule value);
    CultureInfo getLocale();
    void setLocale(CultureInfo value);
    String[] GetResourceKeys();
    String GetResource(String keyName, CultureInfo locale);
    String GenerateScriptResource(CultureInfo locale);
    void Load();
    void SwitchLocale(String ci);
    HashMap<String, ScriptResourceEntry> GetAllResourcesWithKeys(CultureInfo ci);
}
