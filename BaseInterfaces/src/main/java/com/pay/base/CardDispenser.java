package com.pay.base;


import com.pay.base.devicestate.DeviceState;
import com.pay.base.event.EventManager;
import com.pay.base.event.types.CardGivenEvent;
import com.pay.base.event.types.CardMovedCaptureBoxEvent;
import com.pay.base.event.types.Delegates;
import com.pay.base.event.types.DeviceChangedEvent;
import com.pay.helper.enums.LogType;

import java.util.HashMap;

public abstract class CardDispenser implements IHardwareBase {


    private EventManager<DeviceState> stateChangedEventHandler = new EventManager<>();
    private EventManager cardMovedCaptureBoxEventHandler = new EventManager();
    private EventManager cardGivenEventHandler = new EventManager();


    public final void OnStateChanged(DeviceChangedEvent event) {
        hardware.Log("CardDispenser", "OnStateChanged called", LogType.Information);
        if (stateChangedEventHandler != null) {
            stateChangedEventHandler.invoke(event);
        } else {
            hardware.Log("StateChange", "CardDispenser is null", LogType.Warning);
        }
    }

    protected boolean configIsValid = false;

    protected HashMap<Integer, Error> errorDictionary = new HashMap<Integer, Error>();
    protected IHardware hardware;
    protected boolean initialized = false;


    protected String payboxDeviceTypeName = "";

    protected String typeName = "";

    public abstract CardDataInformation ReadCard(int tryCount);

    public abstract boolean DispenseCard(int timeOutSec);

    public abstract void EmptyCardBox();

    public final void OnMoveCaptureBox(Object sender) {
        if (cardMovedCaptureBoxEventHandler != null) {
            cardMovedCaptureBoxEventHandler.invoke(new CardMovedCaptureBoxEvent(sender));
        }
    }


    public final void OnCardGiven(Object sender) {
        if (cardGivenEventHandler != null) {
            cardGivenEventHandler.invoke(new CardGivenEvent(sender));
        }
    }


    public abstract boolean Initialize();

    public abstract boolean Deinitialize();

    public abstract boolean Reset();

    public abstract DeviceState GetState(boolean force);


    public final void SoftwareActionToDeviceHandler(Object sender, SoftwareActionToDevice actionArgs) {
    }

    public final HashMap<String, Integer> GetDeviceCountersDictionary() {
        return null;
    }

}