package com.pay.base;

import com.pay.base.devicestate.DeviceState;
import com.pay.base.event.EventManager;
import com.pay.base.event.types.CardInsertedEvent;
import com.pay.base.event.types.CardRemovedEvent;
import com.pay.base.event.types.Delegates;
import com.pay.base.event.types.DeviceChangedEvent;
import com.pay.helper.enums.LogType;

import java.util.HashMap;

/**
 * Created by dito on 4/6/2017.
 */
public abstract class CardReader implements IHardwareBase {
    protected boolean _readingState;


    private EventManager<DeviceState> stateChangedEventHandler = new EventManager<>();
    private EventManager cardInsertedEventHandler = new EventManager();
    private EventManager cardRemovedEventHandler = new EventManager();


    public final void OnCardInsert(Object sender) {
        if (cardInsertedEventHandler != null) {
            if (_readingState) {
                cardInsertedEventHandler.invoke(new CardInsertedEvent(sender));
            }
        }
    }

    public final void OnCardRemoved(Object sender) {
        if (cardRemovedEventHandler != null) {
            if (_readingState) {
                cardRemovedEventHandler.invoke(new CardRemovedEvent(sender));
            }
        }
    }


    public final void OnStateChanged(DeviceChangedEvent event) {
        if (stateChangedEventHandler != null) {
            stateChangedEventHandler.invoke(event);
        } else {
            hardware.Log("StateChange", "CardReader is null", LogType.Warning);
        }
    }

    protected boolean configIsValid = false;

    protected HashMap<Integer, Error> errorDictionary = new HashMap<Integer, Error>();
    protected IHardware hardware;
    protected boolean initialized = false;
    protected String payboxDeviceTypeName = "";

    protected String typeName = "";


    public void StartReading() {
        _readingState = true;
    }

    public void StopReading() {
        _readingState = false;
    }

    public abstract CardReadStatus ReadCard(MRZPerson person);


    public abstract boolean Initialize();

    public abstract boolean Deinitialize();

    public abstract boolean Reset();

    public abstract DeviceState GetState(boolean force);


    public final void SoftwareActionToDeviceHandler(Object sender, SoftwareActionToDevice actionArgs) {
    }

    public final HashMap<String, Integer> GetDeviceCountersDictionary() {
        return null;
    }


}