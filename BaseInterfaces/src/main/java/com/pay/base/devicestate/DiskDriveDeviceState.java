package com.pay.base.devicestate;

import com.pay.base.Error;
import com.pay.base.devicestate.enums.DiskDriveState;
import com.pay.base.devicestate.enums.PrinterState;

import java.util.Map;

/**
 * Created by dito on 4/3/2017.
 */
public class DiskDriveDeviceState extends DeviceState {
    private DiskDriveState diskDriveState;


    public DiskDriveDeviceState(Class deviceType, String payboxDeviceTypeName, Map<Integer, Error> errorsDictionary, DiskDriveState diskDriveState) {
        super(deviceType, payboxDeviceTypeName, errorsDictionary);
        this.diskDriveState = diskDriveState;
    }

    @Override
    public boolean equals(Object o) {
        boolean baseEquals = super.equals(o);
        if (!baseEquals)
            return false;
        if (!(o instanceof DiskDriveDeviceState))
            return false;
        DiskDriveDeviceState newCashInDeviceState = (DiskDriveDeviceState) o;
        return this.diskDriveState.equals(newCashInDeviceState.diskDriveState);
    }

    @Override
    public String toString() {
        return super.toString() + String.format("; DiskDriveState:%s;", this.diskDriveState.toString());
    }

    public DiskDriveState getDiskDriveState() {
        return diskDriveState;
    }
}
