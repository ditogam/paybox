package com.pay.base.devicestate;

import com.pay.base.Error;
import com.pay.base.devicestate.enums.NetworkConnectionState;

import java.util.Map;

/**
 * Created by dito on 4/3/2017.
 */
public class NetworkConnectionDeviceState extends DeviceState {

    public NetworkConnectionState networkConnectionState;

    public String internetConnection;

    public String processingAddress;

    public String appServerAddress;

    public boolean active;

    public NetworkConnectionDeviceState(Class deviceType, String payboxDeviceTypeName, Map<Integer, Error> errorsDictionary, NetworkConnectionState networkConnectionState, String internetConnection, String processingAddress, String appServerAddress, boolean active) {
        super(deviceType, payboxDeviceTypeName, errorsDictionary);
        this.networkConnectionState = networkConnectionState;
        this.internetConnection = internetConnection;
        this.processingAddress = processingAddress;
        this.appServerAddress = appServerAddress;
        this.active = active;
    }

    public NetworkConnectionState getNetworkConnectionState() {
        return networkConnectionState;
    }

    public String getInternetConnection() {
        return internetConnection;
    }

    public String getProcessingAddress() {
        return processingAddress;
    }

    public String getAppServerAddress() {
        return appServerAddress;
    }

    public boolean isActive() {
        return active;
    }

    @Override
    public boolean equals(Object o) {
        boolean baseEquals = super.equals(o);
        if (!baseEquals)
            return false;
        if (!(o instanceof NetworkConnectionDeviceState))
            return false;
        NetworkConnectionDeviceState newNetworkConnectionDeviceState = (NetworkConnectionDeviceState) o;
        return this.networkConnectionState.equals(newNetworkConnectionDeviceState.networkConnectionState) &&
                this.internetConnection.equals(newNetworkConnectionDeviceState.internetConnection) &&
                this.processingAddress.equals(newNetworkConnectionDeviceState.processingAddress) &&
                this.appServerAddress.equals(newNetworkConnectionDeviceState.appServerAddress);
    }

    @Override
    public String toString() {
        return super.toString() + String.format("; NetworkConnectionState:%s; InternetConnection:%s; ProcessingAddress:%s; AppServerAddress:%s; IsActive:%s", this.networkConnectionState.toString(), this.internetConnection, this.processingAddress, this.appServerAddress, this.active);
    }
}
