package com.pay.base.devicestate;

import com.pay.base.Error;
import com.pay.base.devicestate.enums.VendingState;
import com.pay.base.devicestate.enums.WatchDogState;
import com.pay.base.vending.Vending;
import com.pay.base.watchdog.WatchDog;

import java.util.Map;

/**
 * Created by dito on 4/3/2017.
 */
public class VendingDeviceState extends DeviceState {

    public VendingState vendingState;
    public Map<Integer, Vending.Slot> slotDictionary;

    public int slotCapacity;

    public VendingDeviceState(Class deviceType, String payboxDeviceTypeName, Map<Integer, Error> errorsDictionary, VendingState vendingState, Map<Integer, Vending.Slot> slotDictionary, int slotCapacity) {
        super(deviceType, payboxDeviceTypeName, errorsDictionary);
        this.vendingState = vendingState;
        this.slotDictionary = slotDictionary;
        this.slotCapacity = slotCapacity;
    }

    public Map<Integer, Vending.Slot> getSlotDictionary() {
        return slotDictionary;
    }

    public void setSlotDictionary(Map<Integer, Vending.Slot> slotDictionary) {
        this.slotDictionary = slotDictionary;
    }

    public VendingState getVendingState() {
        return vendingState;
    }

    public int getSlotCapacity() {
        return slotCapacity;
    }

    @Override
    public boolean equals(Object o) {
        boolean baseEquals = super.equals(o);
        if (!baseEquals)
            return false;

        boolean mustNotify = false;

        if (!(o instanceof VendingDeviceState))
            return false;
        VendingDeviceState s = (VendingDeviceState) o;

        boolean equals = (this.vendingState.equals(s.vendingState)) && (this.slotDictionary.size() == s.slotDictionary.size()) && (this.slotCapacity == s.slotCapacity);
        if (equals)
            for (Integer key : slotDictionary.keySet()) {
                Vending.Slot slot = slotDictionary.get(key);
                if (!s.slotDictionary.containsKey(key)) {
                    equals = false;
                    break;
                }
            }
        return equals;
    }

    @Override
    public String toString() {
        StringBuilder errorCodesStr = new StringBuilder("");
        for (Vending.Slot slot : this.slotDictionary.values()) {
            errorCodesStr.append(slot.toString());
            errorCodesStr.append(",");
        }
        return super.toString() + String.format("; VendingDeviceState:%s; Connectors: %s", this.vendingState.toString(), errorCodesStr);
    }

}
