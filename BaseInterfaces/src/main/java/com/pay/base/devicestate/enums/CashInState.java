package com.pay.base.devicestate.enums;

/**
 * Created by dito on 4/3/2017.
 */
public enum CashInState
{
    PowerUp,
    Idle, //Allow cash
    UnitDisable, //Deny cash
    InProcess, //Proccesing cash
    CassetteSet,
    CassetteGet,
    NotWorking, //Problem Error
    Uninitialized //Device Initialization failed
}
