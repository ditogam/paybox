package com.pay.base.devicestate;

import com.pay.base.Error;
import com.pay.base.devicestate.enums.CardReaderState;
import com.pay.base.devicestate.enums.MRZReaderState;

import java.util.Map;

/**
 * Created by dito on 4/3/2017.
 */
public class CardReaderDeviceState extends DeviceState {
    private CardReaderState cardReaderState;

    public CardReaderDeviceState(Class deviceType, String payboxDeviceTypeName, Map<Integer, Error> errorsDictionary, CardReaderState cardReaderState) {
        super(deviceType, payboxDeviceTypeName, errorsDictionary);
        this.cardReaderState = cardReaderState;
    }

    public CardReaderState getCardReaderState() {
        return cardReaderState;
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o) && (o instanceof CardReaderDeviceState) && ((CardReaderDeviceState) o).cardReaderState.equals(cardReaderState);
    }
}
