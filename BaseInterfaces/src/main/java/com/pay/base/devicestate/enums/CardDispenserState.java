package com.pay.base.devicestate.enums;

/**
 * Created by dito on 4/3/2017.
 */

public enum CardDispenserState
{
    Connected,
    Disconnected,
    Uninitialized,
    CardEmpty,
    CaptureBoxFull
}