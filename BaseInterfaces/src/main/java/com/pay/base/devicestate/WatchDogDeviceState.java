package com.pay.base.devicestate;

import com.pay.base.Error;
import com.pay.base.devicestate.enums.HaspState;
import com.pay.base.devicestate.enums.WatchDogState;
import com.pay.base.watchdog.WatchDog;

import java.util.Map;

/**
 * Created by dito on 4/3/2017.
 */
public class WatchDogDeviceState extends DeviceState {

    public WatchDogState watchDogState;
    public Map<WatchDog.ConnectorName, WatchDog.Connector> connectorStatesDictionary;

    public WatchDogDeviceState(Class deviceType, String payboxDeviceTypeName, Map<Integer, Error> errorsDictionary, WatchDogState watchDogState, Map<WatchDog.ConnectorName, WatchDog.Connector> connectorStatesDictionary) {
        super(deviceType, payboxDeviceTypeName, errorsDictionary);
        this.watchDogState = watchDogState;
        this.connectorStatesDictionary = connectorStatesDictionary;
    }

    @Override
    public boolean equals(Object o) {
        boolean baseEquals = super.equals(o);
        if (!baseEquals)
            return false;

        boolean mustNotify = false;
        for (WatchDog.Connector connector : this.connectorStatesDictionary.values()) {
            if (!connector.getNotified()) {
                mustNotify = true;
                break;
            }
        }

        if (!(o instanceof WatchDogDeviceState))
            return false;
        WatchDogDeviceState newCashInDeviceState = (WatchDogDeviceState) o;
        return !mustNotify && this.watchDogState.equals(newCashInDeviceState.watchDogState);
    }

    @Override
    public String toString() {
        StringBuilder errorCodesStr = new StringBuilder("");
        for (WatchDog.Connector connector : this.connectorStatesDictionary.values()) {
            errorCodesStr.append(connector.toString());
            errorCodesStr.append(",");
        }
        return super.toString() + String.format("; WatchDogState:%s; Connectors: %s", this.watchDogState.toString(), errorCodesStr);
    }

}
