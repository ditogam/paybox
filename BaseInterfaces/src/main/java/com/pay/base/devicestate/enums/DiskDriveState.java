package com.pay.base.devicestate.enums;

/**
 * Created by dito on 4/3/2017.
 */
public enum  DiskDriveState {
    Uninitialized,
    Exist,
    Removed,
    Full
}
