package com.pay.base.devicestate;

import com.pay.base.Error;
import com.pay.base.devicestate.enums.CardDispenserState;

import java.util.Map;

/**
 * Created by dito on 4/3/2017.
 */
public class CardDispenserDeviceState extends DeviceState {

    private String deviceName;
    private CardDispenserState cardDispenserState;

    public CardDispenserDeviceState(Class deviceType, String payboxDeviceTypeName, Map<Integer, Error> errorsDictionary, String deviceName, CardDispenserState cardDispenserState) {
        super(deviceType, payboxDeviceTypeName, errorsDictionary);
        this.deviceName = deviceName;
        this.cardDispenserState = cardDispenserState;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public CardDispenserState getCardDispenserState() {
        return cardDispenserState;
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o) && (o instanceof CardDispenserDeviceState) && ((CardDispenserDeviceState) o).cardDispenserState.equals(cardDispenserState);
    }
}
