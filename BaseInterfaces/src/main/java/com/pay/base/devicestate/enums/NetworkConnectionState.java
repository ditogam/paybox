package com.pay.base.devicestate.enums;

/**
 * Created by dito on 4/3/2017.
 */
public enum NetworkConnectionState
{
    Connected,
    Disconnected,
    Uninitialized;
    public static final int SIZE = java.lang.Integer.SIZE;

    public int getValue()
    {
        return this.ordinal();
    }

    public static NetworkConnectionState forValue(int value)
    {
        return values()[value];
    }
}
