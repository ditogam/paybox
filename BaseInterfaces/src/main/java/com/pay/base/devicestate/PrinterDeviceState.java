package com.pay.base.devicestate;

import com.pay.base.Error;
import com.pay.base.devicestate.enums.PrinterState;

import java.util.Map;

/**
 * Created by dito on 4/3/2017.
 */
public class PrinterDeviceState extends DeviceState {
    private PrinterState printerState;

    public PrinterDeviceState(Class deviceType, String payboxDeviceTypeName, Map<Integer, Error> errorsDictionary, PrinterState printerState) {
        super(deviceType, payboxDeviceTypeName, errorsDictionary);
        this.printerState = printerState;
    }

    @Override
    public boolean equals(Object o) {
        boolean baseEquals = super.equals(o);
        if (!baseEquals)
            return false;
        if (!(o instanceof PrinterDeviceState))
            return false;
        PrinterDeviceState newCashInDeviceState = (PrinterDeviceState) o;
        return this.printerState.equals(newCashInDeviceState.printerState);
    }

    @Override
    public String toString() {
        return super.toString() + String.format("; PrinterState:%s;", this.printerState.toString());
    }

    public PrinterState getPrinterState() {
        return printerState;
    }
}
