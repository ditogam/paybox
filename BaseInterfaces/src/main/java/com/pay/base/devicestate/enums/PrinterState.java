package com.pay.base.devicestate.enums;

/**
 * Created by dito on 4/3/2017.
 */
public enum PrinterState
{
    OK,
    NotWorking,
    PaperNearEnd,
    OutOfPaper,
    HeadUp,
    Uninitialized
}