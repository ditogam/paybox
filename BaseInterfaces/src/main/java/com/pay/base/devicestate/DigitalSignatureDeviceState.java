package com.pay.base.devicestate;

import com.pay.base.Error;
import com.pay.base.devicestate.enums.CardReaderState;
import com.pay.base.devicestate.enums.DigitalSignatureState;

import java.util.Map;

/**
 * Created by dito on 4/3/2017.
 */
public class DigitalSignatureDeviceState extends DeviceState {
    private DigitalSignatureState digitalSignatureState;

    public DigitalSignatureDeviceState(Class deviceType, String payboxDeviceTypeName, Map<Integer, Error> errorsDictionary, DigitalSignatureState digitalSignatureState) {
        super(deviceType, payboxDeviceTypeName, errorsDictionary);
        this.digitalSignatureState = digitalSignatureState;
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o) && (o instanceof DigitalSignatureDeviceState) && ((DigitalSignatureDeviceState) o).digitalSignatureState.equals(digitalSignatureState);
    }
}
