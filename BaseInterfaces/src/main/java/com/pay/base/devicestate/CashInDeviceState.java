package com.pay.base.devicestate;

import com.pay.base.Error;
import com.pay.base.devicestate.enums.CashInState;
import com.pay.helper.Compare;

import java.util.Map;

/**
 * Created by dito on 4/3/2017.
 */
public class CashInDeviceState extends DeviceState {
    private CashInState cashInState;
    private int[] allowedCashIDList;


    public CashInDeviceState(Class deviceType, String payboxDeviceTypeName, Map<Integer, Error> errorsDictionary, CashInState cashInState, int[] allowedCashIDList) {
        super(deviceType, payboxDeviceTypeName, errorsDictionary);
        this.cashInState = cashInState;
        this.allowedCashIDList = allowedCashIDList;
    }

    public CashInState getCashInState() {
        return cashInState;
    }

    public void setCashInState(CashInState cashInState) {
        this.cashInState = cashInState;
    }

    public int[] getAllowedCashIDList() {
        return allowedCashIDList;
    }

    @Override
    public boolean equals(Object o) {
        boolean baseEquals = super.equals(o);
        if (!baseEquals)
            return false;
        if (!(o instanceof CashInDeviceState))
            return false;
        CashInDeviceState newCashInDeviceState = (CashInDeviceState) o;
        if (!this.cashInState.equals(newCashInDeviceState.cashInState))
            return false;
        if (allowedCashIDList.length != newCashInDeviceState.allowedCashIDList.length)
            return false;


        return Compare.compare(allowedCashIDList, newCashInDeviceState.allowedCashIDList);
    }

    @Override
    public String toString() {
        StringBuilder allowedCashIDListStr = new StringBuilder("");

        for (int i : this.allowedCashIDList) {
            allowedCashIDListStr.append(i + ",");
        }
        return super.toString() + String.format("; CashInState:%s; AllowedCashIDList:%s; ", this.cashInState.toString(), allowedCashIDListStr);
    }
}
