package com.pay.base.devicestate;

import com.pay.base.Error;
import com.pay.base.enums.StateCategory;

import java.util.Map;

/**
 * Created by dito on 4/3/2017.
 */
public abstract class DeviceState {
    private Class deviceType;
    private String payboxDeviceTypeName;
    private StateCategory category;
    private Map<Integer, Error> errorsDictionary;

    public DeviceState(Class deviceType, String payboxDeviceTypeName, Map<Integer, Error> errorsDictionary) {
        this.deviceType = deviceType;
        this.payboxDeviceTypeName = payboxDeviceTypeName;
        this.category = category;
        this.errorsDictionary = errorsDictionary;

        //Detect Category
        for (Error error : errorsDictionary.values()) {
            if (error == null) {
                this.category = StateCategory.Problem;
                break;
            }

            if (error.getCategory().equals(StateCategory.Problem)) {
                this.category = StateCategory.Problem;
                break;
            }
            if (error.getCategory().equals(StateCategory.Warning)) {
                this.category = StateCategory.Warning;
            } else if (!error.getCategory().equals(StateCategory.Warning)) {
                this.category = StateCategory.OK;
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        boolean equals = false;
        if (!(o instanceof DeviceState))
            return false;
        DeviceState newDeviceState = (DeviceState) o;
        equals = (errorsDictionary.size() == newDeviceState.errorsDictionary.size());
        for (Integer key : errorsDictionary.keySet()) {
            Error error = errorsDictionary.get(key);
            if (!newDeviceState.errorsDictionary.containsKey(key) || !error.equals(newDeviceState.errorsDictionary.get(key))) {
                equals = false;
                break;
            }
        }
        return equals;
    }

    private static final int hashSeedA = 17;

    private static final int hashSeedB = 23;


    @Override
//    @Unchec
    public int hashCode() {

//            unchecked
//            {
        int hash = hashSeedA;

        hash += hash * hashSeedB + deviceType.hashCode();

        hash += hash * hashSeedB + payboxDeviceTypeName.hashCode();

        hash += hash * hashSeedB + category.hashCode();

        hash += hash * hashSeedB + errorsDictionary.hashCode();

        return hash;
//            }
    }

    @Override
    public String toString() {
        StringBuilder errorCodesStr = new StringBuilder("");


        for (Error error : this.errorsDictionary.values()) {
            errorCodesStr.append(error.getCode());
            errorCodesStr.append(",");
        }

        return String.format("DeviceType:%s; PayboxDeviceTypeName:%s; Category%s; ErrorsDictionary:%s", this.deviceType.getName(), this.payboxDeviceTypeName, this.category.toString(), errorCodesStr);
    }

    public Class getDeviceType() {
        return deviceType;
    }

    public String getPayboxDeviceTypeName() {
        return payboxDeviceTypeName;
    }

    public StateCategory getCategory() {
        return category;
    }

    public Map<Integer, Error> getErrorsDictionary() {
        return errorsDictionary;
    }
}
