package com.pay.base.devicestate;

import com.pay.base.Error;
import com.pay.base.devicestate.enums.MRZReaderState;

import java.util.Map;

/**
 * Created by dito on 4/3/2017.
 */
public class MRZReaderDeviceState extends DeviceState {
    private MRZReaderState mrzReaderState;

    public MRZReaderDeviceState(Class deviceType, String payboxDeviceTypeName, Map<Integer, Error> errorsDictionary, MRZReaderState mrzReaderState) {
        super(deviceType, payboxDeviceTypeName, errorsDictionary);
        this.mrzReaderState = mrzReaderState;
    }

    public MRZReaderState getMrzReaderState() {
        return mrzReaderState;
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o) && (o instanceof MRZReaderDeviceState) && ((MRZReaderDeviceState) o).getMrzReaderState().equals(mrzReaderState);
    }
}
