package com.pay.base.devicestate;

import com.pay.base.Error;
import com.pay.base.devicestate.enums.HaspState;

import java.util.Map;

/**
 * Created by dito on 4/3/2017.
 */
public class HaspDeviceState extends DeviceState {

    public HaspState haspState;

    public HaspDeviceState(Class deviceType, String payboxDeviceTypeName, Map<Integer, Error> errorsDictionary, HaspState haspState) {
        super(deviceType, payboxDeviceTypeName, errorsDictionary);
        this.haspState = haspState;
    }

    @Override
    public boolean equals(Object o) {
        boolean baseEquals = super.equals(o);
        if (!baseEquals)
            return false;
        if (!(o instanceof PrinterDeviceState))
            return false;
        HaspDeviceState newCashInDeviceState = (HaspDeviceState) o;
        return this.haspState.equals(newCashInDeviceState.haspState);
    }

    @Override
    public String toString() {
        return super.toString() + String.format("; HaspState:%s;", this.haspState.toString());
    }
    public HaspState getHaspState() {
        return haspState;
    }
}
