package com.pay.base.devicestate.enums;

/**
 * Created by dito on 4/3/2017.
 */
public enum HaspState {
    OK(0),
    NotFound(1),
    CouldNotConnectToProcessingServer(2),
    HaspGeneralError(3),
    PaymentLimitExceed(4);

    int value;

    HaspState(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
