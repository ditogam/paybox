package com.pay.base;

public class CardDataInformation
{
	private String ICCID;
	public final String getICCID()
	{
		return ICCID;
	}
	public final void setICCID(String value)
	{
		ICCID = value;
	}
	private String IMSI;
	public final String getIMSI()
	{
		return IMSI;
	}
	public final void setIMSI(String value)
	{
		IMSI = value;
	}
	private String PhoneNumber;
	public final String getPhoneNumber()
	{
		return PhoneNumber;
	}
	public final void setPhoneNumber(String value)
	{
		PhoneNumber = value;
	}


	@Override
	public String toString()
	{
		return "ICCID: " + getICCID() + System.lineSeparator() + "IMSI: " + getIMSI() + System.lineSeparator() + "PhoneNumber: " + getPhoneNumber() + System.lineSeparator();
	}
}