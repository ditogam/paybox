package com.pay.base;

import com.pay.base.devicestate.DeviceState;
import com.pay.base.devicestate.DiskDriveDeviceState;
import com.pay.base.devicestate.enums.DiskDriveState;
import com.pay.base.event.EventListener;
import com.pay.base.event.EventManager;
import com.pay.base.event.types.DeviceChangedEvent;
import com.pay.base.event.types.SoftwareActionToDeviceEvent;
import com.pay.helper.enums.LogType;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by dito on 4/13/2017.
 */
public abstract class DiskDrive implements IHardwareBase {
    protected boolean configIsValid = false;
    protected DiskDriveDeviceState currentDiskDriveDeviceState;
    protected Object currentDiskDriveDeviceStateLock = new Object();
    protected Map<Integer, Error> errorDictionary = new HashMap<Integer, Error>();

    protected IHardware hardware;
    protected boolean initialized;
    protected volatile boolean monitorDiskDriveState = true;
    protected AtomicInteger monitoringCycleCount = new AtomicInteger();
    protected String payboxDeviceTypeName = "";
    protected Thread diskDriveStateMonitoringThread;

    protected int stateMonitoringIntervalInMiliSeconds;
    protected String typeName = "";

    protected final void DiskDriveStateMonitoring() {
        while (monitorDiskDriveState) {
            synchronized (currentDiskDriveDeviceStateLock) {
                DiskDriveDeviceState stateArgs = (DiskDriveDeviceState) GetStateForced();

                if (!stateArgs.equals(currentDiskDriveDeviceState)) {
                    OnStateChanged(new DeviceChangedEvent(null, stateArgs));
                    currentDiskDriveDeviceState = stateArgs;
                }
            }


            monitoringCycleCount.incrementAndGet();
            try {
                Thread.sleep(stateMonitoringIntervalInMiliSeconds);
            } catch (Exception e) {

            }
            monitoringCycleCount.incrementAndGet();


        }
    }

    protected abstract DeviceState GetStateForced();

    public final boolean Initialize() {
        try {
            if (configIsValid && !initialized) {
                synchronized (currentDiskDriveDeviceStateLock) {
                    currentDiskDriveDeviceState = new DiskDriveDeviceState(this.getClass(), payboxDeviceTypeName, new HashMap<Integer, Error>(), DiskDriveState.Uninitialized);
                }

                monitorDiskDriveState = true;
                diskDriveStateMonitoringThread = new Thread(() -> DiskDriveStateMonitoring());
                diskDriveStateMonitoringThread.start();

                initialized = true;
            }

            return initialized;
        } catch (RuntimeException ex) {
            hardware.Log(typeName, "Initialize()" + ex.getMessage(), LogType.Error);
            hardware.LogException(ex);
            return false;
        }
    }

    public final boolean Deinitialize() {
        if (initialized) {
            monitorDiskDriveState = false;

            while (diskDriveStateMonitoringThread.isAlive()) {
                ;
            }

            synchronized (currentDiskDriveDeviceStateLock) {
                currentDiskDriveDeviceState = new DiskDriveDeviceState(this.getClass(), payboxDeviceTypeName, new HashMap<Integer, Error>(), DiskDriveState.Uninitialized);
            }

            initialized = false;
        }

        return true;
    }

    public final boolean Reset() {
        if (Deinitialize()) {
            return Initialize();
        }
        return false;
    }

    public final DeviceState GetState(boolean force) {
        //if (force)
        //{
        //    int countTemp = this.monitoringCycleCount;

        //    while (this.monitoringCycleCount == countTemp && this.diskDriveStateMonitoringThread != null && this.diskDriveStateMonitoringThread.IsAlive)
        //    {
        //        Thread.Sleep(1000);
        //    }
        //}

        synchronized (currentDiskDriveDeviceStateLock) {
            return currentDiskDriveDeviceState;
        }
    }

    private EventManager<DeviceState> stateChangedEventHandler = new EventManager<>();

    @Override
    public void addStateChangedHandler(EventListener<DeviceChangedEvent> listener) {
        stateChangedEventHandler.registerListener(listener);
    }

    @Override
    public void removeStateChangedHandler(EventListener<DeviceChangedEvent> listener) {
        stateChangedEventHandler.unregisterListener(listener);
    }

    @Override
    public void OnStateChanged(DeviceChangedEvent event) {
        stateChangedEventHandler.invoke(event);
    }

    @Override
    public void SoftwareActionToDeviceHandler(SoftwareActionToDeviceEvent event) {

    }

    @Override
    public Map<String, Integer> GetDeviceCountersDictionary() {
        return null;
    }
}
