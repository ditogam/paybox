package com.pay.base;

/**
 * Created by dito on 4/18/2017.
 */
@FunctionalInterface
public interface Action<T> {
    void invoke(T arg);
}
