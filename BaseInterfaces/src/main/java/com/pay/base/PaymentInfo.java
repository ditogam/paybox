package com.pay.base;

/**
 * Created by dito on 4/5/2017.
 */
public class PaymentInfo
{
    private int ProductNavigationId;
    public final int getProductNavigationId()
    {
        return ProductNavigationId;
    }
    public final void setProductNavigationId(int value)
    {
        ProductNavigationId = value;
    }

    private int ProductProcessingId;
    public final int getProductProcessingId()
    {
        return ProductProcessingId;
    }
    public final void setProductProcessingId(int value)
    {
        ProductProcessingId = value;
    }

    private String ProductName;
    public final String getProductName()
    {
        return ProductName;
    }
    public final void setProductName(String value)
    {
        ProductName = value;
    }

    private int PaymentAmount;
    public final int getPaymentAmount()
    {
        return PaymentAmount;
    }
    public final void setPaymentAmount(int value)
    {
        PaymentAmount = value;
    }

    private String PaymentNumber;
    public final String getPaymentNumber()
    {
        return PaymentNumber;
    }
    public final void setPaymentNumber(String value)
    {
        PaymentNumber = value;
    }

    private String PaymentUniqId;
    public final String getPaymentUniqId()
    {
        return PaymentUniqId;
    }
    public final void setPaymentUniqId(String value)
    {
        PaymentUniqId = value;
    }

    private boolean QuitancePrinted;
    public final boolean getQuitancePrinted()
    {
        return QuitancePrinted;
    }
    public final void setQuitancePrinted(boolean value)
    {
        QuitancePrinted = value;
    }
}