package com.pay.base;

/**
 * Created by dito on 4/5/2017.
 */
public enum SoftState
{
    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [EnumMember] OK = 0,
    OK(0),
    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [EnumMember] ContentError = 1
    ContentError(1);

    public static final int SIZE = java.lang.Integer.SIZE;

    private int intValue;
    private static java.util.HashMap<Integer, SoftState> mappings;
    private static java.util.HashMap<Integer, SoftState> getMappings()
    {
        if (mappings == null)
        {
            synchronized (SoftState.class)
            {
                if (mappings == null)
                {
                    mappings = new java.util.HashMap<Integer, SoftState>();
                }
            }
        }
        return mappings;
    }

    private SoftState(int value)
    {
        intValue = value;
        getMappings().put(value, this);
    }

    public int getValue()
    {
        return intValue;
    }

    public static SoftState forValue(int value)
    {
        return getMappings().get(value);
    }
}
