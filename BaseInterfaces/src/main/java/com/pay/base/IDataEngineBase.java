package com.pay.base;

import com.pay.base.config.xml.XmlDocument;
import com.pay.base.enums.PayboxState;
import com.pay.helper.RefObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by dito on 4/5/2017.
 */
public interface IDataEngineBase {
    Statistics getStatistics();

    String getContentPath();

    Integer getRegionID();

    String getPayboxName();

    String getDefaultUILocale();

    ArrayList<String> getSupportedUiLocale();

    int getProcessingUsageCounter();

    long getPaymentCountInDay();

    Map<Integer, Map<Integer, Boolean>> getProductCashConfig();

    XmlDocument getExternalResourcesConfig();

    void SendMonitoringInfo(boolean guaranteedSend, MonitoringInfo monitoringInfo);

    void SendAlertInfo(boolean guaranteedSend, AlertInfo alertInfo, IServerContainer container);

    void SendDeviceCounters(HashMap<String, HashMap<String, Integer>> dictionary);

    void SendCollectionInfo(CollectionAddInfo info, HashMap<String, HashMap<String, Integer>> dictionary);

    void SendExtractedMoneyInfo(ArrayList<ExtractedMoneyInfo> infoList, String extractorId, int reconciliationIndicator, java.time.LocalDateTime extractTime);

    void GetPayboxSimInfo(RefObject<Integer> simProviderId, RefObject<String> simProviderName, RefObject<String> simNumber, RefObject<String> simSerial);

    void UpdatePayboxSimInfo(Integer simProviderId, String simNumber, String simSerial);

    HashMap<Integer, Integer> GetPayboxDealQuantities();

    boolean SaveFinancialData(XmlDocument accountsTree, DataTable moneyFlow);

    boolean LoadFinancialData(RefObject<XmlDocument> accountsTree, RefObject<DataTable> moneyFlow);

    boolean SendFinancialMoneyFlow(int reconcilationIndicator);

//C# TO JAVA CONVERTER TODO TASK: This event cannot be converted to Java:
//	event StateChangedEventHandler HaspStateChanged;

    ServerResponse QueryServer(ServerRequest request);

    boolean StartUpdateDownload(boolean needConfig, boolean needSoft, boolean needContent);

    void StartLogUpload(LogRequest[] requestedLogsDates);

    boolean SwitchState(PayboxState state);

    void SetConnectionAddresses(String processingAddress, String appServerAddress);

    void StartUploadConfigs();

    void RegisterMaintenanceAction(String actionGroup, String actionDescription, java.time.LocalDateTime actionTime, String payboxUser);

    int GetMaintenanceTransactions(java.time.LocalDateTime transactionDate, RefObject<UserPaymentHistoryRow[]> historyRows);

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#region Railway ticket maintenance print methods

    DataTable RailwayGetTransactions(String terminalName, int daysBefore);

    DataTable RailwayGetDocumentsIDs(int payDocID, int daysBefore);

    DataTable RailwayGetAddInfo(int docID);

    String RailwayGetInvoiceData(int docID, int daysBefore);

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#endregion

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#region Paybox user cabinet

    int PayboxCabinetGetUserInfo(String phoneIdentificator, RefObject<HashMap<String, String>> generalInfo, RefObject<UserSpecificProductPaymentInfo[]> userSpecificProductPaymentInfo, RefObject<HashMap<String, String[]>> myIDsInfo);

    int PayboxCabinetChangeMyIDs(int userID, String configName, String invoiceData, String invoiceDataOld, int operationType, String sessionID);

    int PayboxCabinetUsersGetTransactionsHistory(int userID, String sessionID, RefObject<UserPaymentHistoryRow[]> historyRows);

    int PayboxCabinetUsersGetBalance(int userID, String sessionID, RefObject<Integer> balance, RefObject<Integer> blockedAmount);

    int PayboxCabinetUsersGetPoints(int userID, String sessionID, RefObject<java.math.BigDecimal> points);

    int PayboxCabinetSMSServiceVerify(String sessionID);

    int PayboxCabinetSMSServiceProlog(String sessionID, int minute);

    int PayboxCabinetSMSServiceExpire(String sessionID);

    void PayboxCabinetPerformLogIn(int userId);
}
