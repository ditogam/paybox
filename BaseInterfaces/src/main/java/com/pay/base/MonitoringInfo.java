package com.pay.base;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by dito on 4/5/2017.
 */
public class MonitoringInfo implements Serializable {
    public MonitoringInfo(DeviceStateMonitoringWrapper deviceStateMonitoringWrapper) {
        setInfoCreateDateTime(new Date());
        setDeviceStateMonitoringWrapper(deviceStateMonitoringWrapper);
    }

    public MonitoringInfo() {
    }

    private Date InfoCreateDateTime = new Date(0);

    public final Date getInfoCreateDateTime() {
        return InfoCreateDateTime;
    }

    public final void setInfoCreateDateTime(Date value) {
        InfoCreateDateTime = value;
    }

    private DeviceStateMonitoringWrapper DeviceStateMonitoringWrapper;

    public final DeviceStateMonitoringWrapper getDeviceStateMonitoringWrapper() {
        return DeviceStateMonitoringWrapper;
    }

    public final void setDeviceStateMonitoringWrapper(DeviceStateMonitoringWrapper value) {
        DeviceStateMonitoringWrapper = value;
    }
}