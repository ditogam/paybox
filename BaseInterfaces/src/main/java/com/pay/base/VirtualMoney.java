package com.pay.base;

import com.pay.base.enums.Currency;
import com.pay.base.enums.MoneyType;

/**
 * Created by dito on 4/3/2017.
 */
public class VirtualMoney extends Money {
    private int amount;

    public VirtualMoney(Currency currency, MoneyType type, int amount) {
        super(currency, type);
        this.amount = amount;
    }

    public int getAmount() {
        return amount;
    }
}
