package com.pay.base;

public enum CardReaderState
{
	Connected,
	Disconnected,
	Uninitialized;

	public static final int SIZE = Integer.SIZE;

	public int getValue()
	{
		return this.ordinal();
	}

	public static CardReaderState forValue(int value)
	{
		return values()[value];
	}
}