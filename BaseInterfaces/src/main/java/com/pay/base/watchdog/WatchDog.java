package com.pay.base.watchdog;

import com.pay.base.IHardware;
import com.pay.base.IHardwareBase;
import com.pay.base.SoftwareActionToDevice;
import com.pay.base.devicestate.DeviceState;
import com.pay.base.devicestate.WatchDogDeviceState;
import com.pay.base.event.EventManager;
import com.pay.base.event.types.Delegates;
import com.pay.base.event.types.DeviceChangedEvent;

import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by dito on 4/3/2017.
 */
public abstract class WatchDog implements IHardwareBase
{
    protected boolean configIsValid = false;

    protected HashMap<ConnectorName, Connector> connectorsDictionary = new HashMap<ConnectorName, Connector>();
    //<Key: Connector Name, Value: Connector Object>

    protected WatchDogDeviceState currentWatchDogDeviceState;
    protected Object currentWatchDogDeviceStateLock = new Object();

    protected HashMap<Integer, Error> errorDictionary = new HashMap<Integer, Error>();
    //<Key: Device Specific Code, Value: Error Info>

    protected IHardware hardware;
    protected boolean initialized = false;
    protected AtomicInteger monitoringCycleCount=new AtomicInteger(0);
    protected volatile boolean monitorWatchDogState = true;
    protected String payboxDeviceTypeName = "";
    protected int pollTimerIntervalInSeconds;

    protected int stateMonitoringIntervalInMiliSeconds;

    protected String typeName = "";
    protected Thread watchDogStateMonitoringThread = null;

    protected final void WatchDogStateMonitoring() throws Exception
    {
        while (monitorWatchDogState)
        {
            synchronized (currentWatchDogDeviceStateLock)
            {
                WatchDogDeviceState stateArgs = (WatchDogDeviceState) GetStateForced();

                if (!stateArgs.equals(currentWatchDogDeviceState))
                {
                    OnStateChanged(null);

                    currentWatchDogDeviceState = stateArgs;
                    for (Connector connector : connectorsDictionary.values()) {
                        connector.Notified = true;

                        if (connector.Name.equals( ConnectorName.RFID))
                        {
                            connector.SignalValue = new byte[0];
                        }
                    }

                }
            }

            monitoringCycleCount.incrementAndGet();

            Thread.sleep(stateMonitoringIntervalInMiliSeconds);
        }
    }

    protected abstract DeviceState GetStateForced();

    public final int GetPollingInterval()
    {
        return pollTimerIntervalInSeconds;
    }

    public abstract Date GetDateTime();
    public abstract boolean SetDateTime(Date dateTime);

    //Tell To WatchDog That Program Is Runnging, If Not Poll In Time Interval, Wich Was Set During Initialization, WatchDog Will Restart Computer
    //In Real World Poll Must Call In Interval Wich Is Less Than Set Interval, And Set Interval Must Be Enough For Program To Call First Time
    public abstract boolean Poll();

    public abstract boolean PerformActionOnConnector(ConnectorName connectorName, Object parameter);

    ///#region Helper Classes

    public class Connector
    {
        public Connector(int id, ConnectorName name, SignalType signalType, SignalDirection signalDirection)
        {
            ID = id;
            Name = name;
            SignalType = signalType;
            SignalDirection = signalDirection;
            setState(ConnectorState.Unknown);
            setSignalValue(new byte[0]);
        }

        @Override
        public String toString()
        {
            StringBuilder signalValueStr = new StringBuilder("");

//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: foreach (var byteValue in SignalValue)
            for (byte byteValue : getSignalValue())
            {
                signalValueStr.append(byteValue);
                signalValueStr.append(",");
            }

            return String.format("ID:%1$s, Name:%2$s, SignalType:%3$s, SignalDirection:%4$s, State:%5$s, SignalValue:%6$s, Notified:%7$s", getID(), getName(), getSignalType(), getSignalDirection(), getState(), signalValueStr, getNotified());
        }

        public final Connector Copy()
        {
            Connector copy = new Connector(getID(), getName(), getSignalType(), getSignalDirection());
            copy.setState(getState());
            copy.setSignalValue(getSignalValue());
            copy.setNotified(getNotified());

            return copy;
        }

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
        ///#region Static Properties

        private int ID;
        public final int getID()
        {
            return ID;
        }
        private ConnectorName Name = ConnectorName.values()[0];
        public final ConnectorName getName()
        {
            return Name;
        }
        private SignalType SignalType = getSignalType().values()[0];
        public final SignalType getSignalType()
        {
            return SignalType;
        }
        private SignalDirection SignalDirection = getSignalDirection().values()[0];
        public final SignalDirection getSignalDirection()
        {
            return SignalDirection;
        }

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
        ///#endregion

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
        ///#region Dinamic Properties

        private ConnectorState State = ConnectorState.values()[0];
        public final ConnectorState getState()
        {
            return State;
        }
        public final void setState(ConnectorState value)
        {
            State = value;
        }
        //C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: private byte[] SignalValue;
        private byte[] SignalValue;
        //C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: public byte[] getSignalValue()
        public final byte[] getSignalValue()
        {
            return SignalValue;
        }
        //C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: public void setSignalValue(byte[] value)
        public final void setSignalValue(byte[] value)
        {
            SignalValue = value;
        }
        private boolean Notified;
        public final boolean getNotified()
        {
            return Notified;
        }
        public final void setNotified(boolean value)
        {
            Notified = value;
        }
        //If False Than Must Rise State Changed Event; For Pulse state, because we don't know state was changed or not

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
        ///#endregion
    }

    public enum ConnectorName
    {
        Case,
        Cassette,
        Door,
        Other,
        ComputerReset,
        ComputerShutdown,
        ModemReset,
        ModemShutdown,
        WatchDogReset,
        PowerAdapterReset,
        PowerAdapterOn,
        PowerAdapterOff,
        RFID;

        public static final int SIZE = java.lang.Integer.SIZE;

        public int getValue()
        {
            return this.ordinal();
        }

        public static ConnectorName forValue(int value)
        {
            return values()[value];
        }
    }

    public enum SignalType
    {
        Analog,
        Digital;

        public static final int SIZE = java.lang.Integer.SIZE;

        public int getValue()
        {
            return this.ordinal();
        }

        public static SignalType forValue(int value)
        {
            return values()[value];
        }
    }

    public enum SignalDirection
    {
        Input,
        Output,
        Both;

        public static final int SIZE = java.lang.Integer.SIZE;

        public int getValue()
        {
            return this.ordinal();
        }

        public static SignalDirection forValue(int value)
        {
            return values()[value];
        }
    }

    public enum ConnectorState
    {
        Unknown,
        Open,
        Close,
        Pulse;

        public static final int SIZE = java.lang.Integer.SIZE;

        public int getValue()
        {
            return this.ordinal();
        }

        public static ConnectorState forValue(int value)
        {
            return values()[value];
        }
    }

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#endregion

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#region IHardwareBase Members

    public abstract boolean Initialize();

    public abstract boolean Deinitialize();

    public abstract boolean Reset();

    public final DeviceState GetState(boolean force)
    {
        if (force)
        {
            int countTemp = monitoringCycleCount.get();

            while (monitoringCycleCount.get() == countTemp && watchDogStateMonitoringThread != null && watchDogStateMonitoringThread.isAlive())
            {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        synchronized (currentWatchDogDeviceStateLock)
        {
            return currentWatchDogDeviceState;
        }
    }

    private EventManager<DeviceState> stateChangedEventHandler = new EventManager<>();

    public final void OnStateChanged(DeviceChangedEvent event)
    {
        if (stateChangedEventHandler != null)
        {
            stateChangedEventHandler.invoke(event);
        }
    }

    public final void SoftwareActionToDeviceHandler(Object sender, SoftwareActionToDevice actionArgs)
    {
    }

    public final HashMap<String, Integer> GetDeviceCountersDictionary()
    {
        return null;
    }

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#endregion
}
