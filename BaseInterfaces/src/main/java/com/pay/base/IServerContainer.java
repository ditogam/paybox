package com.pay.base;

import com.pay.*;
import com.pay.base.advertisement.IAdvertisementEngine;
import com.pay.base.dataengine.IDataEngineConfigurator;
import com.pay.base.dataengine.IDataEngineLogger;
import com.pay.base.dataengine.IDataEnginePaymentProcessing;

import javafx.scene.web.WebView;

/**
 * Created by dito on 4/4/2017.
 */
public interface IServerContainer {
    IMonitoringObject getMonitoringObject();
    void setMonitoringObject(com.pay.IMonitoringObject value);
    ITemplateModule getCurrentTemplateModule();
    void setCurrentTemplateModule(ITemplateModule value);
    IGeneralLogicModule getGeneralModule();
    void setGeneralModule(IGeneralLogicModule value);
    WebView getBrowser();
    void setBrowser(WebView value);
    IScriptEntryPoint getScriptEntry();
    IDataEngineLogger getLogger();
    IDataEngineConfigurator getConfigurator();
    IDataEnginePaymentProcessing getPaymentPump();
    IHardware getHardwareAbstractionLayer();
    void setHardwareAbstractionLayer(IHardware value);
    IDataEngineBase getGeneralDataEngine();
    HtmlDocument getWebPageDocument();
    IFinancialModule getFinancialModule();
    IAdvertisementEngine getAdvertisement();
    void setAdvertisement(IAdvertisementEngine value);
    IInfoCollection getInfoCollection();
    void Initialize();
    void Deinitialize();
    void ExecuteScriptMethod(String methodName, Object... scriptArgs);
    void NavigateBrowser(String urlString);
    int GetMainThreadManagedId();



}
