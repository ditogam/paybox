package com.pay.base.enums;

/**
 * Created by dito on 3/31/2017.
 */
public enum SoftwareActionType
{
    CollectionHappend,
    CassetteSet,
    CassetteGet,
    Reset,
    PrinterPaperReset
}