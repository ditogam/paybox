package com.pay.base.enums;

/**
 * Created by dito on 4/4/2017.
 */
public enum LogicalModuleSolution
{
    ProgramRestart,
    ComputerReset,
    ComputerShutdown,
    ModemReset,
    ModemShutdown,
    WatchDogReset,
    PowerAdapterOn,
    PowerAdapterOff,
    PowerAdapterReset,
    GoOutOfServiceAndReport,
    NetworkRecconect,
    BanknoteReceiverReset
}