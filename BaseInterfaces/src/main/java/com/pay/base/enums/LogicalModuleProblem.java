package com.pay.base.enums;

/**
 * Created by dito on 4/4/2017.
 */
public enum LogicalModuleProblem
{
    ChangeUpdate,
    ExitFromOfflineMode,
    DeviceJammed,
    ThreadHang,
    NetworkConnectionHang,
    BanknoteReceiverHang,
    Other
}
