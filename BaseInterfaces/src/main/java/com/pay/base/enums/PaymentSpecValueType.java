package com.pay.base.enums;

/**
 * Created by dito on 4/4/2017.
 */
public enum PaymentSpecValueType
{
    FixedNumber,
    Percentage;

    public static final int SIZE = java.lang.Integer.SIZE;

    public int getValue()
    {
        return this.ordinal();
    }

    public static PaymentSpecValueType forValue(int value)
    {
        return values()[value];
    }
}