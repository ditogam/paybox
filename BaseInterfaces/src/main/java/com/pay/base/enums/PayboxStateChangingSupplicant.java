package com.pay.base.enums;

/**
 * Created by dito on 4/4/2017.
 */
public enum  PayboxStateChangingSupplicant {
    Administration, //Can Set States: OperationalMode, OfflineMode
    MaintenanceUser, //Can Set States: MaintenanceMode, Default
    ProgramLogic //Can Set States: OperationalMode, OutOfServiceMode, VisibleProcessMode, OfflineMode, Default
}
