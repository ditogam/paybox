package com.pay.base.enums;

/**
 * Created by dito on 4/3/2017.
 */
public enum StateCategory
{
    OK,
    Problem,
    Warning
}