package com.pay.base.enums;

/**
 * Created by dito on 4/5/2017.
 */
public enum AlertType
{
    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [EnumMember] CashInCassette = 0,
    CashInCassette(0),
    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [EnumMember] WDDoor = 1,
    WDDoor(1),
    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [EnumMember] WDCassette = 2,
    WDCassette(2),
    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [EnumMember] WDCase = 3
    WDCase(3);

    public static final int SIZE = java.lang.Integer.SIZE;

    private int intValue;
    private static java.util.HashMap<Integer, AlertType> mappings;
    private static java.util.HashMap<Integer, AlertType> getMappings()
    {
        if (mappings == null)
        {
            synchronized (AlertType.class)
            {
                if (mappings == null)
                {
                    mappings = new java.util.HashMap<Integer, AlertType>();
                }
            }
        }
        return mappings;
    }

    private AlertType(int value)
    {
        intValue = value;
        getMappings().put(value, this);
    }

    public int getValue()
    {
        return intValue;
    }

    public static AlertType forValue(int value)
    {
        return getMappings().get(value);
    }
}