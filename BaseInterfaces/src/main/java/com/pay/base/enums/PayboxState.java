package com.pay.base.enums;

/**
 * Created by dito on 4/4/2017.
 */
public enum PayboxState {

    OperationalMode(0),
    OutOfServiceMode(1),
    MaintenanceMode(2),
    VisibleProcessMode(3), //On payment page
    OfflineMode(4), //Terminates all processes, except neccessary (example: work with payment file...)
    Default(5),
    //When current mode is MaintenanceMode or VisibleProcessMode and changes happened, we must postpone this changes and after MaintenanceMode or VisibleProcessMode call with Default parameter to change to PostponedMode
    ProgramRestartMode(6), //Sets server for restart program
    ComputerRestartMode(7); //Sets server for restart computer

    public static final int SIZE = java.lang.Integer.SIZE;

    private int intValue;
    private static java.util.HashMap<Integer, PayboxState> mappings;
    private static java.util.HashMap<Integer, PayboxState> getMappings()
    {
        if (mappings == null)
        {
            synchronized (PayboxState.class)
            {
                if (mappings == null)
                {
                    mappings = new java.util.HashMap<Integer, PayboxState>();
                }
            }
        }
        return mappings;
    }

    private PayboxState(int value)
    {
        intValue = value;
        getMappings().put(value, this);
    }

    public int getValue()
    {
        return intValue;
    }

    public static PayboxState forValue(int value)
    {
        return getMappings().get(value);
    }
}
