package com.pay.base.enums;

/**
 * Created by dito on 4/4/2017.
 */
public enum UserPaymentType
{
    Undefined,
    ByCash,
    ByBalance,
    ByCard,
    ByNFC;

    public static final int SIZE = java.lang.Integer.SIZE;

    public int getValue()
    {
        return this.ordinal();
    }

    public static UserPaymentType forValue(int value)
    {
        return values()[value];
    }
}