package com.pay.base.enums;

/**
 * Created by dito on 3/31/2017.
 */
public enum Currency {
    GEL,
    USD,
    TMT,
    EUR
}
