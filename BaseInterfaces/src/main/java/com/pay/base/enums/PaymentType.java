package com.pay.base.enums;

/**
 * Created by dito on 4/3/2017.
 */
public enum PaymentType {
    CASH,
    CARD,
    NFC
}
