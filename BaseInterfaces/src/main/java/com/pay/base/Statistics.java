package com.pay.base;

import java.util.HashMap;

/**
 * Created by dito on 4/5/2017.
 */
public class Statistics
{
    public static final Object AmindiLock = new Object();

    public Statistics()
    {
        setAmindiStats(new HashMap<String, Integer>());
    }

    private HashMap<String, Integer> AmindiStats;
    public final HashMap<String, Integer> getAmindiStats()
    {
        return AmindiStats;
    }
    public final void setAmindiStats(HashMap<String, Integer> value)
    {
        AmindiStats = value;
    }
}