package com.pay.base;

import com.pay.base.enums.PayboxState;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by dito on 4/5/2017.
 */
public class ServerResponse implements Serializable
{
    public ServerResponse(TIME_ZONE_INFORMATION serverTimeZone, SYSTEMTIME serverTime, PayboxState payboxGlobalState, boolean needContentUpdate, boolean needSoftUpdate, boolean needConfigUpdate, LogRequest[] requestedLogs, long payboxPaymentLimit, Long payboxPaymentCountInDay, int[] paymentCountCoefficientsHourly, boolean needConfigUpload, boolean needBalanceCheckBeforeVerify)
    {
        setServerTimeZone(serverTimeZone.clone());
        setServerTime(serverTime.clone());
        setPayboxGlobalState(payboxGlobalState);
        setNeedConfigUpdate(needConfigUpdate);
        setNeedContentUpdate(needContentUpdate);
        setNeedSoftUpdate(needSoftUpdate);
        setRequestedLogs(requestedLogs);
        setPayboxPaymentLimit(payboxPaymentLimit);
        setPayboxPaymentCountInDay(payboxPaymentCountInDay);
        setPaymentCountCoefficientsHourly(paymentCountCoefficientsHourly);
        setNeedConfigUpload(needConfigUpload);
        setNeedBalanceCheckBeforeVerify(needBalanceCheckBeforeVerify);
    }

    public ServerResponse()
    {
    }

    private TIME_ZONE_INFORMATION ServerTimeZone = new TIME_ZONE_INFORMATION();
    public final TIME_ZONE_INFORMATION getServerTimeZone()
    {
        return ServerTimeZone;
    }
    public final void setServerTimeZone(TIME_ZONE_INFORMATION value)
    {
        ServerTimeZone = value.clone();
    }

    private SYSTEMTIME ServerTime = new SYSTEMTIME();
    public final SYSTEMTIME getServerTime()
    {
        return ServerTime;
    }
    public final void setServerTime(SYSTEMTIME value)
    {
        ServerTime = value.clone();
    }

    private PayboxState PayboxGlobalState = PayboxState.values()[0];
    public final PayboxState getPayboxGlobalState()
    {
        return PayboxGlobalState;
    }
    public final void setPayboxGlobalState(PayboxState value)
    {
        PayboxGlobalState = value;
    }

    private boolean NeedContentUpdate;
    public final boolean getNeedContentUpdate()
    {
        return NeedContentUpdate;
    }
    public final void setNeedContentUpdate(boolean value)
    {
        NeedContentUpdate = value;
    }

    private int CurrentContentId;
    public final int getCurrentContentId()
    {
        return CurrentContentId;
    }
    public final void setCurrentContentId(int value)
    {
        CurrentContentId = value;
    }

    private boolean NeedConfigUpdate;
    public final boolean getNeedConfigUpdate()
    {
        return NeedConfigUpdate;
    }
    public final void setNeedConfigUpdate(boolean value)
    {
        NeedConfigUpdate = value;
    }

    private boolean NeedSoftUpdate;
    public final boolean getNeedSoftUpdate()
    {
        return NeedSoftUpdate;
    }
    public final void setNeedSoftUpdate(boolean value)
    {
        NeedSoftUpdate = value;
    }

    private LogRequest[] RequestedLogs;
    public final LogRequest[] getRequestedLogs()
    {
        return RequestedLogs;
    }
    public final void setRequestedLogs(LogRequest[] value)
    {
        RequestedLogs = value;
    }

    private long PayboxPaymentLimit;
    public final long getPayboxPaymentLimit()
    {
        return PayboxPaymentLimit;
    }
    public final void setPayboxPaymentLimit(long value)
    {
        PayboxPaymentLimit = value;
    }

    private Long PayboxPaymentCountInDay = null;
    public final Long getPayboxPaymentCountInDay()
    {
        return PayboxPaymentCountInDay;
    }
    public final void setPayboxPaymentCountInDay(Long value)
    {
        PayboxPaymentCountInDay = value;
    }

    private int[] PaymentCountCoefficientsHourly;
    public final int[] getPaymentCountCoefficientsHourly()
    {
        return PaymentCountCoefficientsHourly;
    }
    public final void setPaymentCountCoefficientsHourly(int[] value)
    {
        PaymentCountCoefficientsHourly = value;
    }

    private boolean NeedConfigUpload;
    public final boolean getNeedConfigUpload()
    {
        return NeedConfigUpload;
    }
    public final void setNeedConfigUpload(boolean value)
    {
        NeedConfigUpload = value;
    }

    private boolean NeedBalanceCheckBeforeVerify;
    public final boolean getNeedBalanceCheckBeforeVerify()
    {
        return NeedBalanceCheckBeforeVerify;
    }
    public final void setNeedBalanceCheckBeforeVerify(boolean value)
    {
        NeedBalanceCheckBeforeVerify = value;
    }

    private Map<Integer, Map<Integer, Boolean>> ProductCashConfig;
    public final Map<Integer, Map<Integer, Boolean>> getProductCashConfig()
    {
        return ProductCashConfig;
    }
    public final void setProductCashConfig(Map<Integer, Map<Integer, Boolean>> value)
    {
        ProductCashConfig = value;
    }

    private Map<String, String[]> MaintenanceUsers;
    public final Map<String, String[]> getMaintenanceUsers()
    {
        return MaintenanceUsers;
    }
    public final void setMaintenanceUsers(Map<String, String[]> value)
    {
        MaintenanceUsers = value;
    }

    private Map<String, Object> NamedParameters;
    public final Map<String, Object> getNamedParameters()
    {
        return NamedParameters;
    }
    public final void setNamedParameters(Map<String, Object> value)
    {
        NamedParameters = value;
    }

    @Override
    public String toString()
    {
        return String.format("Response: Config:%1$s, Soft:%2$s, Content:%3$s, PayboxGlobalState:%4$s, CurrentContentId:%5$s, PayboxPaymentLimit:%6$s, PaymentCountInDay:%7$s, PaymentCountCoefficientsHourly:%8$s, NeedConfigUpload:%9$s, NeedBalanceCheckBeforeVerify:%10$s", getNeedConfigUpdate(), getNeedSoftUpdate(), getNeedContentUpdate(), getPayboxGlobalState(), getCurrentContentId(), getPayboxPaymentLimit(), getPayboxPaymentCountInDay() == null ? "" : getPayboxPaymentCountInDay().toString(), getPaymentCountCoefficientsHourly() == null ? "" : getPaymentCountCoefficientsHourly().toString(), getNeedConfigUpload(), getNeedBalanceCheckBeforeVerify());
    }
}