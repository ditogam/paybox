package com.pay.base;

import com.pay.base.enums.Currency;
import com.pay.base.enums.MoneyType;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dito on 4/5/2017.
 */
public interface IFinancialModule
{
//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#region Base

    IServerContainer getContainer();

    HashMap<Integer, CashMoney> getCashMoneyDictionary();

    String GetCassetteNumber(MoneyType moneyType, Currency currency);

    void SetCassetteNumber(String cassetteNumber, MoneyType moneyType, Currency currency);

    int GetAccountBalanceByAccountID(String accountID);

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#endregion

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#region Money Flow Functions

    void DeviceReceivedMoney(Money money, String transactionID, String userNumber);
    //Device Received Money From User (Deivce +, User -)

    void PutMoneyInCassette(Money money, String transactionID);
    //Cassette Received Money From Device (Cassette +, Device -)

    void CreateTransaction(Currency currency, int amount, String transactionID);
    //Create Transaction For User (User +, Transaction -)

    void StartCollection(Currency currency, MoneyType moneyType, String collectionerID, String transactionID);
    //Collectioner Made Collection (Collectioner +, Cassette -)

    void EndCollection(int reconcilationIndicator);
    //Start Sending Money Flow History Then Clearing Account Tree And MoneyFlow DataTable

    void ExtractFailedMoney(ArrayList<Integer> cashIDList, String extractorID); //Find Failed Money

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#endregion
}