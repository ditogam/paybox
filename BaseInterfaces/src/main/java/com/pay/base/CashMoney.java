package com.pay.base;

import com.pay.base.enums.Currency;
import com.pay.base.enums.MoneyType;

/**
 * Created by dito on 3/31/2017.
 */
public class CashMoney extends Money {
    private int id;
    private String name;
    private int nominal;

    public CashMoney(Currency currency, MoneyType type, int id, String name, int nominal) {
        super(currency, type);
        this.id = id;
        this.name = name;
        this.nominal = nominal;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getNominal() {
        return nominal;
    }
}
