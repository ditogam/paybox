package com.pay.base;

import java.io.Serializable;

/**
 * Created by dito on 4/5/2017.
 */
public class LastReceivedMoneyTypesInfo implements Serializable {
    public LastReceivedMoneyTypesInfo(CashMoney banknote, CashMoney coin, VirtualMoney card, VirtualMoney novaAccount) {
        setBanknote(banknote);
        setCoin(coin);
        setCard(card);
        setNovaAccount(novaAccount);
    }

    private CashMoney Banknote;

    public final CashMoney getBanknote() {
        return Banknote;
    }

    public final void setBanknote(CashMoney value) {
        Banknote = value;
    }

    private CashMoney Coin;

    public final CashMoney getCoin() {
        return Coin;
    }

    public final void setCoin(CashMoney value) {
        Coin = value;
    }

    private VirtualMoney Card;

    public final VirtualMoney getCard() {
        return Card;
    }

    public final void setCard(VirtualMoney value) {
        Card = value;
    }

    private VirtualMoney NovaAccount;

    public final VirtualMoney getNovaAccount() {
        return NovaAccount;
    }

    public final void setNovaAccount(VirtualMoney value) {
        NovaAccount = value;
    }
}