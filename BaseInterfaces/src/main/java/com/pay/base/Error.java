package com.pay.base;

import com.pay.base.enums.StateCategory;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Created by dito on 3/31/2017.
 */
public class Error {
    private int code;
    private String description;
    private int generalCode;
    private String generalDescription;
    private StateCategory category;
    private boolean manualRecovery;


    public Error(int code, String description, int generalCode, String generalDescription, StateCategory category, boolean manualRecovery) {
        this.code = code;
        this.description = description;
        this.generalCode = generalCode;
        this.generalDescription = generalDescription;
        this.category = category;
        this.manualRecovery = manualRecovery;
    }

    public Error(int code, String description, int generalCode, String generalDescription, StateCategory category) {
        this(code, description, generalCode, generalDescription, category, false);
    }


    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public int getGeneralCode() {
        return generalCode;
    }

    public String getGeneralDescription() {
        return generalDescription;
    }

    public StateCategory getCategory() {
        return category;
    }

    public boolean isManualRecovery() {
        return manualRecovery;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Error error = (Error) o;

        return new EqualsBuilder()
                .append(code, error.code)
                .append(generalCode, error.generalCode)
                .append(manualRecovery, error.manualRecovery)
                .append(description, error.description)
                .append(generalDescription, error.generalDescription)
                .append(category, error.category)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(code)
                .append(description)
                .append(generalCode)
                .append(generalDescription)
                .append(category)
                .append(manualRecovery)
                .toHashCode();
    }
}
