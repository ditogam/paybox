package com.pay.base;

import com.pay.base.enums.AlertType;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by dito on 4/5/2017.
 */
@Data
public class AlertInfo implements Serializable {
    public AlertInfo(HashMap<AlertType, Boolean> alerts) {
        setInfoCreateDateTime(new Date());
        setAlerts(alerts);
        setReason(AlertReason.Unknown);
    }

    public AlertInfo() {
    }

    private Date infoCreateDateTime = new Date(0);


    private Map<AlertType, Boolean> Alerts;


    private AlertReason reason = AlertReason.values()[0];


    private String additionalInfo;

}