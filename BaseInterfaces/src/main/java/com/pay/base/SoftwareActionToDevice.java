package com.pay.base;

import com.pay.base.enums.SoftwareActionType;

/**
 * Created by dito on 3/31/2017.
 */
public class SoftwareActionToDevice {
    private String payboxDeviceTypeName = "";

    private SoftwareActionType softwareAction;

    public String getPayboxDeviceTypeName() {
        return payboxDeviceTypeName;
    }

    public SoftwareActionType getSoftwareAction() {
        return softwareAction;
    }

    public SoftwareActionToDevice(String payboxDeviceTypeName, SoftwareActionType softwareAction) {
        this.payboxDeviceTypeName = payboxDeviceTypeName;
        this.softwareAction = softwareAction;
    }


}
