package com.pay.base;

import com.pay.base.devicestate.DeviceState;
import com.pay.base.event.EventListener;
import com.pay.base.event.types.DeviceChangedEvent;
import com.pay.base.event.types.SoftwareActionToDeviceEvent;

import java.util.Map;

/**
 * Created by dito on 3/31/2017.
 */
public interface IHardwareBase {
    boolean Initialize();
    boolean Deinitialize();
    boolean Reset();

    //Get Device State
    //Force : Forcing Device to get current state
    DeviceState GetState(boolean force);


    void addStateChangedHandler(EventListener<DeviceChangedEvent> listener);

    void removeStateChangedHandler(EventListener<DeviceChangedEvent> listener);


    //    event StateChangedEventHandler StateChanged;
    //StateChanged should be arise StateChangedEvent
    void OnStateChanged(DeviceChangedEvent event);

    void SoftwareActionToDeviceHandler(SoftwareActionToDeviceEvent event);

    Map<String, Integer> GetDeviceCountersDictionary();
}
