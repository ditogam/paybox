package com.pay.base;

import lombok.Data;

/**
 * Created by dito on 4/5/2017.
 */
@Data
public final class CollectionAddInfo {
    private String ejectedBanknoteCassetteId;

    private String insertedBanknoteCassetteId;

    private String ejectedCoinCassetteId;

    private String insertedCoinCassetteId;

    private String personnelId;
    private String financialTransactionId;

    private int reconciliationIndicator;
}
