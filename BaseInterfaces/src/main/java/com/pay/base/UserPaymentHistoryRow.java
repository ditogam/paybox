package com.pay.base;

/**
 * Created by dito on 4/4/2017.
 */
public class UserPaymentHistoryRow
{
    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [DataMember] public DateTime PayTime {get;set;}
    private java.time.LocalDateTime PayTime = java.time.LocalDateTime.MIN;
    public final java.time.LocalDateTime getPayTime()
    {
        return PayTime;
    }
    public final void setPayTime(java.time.LocalDateTime value)
    {
        PayTime = value;
    }

    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [DataMember] public int Status {get;set;}
    private int Status;
    public final int getStatus()
    {
        return Status;
    }
    public final void setStatus(int value)
    {
        Status = value;
    }

    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [DataMember] public int Product {get;set;}
    private int Product;
    public final int getProduct()
    {
        return Product;
    }
    public final void setProduct(int value)
    {
        Product = value;
    }

    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [DataMember] public string UniqID {get;set;}
    private String UniqID;
    public final String getUniqID()
    {
        return UniqID;
    }
    public final void setUniqID(String value)
    {
        UniqID = value;
    }

    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [DataMember] public string InvoiceData {get;set;}
    private String InvoiceData;
    public final String getInvoiceData()
    {
        return InvoiceData;
    }
    public final void setInvoiceData(String value)
    {
        InvoiceData = value;
    }

    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [DataMember] public decimal Nominal {get;set;}
    private java.math.BigDecimal Nominal = new java.math.BigDecimal(0);
    public final java.math.BigDecimal getNominal()
    {
        return Nominal;
    }
    public final void setNominal(java.math.BigDecimal value)
    {
        Nominal = value;
    }

    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [DataMember] public decimal Comission {get;set;}
    private java.math.BigDecimal Comission = new java.math.BigDecimal(0);
    public final java.math.BigDecimal getComission()
    {
        return Comission;
    }
    public final void setComission(java.math.BigDecimal value)
    {
        Comission = value;
    }

    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [DataMember] public decimal ClientPrice {get;set;}
    private java.math.BigDecimal ClientPrice = new java.math.BigDecimal(0);
    public final java.math.BigDecimal getClientPrice()
    {
        return ClientPrice;
    }
    public final void setClientPrice(java.math.BigDecimal value)
    {
        ClientPrice = value;
    }
}