package com.pay.base;

import com.pay.base.devicestate.DeviceState;
import com.pay.base.enums.*;

import java.util.HashMap;

/**
 * Created by dito on 4/4/2017.
 */
public interface IMonitoringObject
{
    IServerContainer getContainer();
    void setContainer(IServerContainer value);
    HashMap<String, DeviceState> getLastKnownDeviceStates();
    Object getLockLastKnownDeviceStates();
    PayboxState getPayboxGlobalState();
    void setPayboxGlobalState(PayboxState value);
    int getCurrentMaintenanceUserId();
    void setCurrentMaintenanceUserId(int value);
    Object getMaintenanceAlertLockObj();
    boolean getDebugMode();
    void Initialize();
    void Deinitialize();
    void ReportDeviceProblem(DeviceState deviceState);
    boolean HasPostponedPayboxGlobalState();

    boolean RequestChangePayboxGlobalState(PayboxState payboxGlobalState, PayboxStateChangingSupplicant supplicant, boolean switchMode);

    boolean HelpLogicalModule(LogicalModule logicalModule, LogicalModuleProblem logicalModuleProblem, LogicalModuleSolution logicalModuleSolution);

    void HelpLogicalModuleAsync(LogicalModule logicalModule, LogicalModuleProblem logicalModuleProblem, LogicalModuleSolution logicalModuleSolution);

    void ExitApplication();
}