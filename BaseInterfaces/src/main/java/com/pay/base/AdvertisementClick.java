package com.pay.base;

/**
 * Created by dito on 4/5/2017.
 */
public class AdvertisementClick
{
    private int AdvLinkId;
    public final int getAdvLinkId()
    {
        return AdvLinkId;
    }
    public final void setAdvLinkId(int value)
    {
        AdvLinkId = value;
    }

    private String AdvName;
    public final String getAdvName()
    {
        return AdvName;
    }
    public final void setAdvName(String value)
    {
        AdvName = value;
    }

    private String AdvType;
    public final String getAdvType()
    {
        return AdvType;
    }
    public final void setAdvType(String value)
    {
        AdvType = value;
    }
}