package com.pay.base;

/**
 * Created by dito on 4/5/2017.
 */
public final class ExtractedMoneyInfo {
    public ExtractedMoneyInfo(int cashID, int state, int location, int count) {
        setCashID(cashID);
        setState(state);
        setLocation(location);
        setCount(count);
    }

    private int CashID;

    public int getCashID() {
        return CashID;
    }

    private void setCashID(int value) {
        CashID = value;
    }

    private int State;

    public int getState() {
        return State;
    }

    private void setState(int value) {
        State = value;
    }

    private int Location;

    public int getLocation() {
        return Location;
    }

    private void setLocation(int value) {
        Location = value;
    }

    private int Count;

    public int getCount() {
        return Count;
    }

    private void setCount(int value) {
        Count = value;
    }
}