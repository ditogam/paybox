/*
 * Copyright (c) 2017, GoMint, BlackyPaw and geNAZt
 *
 * This code is licensed under the BSD license found in the
 * LICENSE file in the root directory of this source tree.
 */

package com.pay.base.event;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This list sorts and triggers all EventHandlerMethods which have been registered for a event.
 *
 * @author BlackyPaw
 * @version 1.0
 */
public class EventHandlerList {


    private List<EventListener> sortedHandlerList = new ArrayList<>();
    private Map<Integer, EventListener> referenceMap = new HashMap<>();

    /**
     * Construct a new EventHandlerList
     */
    public EventHandlerList() {

    }

    /**
     * Add a new handler. This marks the whole list dirty and it gets sorted when the next event arrives.
     *
     * @param handler The handler which should be added
     */
    public void addHandler(EventListener handler) {
        int hash = handler.hashCode();

        referenceMap.put(hash, handler);
        this.sortedHandlerList.add(handler);
    }

    /**
     * Remove a handler from the list. This does not dirty the list.
     */
    public void removeHandler(EventListener handler) {
        int hash = handler.hashCode();
        if (referenceMap.containsKey(hash))
            this.sortedHandlerList.remove(referenceMap.get(hash));
    }

    /**
     * Iterate over all EventHandler Methods and sort them if needed. This also controls when a Event got cancelled
     * that it does not get fired for Handlers which does not want it.
     *
     * @param event The event which gets passed to all handlers
     */
    public void triggerEvent(Event event) {
        if (event instanceof CancellableEvent) {
            CancellableEvent cancelableEvent = (CancellableEvent) event;
            for (EventListener handler : this.sortedHandlerList) {
                if (cancelableEvent.isCancelled()) {
                    continue;
                }

                handler.invoke(event);
            }
        } else {
            for (EventListener handler : this.sortedHandlerList) {
                handler.invoke(event);
            }
        }
    }

}
