package com.pay.base.event.types;

import com.pay.base.event.Event;

/**
 * Created by dito on 4/12/2017.
 */
public class CardGivenEvent extends Event {
    public CardGivenEvent(Object sender) {
        super(sender);
    }
}
