package com.pay.base.event.types;

import com.pay.base.CashMoney;
import com.pay.base.devicestate.DeviceState;
import com.pay.base.event.Event;

/**
 * Created by dito on 4/12/2017.
 */
public class CashReceivedEvent extends Event<CashMoney> {
    public CashReceivedEvent(Object sender, CashMoney value) {
        super(sender, value);
    }
}
