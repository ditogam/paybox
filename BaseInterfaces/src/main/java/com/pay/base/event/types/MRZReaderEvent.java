package com.pay.base.event.types;

import com.pay.base.MRZPerson;
import com.pay.base.SoftwareActionToDevice;
import com.pay.base.event.Event;

/**
 * Created by dito on 4/12/2017.
 */
public class MRZReaderEvent extends Event<MRZPerson> {
    public MRZReaderEvent(Object sender, MRZPerson value) {
        super(sender, value);
    }
}
