/*
 * Copyright (c) 2017, GoMint, BlackyPaw and geNAZt
 *
 * This code is licensed under the BSD license found in the
 * LICENSE file in the root directory of this source tree.
 */

package com.pay.base.event;

import lombok.ToString;

/**
 * @author geNAZt
 * @version 1.0
 */
@ToString(callSuper = true)
public class Event<T> {
    protected Object sender;
    protected T value;

    public Event(Object sender, T value) {
        this.sender = sender;
        this.value = value;
    }

    public Event(Object sender) {
        this(sender, null);
    }

    public Object getSender() {
        return sender;
    }

    public T getValue() {
        return value;
    }
}
