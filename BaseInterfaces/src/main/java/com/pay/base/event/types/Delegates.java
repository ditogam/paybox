package com.pay.base.event.types;

import com.pay.base.CashMoney;
import com.pay.base.MRZPerson;
import com.pay.base.SigCaptureEventArgs;
import com.pay.base.SoftwareActionToDevice;
import com.pay.base.devicestate.DeviceState;
import com.pay.base.event.EventManager;
import lombok.experimental.UtilityClass;

/**
 * Created by dito on 4/12/2017.
 */
public class Delegates {
    private EventManager<DeviceState> stateChangedEventHandler = new EventManager<>();
    private EventManager<CashMoney> cashReceivedEventHandler = new EventManager<>();
    private EventManager<SoftwareActionToDevice> softwareActionToDeviceEventHanler = new EventManager<>();
    private EventManager<MRZPerson> mRZReaderEventHandler = new EventManager<>();
    private EventManager cardMovedCaptureBoxEventHandler = new EventManager<>();
    private EventManager cardGivenEventHandler = new EventManager<>();
    private EventManager cardInsertedEventHandler = new EventManager<>();
    private EventManager cardRemovedEventHandler = new EventManager<>();
    private EventManager<SigCaptureEventArgs> signedEventHandler = new EventManager<>();

    private Delegates() {

    }

    private static Delegates instance = null;
    private static Object lock = new Object();

    public static Delegates getInstance() {
        if (instance == null) {
            synchronized (lock) {
                instance = new Delegates();
            }
        }
        return instance;
    }

    public static void main(String[] args) {
        Delegates.getInstance();
    }

    public EventManager<DeviceState> getStateChangedEventHandler() {
        return stateChangedEventHandler;
    }

    public EventManager<CashMoney> getCashReceivedEventHandler() {
        return cashReceivedEventHandler;
    }

    public EventManager<SoftwareActionToDevice> getSoftwareActionToDeviceEventHanler() {
        return softwareActionToDeviceEventHanler;
    }

    public EventManager<MRZPerson> getmRZReaderEventHandler() {
        return mRZReaderEventHandler;
    }

    public EventManager getCardMovedCaptureBoxEventHandler() {
        return cardMovedCaptureBoxEventHandler;
    }

    public EventManager getCardGivenEventHandler() {
        return cardGivenEventHandler;
    }

    public EventManager getCardInsertedEventHandler() {
        return cardInsertedEventHandler;
    }

    public EventManager getCardRemovedEventHandler() {
        return cardRemovedEventHandler;
    }

    public EventManager<SigCaptureEventArgs> getSignedEventHandler() {
        return signedEventHandler;
    }


}
