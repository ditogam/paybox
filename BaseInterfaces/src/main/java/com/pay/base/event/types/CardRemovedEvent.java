package com.pay.base.event.types;

import com.pay.base.event.Event;

/**
 * Created by dito on 4/12/2017.
 */
public class CardRemovedEvent extends Event {
    public CardRemovedEvent(Object sender) {
        super(sender);
    }
}
