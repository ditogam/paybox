/*
 * Copyright (c) 2017, GoMint, BlackyPaw and geNAZt
 *
 * This code is licensed under the BSD license found in the
 * LICENSE file in the root directory of this source tree.
 */

package com.pay.base.event;

/**
 * @author geNAZt
 * @version 1.0
 */

@FunctionalInterface
public interface EventListener<T extends Event> {
    public void invoke(T event);
}
