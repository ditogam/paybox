package com.pay.base.event.types;

import com.pay.base.SigCaptureEventArgs;
import com.pay.base.devicestate.DeviceState;
import com.pay.base.event.Event;

/**
 * Created by dito on 4/12/2017.
 */
public class SignedEvent extends Event<SigCaptureEventArgs> {
    public SignedEvent(Object sender, SigCaptureEventArgs value) {
        super(sender, value);
    }
}
