/*
 * Copyright (c) 2017, GoMint, BlackyPaw and geNAZt
 *
 * This code is licensed under the BSD license found in the
 * LICENSE file in the root directory of this source tree.
 */

package com.pay.base.event;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author BlackyPaw
 * @version 1.0
 */
public class EventManager<T> {

    // Fair-Mode reentrant R/W lock used for synchronizing access to the event handler map:
    private final ReentrantReadWriteLock collectionLock = new ReentrantReadWriteLock(true);
    private final ReentrantLock queueLock = new ReentrantLock(true);
    ExecutorService executorService = Executors.newSingleThreadExecutor();

    // All event handlers that have been registered
    private final EventHandlerList eventHandlers = new EventHandlerList();

    // Not actually a queue, but used for the sake of cache efficiency provided by array lists over
    // actual queue implementations such as LinkedList
    private final List<Event> eventQueue = new ArrayList<>();

    /**
     * Constructs a new EventManager.
     */
    public EventManager() {

    }

    /**
     * Queues the event. It will be triggered as soon as the event manager flushes its internal event queue.
     *
     * @param event The event to be enqueued
     */
    public void queueEvent(Event event) {
        this.queueLock.lock();
        try {
            this.eventQueue.add(event);
        } finally {
            this.queueLock.unlock();
        }
    }

    /**
     * Triggers the event. It will be dispatched to all interested listeners immediately.
     *
     * @param event The event to be triggered
     */
    public void beginInvoke(final Event<T> event) {
        executorService.execute(() -> invoke(event));

    }

    public void invoke(Event<T> event) {
        this.collectionLock.readLock().lock();
        try {
            this.triggerEvent0(event);
        } finally {
            this.collectionLock.readLock().unlock();
        }
    }


    /**
     * Flushes the internal event queue of the EventManager by triggering all enqueued events at once.
     */
    public void flush() {
        this.collectionLock.readLock().lock();
        this.queueLock.lock();
        try {
            for (Event event : this.eventQueue) {
                this.triggerEvent0(event);
            }

            this.eventQueue.clear();
        } finally {
            this.queueLock.unlock();
            this.collectionLock.writeLock().unlock();
        }
    }

    /**
     * Registers all event handler methods found on the specified listener.
     *
     * @param listener The listener to register
     * @param <T>      The generic type of the listener
     */
    public  <K extends Event<T>> void registerListener(EventListener<K> listener) {

        this.collectionLock.writeLock().lock();
        try {
            eventHandlers.addHandler(listener);
        } finally {
            this.collectionLock.writeLock().unlock();
        }
    }

    /**
     * Registers all event handler methods found on the specified listener.
     *
     * @param listener The listener to register
     * @param <T>      The generic type of the listener
     */
    public <T extends EventListener> void unregisterListener(EventListener listener) {
        this.collectionLock.writeLock().lock();
        try {
            eventHandlers.removeHandler(listener);
        } finally {
            this.collectionLock.writeLock().unlock();
        }
    }

    private void triggerEvent0(Event event) {
        // Assume we already acquired a readLock:

        eventHandlers.triggerEvent(event);
    }


    private <T extends EventListener> void unregisterListener0(T listener, Method listenerMethod) {

    }

}
