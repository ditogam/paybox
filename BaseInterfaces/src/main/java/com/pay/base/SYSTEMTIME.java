package com.pay.base;

/**
 * Created by dito on 4/5/2017.
 */
public final class SYSTEMTIME
{
    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [MarshalAs(UnmanagedType.U2)] public short Year;
    public short Year;
    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [MarshalAs(UnmanagedType.U2)] public short Month;
    public short Month;
    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [MarshalAs(UnmanagedType.U2)] public short DayOfWeek;
    public short DayOfWeek;
    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [MarshalAs(UnmanagedType.U2)] public short Day;
    public short Day;
    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [MarshalAs(UnmanagedType.U2)] public short Hour;
    public short Hour;
    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [MarshalAs(UnmanagedType.U2)] public short Minute;
    public short Minute;
    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [MarshalAs(UnmanagedType.U2)] public short Second;
    public short Second;
    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [MarshalAs(UnmanagedType.U2)] public short Milliseconds;
    public short Milliseconds;

    @Override
    public boolean equals(Object obj)
    {
        SYSTEMTIME ST = (SYSTEMTIME) obj;

        if (Year == ST.Year && Month == ST.Month && DayOfWeek == ST.DayOfWeek && Day == ST.Day && Hour == ST.Hour && Minute == ST.Minute && Second == ST.Second && Milliseconds == ST.Milliseconds)
        {
            return true;
        }
        return false;
    }

    public boolean Compare(SYSTEMTIME ST)
    {
        int inter = ST.Minute - Minute;

        if (Year == ST.Year && Month == ST.Month && DayOfWeek == ST.DayOfWeek && Day == ST.Day && Hour == ST.Hour && Minute == ST.Minute && (inter > -5 || inter < 5))
        {
            return true;
        }
        return false;
    }

    @Override
    public String toString()
    {
        String str = Year + ";" + Month + ";" + DayOfWeek + ";" + Day + ";" + Hour + ";" + Minute + ";" + Second + ";" + Milliseconds + ";";

        return str;
    }

    public void FromString(String Str)
    {
//C# TO JAVA CONVERTER TODO TASK: There is no equivalent to implicit typing in Java:
        String[] strArray = Str.split("[;]", -1);
        Year = Short.parseShort(strArray[0]);
        Month = Short.parseShort(strArray[1]);
        DayOfWeek = Short.parseShort(strArray[2]);
        Day = Short.parseShort(strArray[3]);
        Hour = Short.parseShort(strArray[4]);
        Minute = Short.parseShort(strArray[5]);
        Second = Short.parseShort(strArray[6]);
        Milliseconds = Short.parseShort(strArray[7]);
    }

    public SYSTEMTIME clone()
    {
        SYSTEMTIME varCopy = new SYSTEMTIME();

        varCopy.Year = this.Year;
        varCopy.Month = this.Month;
        varCopy.DayOfWeek = this.DayOfWeek;
        varCopy.Day = this.Day;
        varCopy.Hour = this.Hour;
        varCopy.Minute = this.Minute;
        varCopy.Second = this.Second;
        varCopy.Milliseconds = this.Milliseconds;

        return varCopy;
    }
}