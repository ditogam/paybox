package com.pay.base;

/**
 * Created by dito on 4/5/2017.
 */
public enum AlertReason
{
    Unknown(0),
    Maintenance(1),
    StartUp(2);

    public static final int SIZE = java.lang.Integer.SIZE;

    private int intValue;
    private static java.util.HashMap<Integer, AlertReason> mappings;
    private static java.util.HashMap<Integer, AlertReason> getMappings()
    {
        if (mappings == null)
        {
            synchronized (AlertReason.class)
            {
                if (mappings == null)
                {
                    mappings = new java.util.HashMap<Integer, AlertReason>();
                }
            }
        }
        return mappings;
    }

    private AlertReason(int value)
    {
        intValue = value;
        getMappings().put(value, this);
    }

    public int getValue()
    {
        return intValue;
    }

    public static AlertReason forValue(int value)
    {
        return getMappings().get(value);
    }
}