package com.pay.base;

import com.pay.base.enums.PayboxState;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by dito on 4/5/2017.
 */
public class ServerRequest implements Serializable
{
    public ServerRequest(PayboxState payboxGlobalState, LastReceivedMoneyTypesInfo lastReceivedMoneyTypesInfo, boolean needServerTimeZone, String currentUsedInternetConnection)
    {
        setPayboxTime(java.time.LocalDateTime.now());
        setPayboxGlobalState(payboxGlobalState);
        setNeedServerTimeZone(needServerTimeZone);
        setLastReceivedMoneyTypesInfo(lastReceivedMoneyTypesInfo);
        setCurrentUsedInternetConnection(currentUsedInternetConnection);
    }

    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [DataMember] public DateTime PayboxTime {get;set;}
    private java.time.LocalDateTime PayboxTime = java.time.LocalDateTime.MIN;
    public final java.time.LocalDateTime getPayboxTime()
    {
        return PayboxTime;
    }
    public final void setPayboxTime(java.time.LocalDateTime value)
    {
        PayboxTime = value;
    }

    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [DataMember] public bool NeedServerTimeZone {get;set;}
    private boolean NeedServerTimeZone;
    public final boolean getNeedServerTimeZone()
    {
        return NeedServerTimeZone;
    }
    public final void setNeedServerTimeZone(boolean value)
    {
        NeedServerTimeZone = value;
    }

    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [DataMember] public PayboxState PayboxGlobalState {get;set;}
    private PayboxState PayboxGlobalState = PayboxState.values()[0];
    public final PayboxState getPayboxGlobalState()
    {
        return PayboxGlobalState;
    }
    public final void setPayboxGlobalState(PayboxState value)
    {
        PayboxGlobalState = value;
    }

    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [DataMember] public LastReceivedMoneyTypesInfo LastReceivedMoneyTypesInfo {get;set;}
    private LastReceivedMoneyTypesInfo LastReceivedMoneyTypesInfo;
    public final LastReceivedMoneyTypesInfo getLastReceivedMoneyTypesInfo()
    {
        return LastReceivedMoneyTypesInfo;
    }
    public final void setLastReceivedMoneyTypesInfo(LastReceivedMoneyTypesInfo value)
    {
        LastReceivedMoneyTypesInfo = value;
    }

    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [DataMember] public string ConfigHash {get;set;}
    private String ConfigHash;
    public final String getConfigHash()
    {
        return ConfigHash;
    }
    public final void setConfigHash(String value)
    {
        ConfigHash = value;
    }

    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [DataMember] public string SoftHash {get;set;}
    private String SoftHash;
    public final String getSoftHash()
    {
        return SoftHash;
    }
    public final void setSoftHash(String value)
    {
        SoftHash = value;
    }

    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [DataMember] public string[] ContentVersions {get;set;}
    private String[] ContentVersions;
    public final String[] getContentVersions()
    {
        return ContentVersions;
    }
    public final void setContentVersions(String[] value)
    {
        ContentVersions = value;
    }

    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [DataMember] public int ContentId {get;set;}
    private int ContentId;
    public final int getContentId()
    {
        return ContentId;
    }
    public final void setContentId(int value)
    {
        ContentId = value;
    }

    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [DataMember] public int BinaryPackageId {get;set;}
    private int BinaryPackageId;
    public final int getBinaryPackageId()
    {
        return BinaryPackageId;
    }
    public final void setBinaryPackageId(int value)
    {
        BinaryPackageId = value;
    }

    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [DataMember] public int CurrentUsedContentVersionId {get;set;}
    private int CurrentUsedContentVersionId;
    public final int getCurrentUsedContentVersionId()
    {
        return CurrentUsedContentVersionId;
    }
    public final void setCurrentUsedContentVersionId(int value)
    {
        CurrentUsedContentVersionId = value;
    }

    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [DataMember] public string CurrentUsedInternetConnection {get;set;}
    private String CurrentUsedInternetConnection;
    public final String getCurrentUsedInternetConnection()
    {
        return CurrentUsedInternetConnection;
    }
    public final void setCurrentUsedInternetConnection(String value)
    {
        CurrentUsedInternetConnection = value;
    }

    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [DataMember] public int AcceptedCashCount {get;set;}
    private int AcceptedCashCount;
    public final int getAcceptedCashCount()
    {
        return AcceptedCashCount;
    }
    public final void setAcceptedCashCount(int value)
    {
        AcceptedCashCount = value;
    }

    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [DataMember] public int RejectedCashCount {get;set;}
    private int RejectedCashCount;
    public final int getRejectedCashCount()
    {
        return RejectedCashCount;
    }
    public final void setRejectedCashCount(int value)
    {
        RejectedCashCount = value;
    }

    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [DataMember] public string PrashivkaVersion {get;set;}
    private String PrashivkaVersion;
    public final String getPrashivkaVersion()
    {
        return PrashivkaVersion;
    }
    public final void setPrashivkaVersion(String value)
    {
        PrashivkaVersion = value;
    }

    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [DataMember] public SoftState SoftState {get;set;}
    private SoftState SoftState = getSoftState().values()[0];
    public final SoftState getSoftState()
    {
        return SoftState;
    }
    public final void setSoftState(SoftState value)
    {
        SoftState = value;
    }

    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [DataMember] public Dictionary<string, object> NamedParameters {get;set;}
    private HashMap<String, Object> NamedParameters;
    public final HashMap<String, Object> getNamedParameters()
    {
        return NamedParameters;
    }
    public final void setNamedParameters(HashMap<String, Object> value)
    {
        NamedParameters = value;
    }
}
