package com.pay.base.advertisement;


import java.time.Duration;

public class TopBanner {
    private String ItemName;

    public final String getItemName() {
        return ItemName;
    }

    public final void setItemName(String value) {
        ItemName = value;
    }

    private String ItemResourceId;

    public final String getItemResourceId() {
        return ItemResourceId;
    }

    public final void setItemResourceId(String value) {
        ItemResourceId = value;
    }

    private int ItemLinkId;

    public final int getItemLinkId() {
        return ItemLinkId;
    }

    public final void setItemLinkId(int value) {
        ItemLinkId = value;
    }

    private Duration SecondsLeft;

    public final Duration getSecondsLeft() {
        return SecondsLeft;
    }

    public final void setSecondsLeft(Duration value) {
        SecondsLeft = value;
    }
}