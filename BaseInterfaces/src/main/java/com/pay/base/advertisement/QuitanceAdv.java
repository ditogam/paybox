package com.pay.base.advertisement;

public class QuitanceAdv
{
	private String ItemName;
	public final String getItemName()
	{
		return ItemName;
	}
	public final void setItemName(String value)
	{
		ItemName = value;
	}

	private String ItemResourceId;
	public final String getItemResourceId()
	{
		return ItemResourceId;
	}
	public final void setItemResourceId(String value)
	{
		ItemResourceId = value;
	}
}