package com.pay.base.advertisement;

public interface IAdvertisementEngine
{
	/** 
		 Gets advertisement texts
	 
	 @return 
	*/
	NovaTextAdv GetNovaTextAvdItem();

	/** 
		 Gets advertisement item to show on quitance
	 
	 @return 
	*/
	QuitanceAdv GetQuitanceAdvItem();

	/** 
		 Gets advertisement item to show on validation page
	 
	 @return 
	*/
	ValidationAdv GetValidationAdvItem(String configName);

	/** 
		 Gets advertisement item to show after transaction
	 
	 @return 
	*/
	EndTransactionAdv GetEndTransactionAdvItem(String configName);

	/** 
		 Gets advertisement item to show in complex templates
	 
	 @param configName
	 @return 
	*/
	MiddleTransactionAdv GetMiddleTransactionAdvItem(String configName);

	/** 
		 Gets advertisement item to show on the top banner
	 
	 @param datetime The date you want to get advertisement for.
	 @return 
	*/
	TopBanner GetTopBannerItem(java.time.LocalDateTime datetime);

	TopBanner GetRootCategoryBannerItem(java.time.LocalDateTime dateTime);
}