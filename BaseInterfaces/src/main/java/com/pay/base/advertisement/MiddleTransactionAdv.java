package com.pay.base.advertisement;

public class MiddleTransactionAdv
{
	private String ItemName;
	public final String getItemName()
	{
		return ItemName;
	}
	public final void setItemName(String value)
	{
		ItemName = value;
	}

	private String ItemTextResourceId;
	public final String getItemTextResourceId()
	{
		return ItemTextResourceId;
	}
	public final void setItemTextResourceId(String value)
	{
		ItemTextResourceId = value;
	}

	private String ItemImageResourceId;
	public final String getItemImageResourceId()
	{
		return ItemImageResourceId;
	}
	public final void setItemImageResourceId(String value)
	{
		ItemImageResourceId = value;
	}

	private int ItemLinkId;
	public final int getItemLinkId()
	{
		return ItemLinkId;
	}
	public final void setItemLinkId(int value)
	{
		ItemLinkId = value;
	}
}