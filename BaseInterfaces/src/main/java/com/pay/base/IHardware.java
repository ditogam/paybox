package com.pay.base;

import com.google.common.primitives.UnsignedInteger;
import com.pay.base.config.xml.XmlDocument;
import com.pay.base.devicestate.DeviceState;
import com.pay.base.event.EventListener;
import com.pay.base.event.types.CashReceivedEvent;
import com.pay.base.event.types.DeviceChangedEvent;
import com.pay.helper.enums.LogType;
import org.apache.commons.lang3.mutable.MutableInt;

import java.util.Date;
import java.util.Map;

/**
 * Created by dito on 3/31/2017.
 */
public interface IHardware extends IHardwareBase {

    boolean Initialize(String payboxDeviceTypeName);

    boolean Deinitialize(String payboxDeviceTypeName);

    boolean Reset(String payboxDeviceTypeName);

    DeviceState GetState(boolean force, String payboxDeviceTypeName);

    DeviceState[] GetStates(boolean force);

    // #region CashIn Functionality
//    event CashReceivedEventHandler CashReceived;

    void addCashReceivedHandler(EventListener<CashReceivedEvent> listener );

    void removeCashReceivedHandler(EventListener<CashReceivedEvent> listener);

    boolean AllowCashReceive(int... allowCashIDList);

    boolean AllowCashReceive();

    boolean DenyCashReceive();

    int[] GetAllowedCashIDList();

    void GetAcceptRejectCounters(MutableInt accepted, MutableInt rejected);

    String getPrashivkaVersion();
    //  #endregion

    //    #region Printer Functionaliyy
    DeviceState PrintSynchronously(String payboxPrinterTypeName, String taggedText, int timeoutIntervalInMiliSeconds);

    DeviceState PrintSynchronously(String payboxPrinterTypeName, String taggedText, Printer.PrintMode printMode, int lineFeedCount, boolean fullCut, int timeoutIntervalInMiliSeconds);

    void PrintAsynchronously(String payboxPrinterTypeName, String taggedText);

    void PrintAsynchronously(String payboxPrinterTypeName, String taggedText, Printer.PrintMode printMode, int lineFeedCount, boolean fullCut);
    //    #endregion

    //   #region Network Functionality
    boolean NetworkConnect(boolean tryOtherConnection);

    boolean NetworkDisconnect();

    boolean NetworkReconnect();
    //    #endregion

    //    #region WatchDog Fuctionality
    int GetWatchDogPollingInterval(String payboxDeviceTypeName); //Return Result Is In Seconds

    Date GetWatchDogDateTime(String payboxDeviceTypeName);

    boolean SetWatchDogDateTime(String payboxDeviceTypeName, Date dateTime);

    boolean PollWatchDog(String payboxDeviceTypeName);

    boolean ComputerReset(String payboxDeviceTypeName);

    boolean ComputerShutdown(String payboxDeviceTypeName, UnsignedInteger startUpTimeOut);

    boolean ModemReset(String payboxDeviceTypeName);

    boolean ModemShutdown(String payboxDeviceTypeName, UnsignedInteger startUpTimeOut);

    boolean WatchDogReset(String payboxDeviceTypeName);

    boolean PowerAdapterReset(String payboxDeviceTypeName);

    boolean PowerAdapterOn(String payboxDeviceTypeName);

    boolean PowerAdapterOff(String payboxDeviceTypeName);
    //    #endregion

    //        #region Vending Fuctionality


    boolean DropItem(int slotId, int timeoutIntervalInMiliSeconds, boolean test);

    boolean DropItem(int slotId);

    boolean ChangeItemsInSlots(Map<Integer, Integer> slotItemsDictionary);
    //  #endregion

    Object GetDeviceResource(String name);

    void Log(String facility, String value, LogType type);

    void LogException(Exception ex);

    XmlDocument GetConfig(String configFacility, String additionalParameter);

    String GetStaticValue(String key);

    void SaveStaticValue(String key, String value);

    void SubscribeSoftwareActionEvents(IGeneralLogicModule generalModule);

    Map<String, Map<String, Integer>> GetAllDeviceCountersDictionary();
}
