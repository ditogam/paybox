package com.pay.base;

/**
 * Created by dito on 4/4/2017.
 */
public interface IScriptEntryPoint
{
    void ExecuteSynchronous(String method, Object... arguments);
    void ExecuteAsynchronous(String method, Object... arguments);

    IServerContainer getContainer();
    void setContainer(IServerContainer value);

    void CreateForm(String formType, Object... arguments);
}
