package com.pay.base;

public enum CardReadStatus
{
	NotAutorized,
	OK,
	Error;

	public static final int SIZE = Integer.SIZE;

	public int getValue()
	{
		return this.ordinal();
	}

	public static CardReadStatus forValue(int value)
	{
		return values()[value];
	}
}