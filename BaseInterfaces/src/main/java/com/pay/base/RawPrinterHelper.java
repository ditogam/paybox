package com.pay.base;

import com.pay.helper.RefObject;

/**
 * Created by dito on 4/7/2017.
 */
public class RawPrinterHelper {
    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [DllImport("winspool.Drv", EntryPoint = "OpenPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)] public static extern bool OpenPrinter([MarshalAs(UnmanagedType.LPStr)] string szPrinter, out IntPtr hPrinter, IntPtr pd);
  /*  public static native boolean OpenPrinter(String szPrinter, tangible.RefObject<IntPtr> hPrinter, IntPtr pd);
    static
    {
        System.loadLibrary("winspool.Drv");
    }

    public static native boolean ClosePrinter(IntPtr hPrinter);

    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [DllImport("winspool.Drv", EntryPoint = "StartDocPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)] public static extern bool StartDocPrinter(IntPtr hPrinter, int level, [In][MarshalAs(UnmanagedType.LPStruct)] DOCINFOA di);
    public static native boolean StartDocPrinter(IntPtr hPrinter, int level, DOCINFOA di);

    public static native boolean EndDocPrinter(IntPtr hPrinter);

    public static native boolean StartPagePrinter(IntPtr hPrinter);

    public static native boolean EndPagePrinter(IntPtr hPrinter);

    public static native boolean WritePrinter(IntPtr hPrinter, IntPtr pBytes, int dwCount, tangible.RefObject<Integer> dwWritten);

    public static native boolean ReadPrinter(IntPtr hPrinter, tangible.RefObject<IntPtr> pBytes, int dwCount, tangible.RefObject<Integer> dwReaden);

    //[DllImport("winspool.drv", SetLastError = true)]
    //public static extern bool GetPrinter(IntPtr hPrinter, Int32 dwLevel, IntPtr pPrinter, Int32 dwBuf, out Int32 dwNeeded);

    public static native boolean GetPrinter(IntPtr hPrinter, int dwLevel, IntPtr pPrinter, int dwBuf, tangible.RefObject<Integer> dwNeeded);
    static
    {
        System.loadLibrary("winspool.drv");
    }
    */

    // SendBytesToPrinter()
    // When the function is given a printer name and an unmanaged array
    // of bytes, the function sends those bytes to the print queue.
    // Returns true on success, false on failure.
   /* public static boolean SendBytesToPrinter(String szPrinterName, IntPtr pBytes, int dwCount)
    {
        boolean bSuccess = false; // Assume failure unless you specifically succeed.

        int dwError = 0, dwWritten = 0;
        IntPtr hPrinter = new IntPtr(0);
        DOCINFOA di = new DOCINFOA();


        di.pDocName = "My C#.NET RAW Document";
        di.pDataType = "RAW";

        // Open the printer.
        tangible.RefObject<IntPtr> tempRef_hPrinter = new tangible.RefObject<IntPtr>(hPrinter);
        if (OpenPrinter(szPrinterName.Normalize(), tempRef_hPrinter, IntPtr.Zero))
        {
            hPrinter = tempRef_hPrinter.argValue;
            // Start a document.
            if (StartDocPrinter(hPrinter, 1, di))
            {
                // Start a page.
                if (StartPagePrinter(hPrinter))
                {
                    // Write your bytes.
                    tangible.RefObject<Integer> tempRef_dwWritten = new tangible.RefObject<Integer>(dwWritten);
                    bSuccess = WritePrinter(hPrinter, pBytes, dwCount, tempRef_dwWritten);
                    dwWritten = tempRef_dwWritten.argValue;
                    EndPagePrinter(hPrinter);
                }
                EndDocPrinter(hPrinter);
            }
            ClosePrinter(hPrinter);
        }
        else
        {
            hPrinter = tempRef_hPrinter.argValue;
        }
        // If you did not succeed, GetLastError may give more information
        // about why not.
        if (bSuccess == false)
        {
            dwError = Marshal.GetLastWin32Error();
        }
        return bSuccess;
    }*/

    public static boolean SendFileToPrinter(String szPrinterName, String szFileName) {
        boolean bSuccess = false;
        /*
        // Open the file.
//C# TO JAVA CONVERTER TODO TASK: C# to Java Converter cannot determine whether this System.IO.FileStream is input or output:
        FileStream fs = new FileStream(szFileName, FileMode.Open);
        // Create a BinaryReader on the file.
        BinaryReader br = new BinaryReader(fs);
        // Dim an array of bytes big enough to hold the file's contents.
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: var bytes = new byte[fs.Length];
        byte[] bytes = new byte[fs.getLength()];

        // Your unmanaged pointer.
        IntPtr pUnmanagedBytes = new IntPtr(0);
        int nLength;

        nLength = (int)fs.getLength();
        // Read the contents of the file into the array.
        bytes = br.ReadBytes(nLength);
        // Allocate some unmanaged memory for those bytes.
        pUnmanagedBytes = Marshal.AllocCoTaskMem(nLength);
        // Copy the managed byte array into the unmanaged array.
        Marshal.Copy(bytes, 0, pUnmanagedBytes, nLength);
        // Send the unmanaged bytes to the printer.
        bSuccess = SendBytesToPrinter(szPrinterName, pUnmanagedBytes, nLength);
        // Free the unmanaged memory that you allocated earlier.
        Marshal.FreeCoTaskMem(pUnmanagedBytes);*/
        return bSuccess;
    }

    //C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: public static bool SendBytesToPrinter(string szPrinterName, byte[] szBytes)
    public static boolean SendBytesToPrinter(String szPrinterName, byte[] szBytes) {
        boolean bSuccess = false;
        /*
        // Open the file.
        // Create a BinaryReader on the file.
        // Dim an array of bytes big enough to hold the file's contents.

        // Your unmanaged pointer.
        IntPtr pUnmanagedBytes = new IntPtr(0);
        // Read the contents of the file into the array.
        // Allocate some unmanaged memory for those bytes.
        pUnmanagedBytes = Marshal.AllocCoTaskMem(szBytes.length);
        // Copy the managed byte array into the unmanaged array.
        Marshal.Copy(szBytes, 0, pUnmanagedBytes, szBytes.length);
        // Send the unmanaged bytes to the printer.
        bSuccess = SendBytesToPrinter(szPrinterName, pUnmanagedBytes, szBytes.length);
        // Free the unmanaged memory that you allocated earlier.
        Marshal.FreeCoTaskMem(pUnmanagedBytes);
        */
        return bSuccess;
    }

    public static boolean SendStringToPrinter(String szPrinterName, String szString) {
       /* IntPtr pBytes = new IntPtr();
        int dwCount;
        // How many characters are in the string?
        dwCount = szString.length();
        // Assume that the printer is expecting ANSI text, and then convert
        // the string to ANSI text.
        pBytes = Marshal.StringToCoTaskMemAnsi(szString);
        // Send the converted ANSI string to the printer.
        SendBytesToPrinter(szPrinterName, pBytes, dwCount);
        Marshal.FreeCoTaskMem(pBytes);
        */
        return true;
    }

    //C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: public static bool ReadBytesFromPrinter(string szPrinterName, out byte[] szBytes)
    public static boolean ReadBytesFromPrinter(String szPrinterName, RefObject<byte[]> szBytes) {
        // Open the file.
        // Create a BinaryReader on the file.
        // Dim an array of bytes big enough to hold the file's contents.
        boolean bSuccess = false;
       /*
        // Your unmanaged pointer.
        IntPtr pUnmanagedBytes = new IntPtr(0);
        // Read the contents of the file into the array.
        // Allocate some unmanaged memory for those bytes.
        pUnmanagedBytes = Marshal.AllocCoTaskMem(10);

        int dwReaden = 0;
        // Send the unmanaged bytes to the printer.
        tangible.RefObject<IntPtr> tempRef_pUnmanagedBytes = new tangible.RefObject<IntPtr>(pUnmanagedBytes);
        tangible.RefObject<Integer> tempRef_dwReaden = new tangible.RefObject<Integer>(dwReaden);
        bSuccess = ReadBytesFromPrinter(szPrinterName, tempRef_pUnmanagedBytes, 10, tempRef_dwReaden);
        dwReaden = tempRef_dwReaden.argValue;
        pUnmanagedBytes = tempRef_pUnmanagedBytes.argValue;

//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: szBytes = new byte[dwReaden];
        szBytes.argValue = new byte[dwReaden];
        // Copy the unmanaged byte array into the managed array.
        Marshal.Copy(pUnmanagedBytes, szBytes.argValue, 0, dwReaden);

        // Free the unmanaged memory that you allocated earlier.
        Marshal.FreeCoTaskMem(pUnmanagedBytes);
        */
        return bSuccess;
    }

    /*public static boolean ReadBytesFromPrinter(String szPrinterName, RefObject<IntPtr> pBytes, int dwCount, tangible.RefObject<Integer> dwReaden)
    {
        boolean bSuccess = false; // Assume failure unless you specifically succeed.

        int dwError = 0;
        dwReaden.argValue = 0;
        IntPtr hPrinter = new IntPtr(0);


        // Open the printer.
        tangible.RefObject<IntPtr> tempRef_hPrinter = new tangible.RefObject<IntPtr>(hPrinter);
        if (OpenPrinter(szPrinterName.Normalize(), tempRef_hPrinter, IntPtr.Zero))
        {
            hPrinter = tempRef_hPrinter.argValue;
            // Read bytes.
            bSuccess = ReadPrinter(hPrinter, pBytes, dwCount, dwReaden);

            ClosePrinter(hPrinter);
        }
        else
        {
            hPrinter = tempRef_hPrinter.argValue;
        }
        // If you did not succeed, GetLastError may give more information
        // about why not.
        if (bSuccess == false)
        {
            dwError = Marshal.GetLastWin32Error();
        }
        return bSuccess;
    }
*/
    public static boolean ReadBytesFromPrinter2(String szPrinterName) {
        boolean bRet = false;
      /*  IntPtr pHandle = new IntPtr(0);

        tangible.RefObject<IntPtr> tempRef_pHandle = new tangible.RefObject<IntPtr>(pHandle);
        if (OpenPrinter(szPrinterName.Normalize(), tempRef_pHandle, IntPtr.Zero))
        {
            pHandle = tempRef_pHandle.argValue;
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: uint cbNeeded = 0;
            int cbNeeded = 0;

            tangible.RefObject<Integer> tempRef_cbNeeded = new tangible.RefObject<Integer>(cbNeeded);
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: bRet = GetPrinter(pHandle, 6, IntPtr.Zero, 0, ref cbNeeded);
            bRet = GetPrinter(pHandle, 6, IntPtr.Zero, 0, tempRef_cbNeeded);
            cbNeeded = tempRef_cbNeeded.argValue;

            if (cbNeeded > 0)
            {
                System.IntPtr pAddr = Marshal.AllocHGlobal((int) cbNeeded);
                tangible.RefObject<Integer> tempRef_cbNeeded2 = new tangible.RefObject<Integer>(cbNeeded);
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: bRet = GetPrinter(pHandle, 6, pAddr, cbNeeded, ref cbNeeded);
                bRet = GetPrinter(pHandle, 6, pAddr, cbNeeded, tempRef_cbNeeded2);
                cbNeeded = tempRef_cbNeeded2.argValue;
                if (bRet)
                {
                    PRINTER_INFO_6 Info6 = new PRINTER_INFO_6();
                    Info6 = (PRINTER_INFO_6) Marshal.PtrToStructure(pAddr, PRINTER_INFO_6.class);
                    // Now use the info from Info2 structure etc
                }

                Marshal.FreeHGlobal(pAddr);
            }

            ClosePrinter(pHandle);
        }
        else
        {
            pHandle = tempRef_pHandle.argValue;
        }
*/
        return bRet;
    }

//    public static native int GetPrinterData(IntPtr hPrinter, String pValueName, tangible.RefObject<Integer> pType, IntPtr pBytes, int dwCount, tangible.RefObject<Integer> dwReaden);

    //C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: public static bool GetPrinterStatusData(string szPrinterName, out byte[] statusBytes, int dataSize)
    public static boolean GetPrinterStatusData(String szPrinterName, RefObject<byte[]> statusBytes, int dataSize) {
        boolean bSuccess = false; // Assume failure unless you specifically succeed.
      /*  int dwError = 0;
        IntPtr hPrinter = new IntPtr(0);

//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: statusBytes = new byte[dataSize];
        statusBytes.argValue = new byte[dataSize];

        // Open the printer.
        tangible.RefObject<IntPtr> tempRef_hPrinter = new tangible.RefObject<IntPtr>(hPrinter);
        if (OpenPrinter(szPrinterName.Normalize(), tempRef_hPrinter, IntPtr.Zero))
        {
            hPrinter = tempRef_hPrinter.argValue;
            int pType = 0;
            int pcbN = 0;
            System.IntPtr pData = Marshal.AllocHGlobal(statusBytes.argValue.length);

            tangible.RefObject<Integer> tempRef_pType = new tangible.RefObject<Integer>(pType);
            tangible.RefObject<Integer> tempRef_pcbN = new tangible.RefObject<Integer>(pcbN);
//C# TO JAVA CONVERTER TODO TASK: There is no equivalent to implicit typing in Java:
            var result = GetPrinterData(hPrinter, "Status", tempRef_pType, pData, dataSize, tempRef_pcbN);
            pcbN = tempRef_pcbN.argValue;
            pType = tempRef_pType.argValue;

            if (result == 0)
            {
                Marshal.Copy(pData, statusBytes.argValue, 0, statusBytes.argValue.length);
                bSuccess = true;
            }

            Marshal.FreeHGlobal(pData);

            ClosePrinter(hPrinter);
        }
        else
        {
            hPrinter = tempRef_hPrinter.argValue;
        }

        // If you did not succeed, GetLastError may give more information
        // about why not.
        if (bSuccess == false)
        {
            dwError = Marshal.GetLastWin32Error();
        }
*/
        return bSuccess;
    }

    // Structure and API declarions:
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)] public class DOCINFOA
    public static class DOCINFOA {
        //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [MarshalAs(UnmanagedType.LPStr)] public string pDataType;
        public String pDataType;

        //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [MarshalAs(UnmanagedType.LPStr)] public string pDocName;
        public String pDocName;

        //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [MarshalAs(UnmanagedType.LPStr)] public string pOutputFile;
        public String pOutputFile;
    }

    //C# TO JAVA CONVERTER WARNING: Java does not allow user-defined value types. The behavior of this class will differ from the original:
//ORIGINAL LINE: [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)] public struct PRINTER_INFO_6
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
    public final static class PRINTER_INFO_6 {
        //C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: public uint dwStatus;
        public int dwStatus;

        public PRINTER_INFO_6 clone() {
            PRINTER_INFO_6 varCopy = new PRINTER_INFO_6();

            varCopy.dwStatus = this.dwStatus;

            return varCopy;
        }
    }
}