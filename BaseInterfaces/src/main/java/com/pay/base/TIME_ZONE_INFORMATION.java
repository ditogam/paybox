package com.pay.base;

/**
 * Created by dito on 4/5/2017.
 */
public final class TIME_ZONE_INFORMATION
{
    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [MarshalAs(UnmanagedType.I4)] public int Bias;
    public int Bias;

    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)] public string StandardName;
    public String StandardName;

    public SYSTEMTIME StandardDate = new SYSTEMTIME();

    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [MarshalAs(UnmanagedType.I4)] public int StandardBias;
    public int StandardBias;

    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)] public string DaylightName;
    public String DaylightName;

    public SYSTEMTIME DaylightDate = new SYSTEMTIME();

    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [MarshalAs(UnmanagedType.I4)] public int DaylightBias;
    public int DaylightBias;

    @Override
    public boolean equals(Object obj)
    {
        TIME_ZONE_INFORMATION tzi = (TIME_ZONE_INFORMATION) obj;
        return Bias == tzi.Bias && StandardName.equals(tzi.StandardName) && StandardDate.equals(tzi.StandardDate.clone()) && StandardBias == tzi.StandardBias && DaylightName.equals(tzi.DaylightName) && DaylightDate.equals(tzi.DaylightDate.clone()) && DaylightBias == tzi.DaylightBias;
    }

    public TIME_ZONE_INFORMATION clone()
    {
        TIME_ZONE_INFORMATION varCopy = new TIME_ZONE_INFORMATION();

        varCopy.Bias = this.Bias;
        varCopy.StandardName = this.StandardName;
        varCopy.StandardDate = this.StandardDate.clone();
        varCopy.StandardBias = this.StandardBias;
        varCopy.DaylightName = this.DaylightName;
        varCopy.DaylightDate = this.DaylightDate.clone();
        varCopy.DaylightBias = this.DaylightBias;

        return varCopy;
    }
}
