package com.pay.base.vending;

import com.pay.base.devicestate.WatchDogDeviceState;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dito on 4/3/2017.
 */
public class Vending {

    public boolean DropItem(int slotId, int timeoutIntervalInMiliSeconds, boolean test) {
        return false;
    }

    public boolean ChangeItemsInSlots(Map<Integer, Integer> slotItemsDictionary) {
        return false;
    }

    public enum SensorName {
        ItemExists,
        MotorAtStartUpPosition,
        ItemDropped,
        MotorLastState //Open - Error, Close - OK
    }

    public enum SensorState {
        Unknown,
        Open, //inactive
        Close, //active
    }

    public class Slot {
        public int id;
        public Map<SensorName, SensorState> sensorDictionary = new HashMap<>(); //<Key: Sensor Name, Value: sensor state>
        public int itemCount; //does not participate in Equals() ???

        public Slot(int id, Map<SensorName, SensorState> sensorDictionary, int itemCount) {
            this.id = id;
            this.sensorDictionary = sensorDictionary;
            this.itemCount = itemCount;
        }

        @Override
        public String toString() {
            StringBuilder sensorDictionaryStr = new StringBuilder("");
            for (SensorName sensorName : this.sensorDictionary.keySet()) {
                sensorDictionaryStr.append(sensorName);
                sensorDictionaryStr.append("=");
                sensorDictionaryStr.append(this.sensorDictionary.get(sensorName));
                sensorDictionaryStr.append(",");
            }
            return String.format("ID:%s, SensorDictionary:%s, ItemCount:%s", this.id + "", sensorDictionaryStr.toString(), itemCount + "");
        }


        public int getId() {
            return id;
        }

        public Map<SensorName, SensorState> getSensorDictionary() {
            return sensorDictionary;
        }

        public int getItemCount() {
            return itemCount;
        }

        public void setSensorDictionary(Map<SensorName, SensorState> sensorDictionary) {
            this.sensorDictionary = sensorDictionary;
        }

        public void setItemCount(int itemCount) {
            this.itemCount = itemCount;
        }

        @Override
        public boolean equals(Object obj) {
            boolean baseEquals = super.equals(obj);
            if (baseEquals)
                return true;

            if (!(obj instanceof Slot))
                return false;
            Slot newSlot = (Slot) obj;
            boolean equals=(this.id == newSlot.id) && (this.sensorDictionary.size() == newSlot.sensorDictionary.size()) && (this.itemCount == newSlot.itemCount);
            for (SensorName sensorName : sensorDictionary.keySet()) {
                SensorState sensorState=sensorDictionary.get(sensorName);
                if(!newSlot.sensorDictionary.containsKey(sensorName)) {
                    equals = false;
                    break;
                }
            }
            return equals;
        }



        public Slot copy() {
            Map<SensorName, SensorState> sensorDictionaryCopy = new HashMap<>(this.getSensorDictionary());
            Slot copy = new Slot(this.id, sensorDictionaryCopy, this.itemCount);

            return copy;
        }
    }
}
