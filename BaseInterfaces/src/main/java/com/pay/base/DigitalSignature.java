package com.pay.base;

import com.pay.base.devicestate.DeviceState;
import com.pay.base.event.EventManager;
import com.pay.base.event.types.Delegates;
import com.pay.base.event.types.DeviceChangedEvent;
import com.pay.base.event.types.SignedEvent;

import java.util.HashMap;

/**
 * Created by dito on 4/6/2017.
 */
public abstract class DigitalSignature implements IHardwareBase {
    protected boolean configIsValid = false;

    protected HashMap<Integer, Error> errorDictionary = new HashMap<Integer, Error>();
    protected String folderPath;
    protected IHardware hardware;
    protected boolean initialized = false;
    protected String payboxDeviceTypeName = "";


    private EventManager<SigCaptureEventArgs> signedEventHandler = new EventManager<>();

    protected String typeName = "";

    public abstract Object getControl();


    public final void OnSigned(Object sender, SigCaptureEventArgs e) {
        if (signedEventHandler != null) {
            signedEventHandler.invoke(new SignedEvent(sender, e));
        }
    }

    public abstract void StartPaint();


    public abstract boolean Initialize();

    public abstract boolean Deinitialize();

    public abstract boolean Reset();

    public abstract DeviceState GetState(boolean force);

    private EventManager<DeviceState> stateChangedEventHandler = new EventManager<>();


    public final void OnStateChanged(DeviceChangedEvent event) {
        if (stateChangedEventHandler != null) {
            stateChangedEventHandler.invoke(event);
        }
    }

    public final void SoftwareActionToDeviceHandler(Object sender, SoftwareActionToDevice actionArgs) {
    }

    public final HashMap<String, Integer> GetDeviceCountersDictionary() {
        return null;
    }

}