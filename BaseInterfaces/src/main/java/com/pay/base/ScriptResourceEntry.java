package com.pay.base;

/**
 * Created by dito on 4/4/2017.
 */
public class ScriptResourceEntry
{
    private String EntryKey;
    public final String getEntryKey()
    {
        return EntryKey;
    }
    public final void setEntryKey(String value)
    {
        EntryKey = value;
    }
    private String TargetId;
    public final String getTargetId()
    {
        return TargetId;
    }
    public final void setTargetId(String value)
    {
        TargetId = value;
    }
    private String TargetProperty;
    public final String getTargetProperty()
    {
        return TargetProperty;
    }
    public final void setTargetProperty(String value)
    {
        TargetProperty = value;
    }
    private String EntryValue;
    public final String getEntryValue()
    {
        return EntryValue;
    }
    public final void setEntryValue(String value)
    {
        EntryValue = value;
    }
}