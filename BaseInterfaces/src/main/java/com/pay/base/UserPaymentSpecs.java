package com.pay.base;

import com.pay.base.enums.PaymentSpecValueType;

/**
 * Created by dito on 4/4/2017.
 */
public class UserPaymentSpecs {
    private int ProductID;
    public final int getProductID()
    {
        return ProductID;
    }
    public final void setProductID(int value)
    {
        ProductID = value;
    }

    private double ByCashValue;
    public final double getByCashValue()
    {
        return ByCashValue;
    }
    public final void setByCashValue(double value)
    {
        ByCashValue = value;
    }
    private PaymentSpecValueType ByCashType = PaymentSpecValueType.values()[0];
    public final PaymentSpecValueType getByCashType()
    {
        return ByCashType;
    }
    public final void setByCashType(PaymentSpecValueType value)
    {
        ByCashType = value;
    }

    private double ByBalanceValue;
    public final double getByBalanceValue()
    {
        return ByBalanceValue;
    }
    public final void setByBalanceValue(double value)
    {
        ByBalanceValue = value;
    }
    private PaymentSpecValueType ByBalanceType = PaymentSpecValueType.values()[0];
    public final PaymentSpecValueType getByBalanceType()
    {
        return ByBalanceType;
    }
    public final void setByBalanceType(PaymentSpecValueType value)
    {
        ByBalanceType = value;
    }

    private double MyButtonByCashValue;
    public final double getMyButtonByCashValue()
    {
        return MyButtonByCashValue;
    }
    public final void setMyButtonByCashValue(double value)
    {
        MyButtonByCashValue = value;
    }
    private PaymentSpecValueType MyButtonByCashType = PaymentSpecValueType.values()[0];
    public final PaymentSpecValueType getMyButtonByCashType()
    {
        return MyButtonByCashType;
    }
    public final void setMyButtonByCashType(PaymentSpecValueType value)
    {
        MyButtonByCashType = value;
    }

    private double MyButtonByBalanceValue;
    public final double getMyButtonByBalanceValue()
    {
        return MyButtonByBalanceValue;
    }
    public final void setMyButtonByBalanceValue(double value)
    {
        MyButtonByBalanceValue = value;
    }
    private PaymentSpecValueType MyButtonByBalanceType = PaymentSpecValueType.values()[0];
    public final PaymentSpecValueType getMyButtonByBalanceType()
    {
        return MyButtonByBalanceType;
    }
    public final void setMyButtonByBalanceType(PaymentSpecValueType value)
    {
        MyButtonByBalanceType = value;
    }
}
