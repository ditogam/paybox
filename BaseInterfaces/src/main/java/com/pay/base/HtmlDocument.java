package com.pay.base;

import netscape.javascript.JSObject;

/**
 * Created by dito on 4/5/2017.
 */
public class HtmlDocument {
    JSObject window;

    public HtmlDocument(JSObject window) {
        this.window = window;
    }

    public JSObject getWindow() {
        return window;
    }
}
