package com.pay.base.dataengine;

import com.pay.helper.enums.LogType;

/**
 * Created by dito on 4/4/2017.
 */
public interface IDataEngineLogger
{
    void Log(String logFacility, String logValue, LogType type);

    void LogException(Exception ex);
}
