package com.pay.base.dataengine;

import com.pay.base.config.xml.XmlDocument;
import com.pay.base.config.xml.XmlReader;
import com.pay.base.config.xml.XmlReaderSettings;
import com.pay.base.dataengine.enums.ConfigType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by dito on 4/4/2017.
 */
public interface IDataEngineConfigurator
{
    //XmlDocument GetConfig(string configFacility);
    Map<Integer, Integer> getVendingProductSlots();

    Map<String, String[]> getMaintenanceUsers();

    ArrayList<Integer> getRestrictedVendingProducts();

    XmlDocument GetConfig(String configFacility, ConfigType configType);

    boolean SaveConfig(XmlDocument xmlDocument, String configFacility, ConfigType configType);

    XmlReader GetConfig(String configFacility, ConfigType configType, XmlReaderSettings settings);

    //XmlReader GetConfig(string configFacility, XmlReaderSettings settings);

    String GetStaticValue(String key);

    void SaveStaticValue(String key, String value);
}