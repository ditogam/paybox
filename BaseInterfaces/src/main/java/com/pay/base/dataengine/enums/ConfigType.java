package com.pay.base.dataengine.enums;

/**
 * Created by dito on 4/4/2017.
 */
/**
 კონტენტის ტიპი
 */
public enum ConfigType
{
    /**
     Hardware . . .
     */
    GeneralConfig(0),

    /**
     პროდუქტები,ნავიგაცია...
     */
    ContentConfig(1),

    /**
     ცვალებადი კონტენტი, მაგ. ამინდი...
     */
    ChangingContentConfig(2);

    public static final int SIZE = java.lang.Integer.SIZE;

    private int intValue;
    private static java.util.HashMap<Integer, ConfigType> mappings;
    private static java.util.HashMap<Integer, ConfigType> getMappings()
    {
        if (mappings == null)
        {
            synchronized (ConfigType.class)
            {
                if (mappings == null)
                {
                    mappings = new java.util.HashMap<Integer, ConfigType>();
                }
            }
        }
        return mappings;
    }

    private ConfigType(int value)
    {
        intValue = value;
        getMappings().put(value, this);
    }

    public int getValue()
    {
        return intValue;
    }

    public static ConfigType forValue(int value)
    {
        return getMappings().get(value);
    }
}