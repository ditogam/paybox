package com.pay.base.dataengine;

import com.pay.helper.RefObject;

import java.util.Hashtable;

/**
 * Created by dito on 4/4/2017.
 */
public interface IDataEnginePaymentProcessing
{
    int getReconciliationIndicator();
    long getPaymentLimit();
    boolean Verify(int productId, String customerData, int amount, int commission, RefObject<Hashtable> outputParameters);

    boolean Verify(int productId, String customerData, int amount, int commission, RefObject<Hashtable> outputParameters, String operation);

    boolean TestConnection();
    //void SyncPayment(int productId, string customerData, int amount, out Hashtable outputParameters);

    boolean Pay(int productId, String customerData, int amount, int commission, String generatedIRRN, RefObject<Hashtable> outputParameters);

    void PayAsync(int productId, String customerData, int amount, int commission, String generatedIRRN, RefObject<Hashtable> outputParameters);

    boolean GetBalance(RefObject<Long> balance);

    //void AsyncPayment(int productId, string customerData, int amount, int commission, string generatedIRRN,
    //                  out Hashtable outputParameters);

    //bool TestConnection();

    void CloseOperationDay(int totalAmount, String generatedRRN);

    String GenerateIRRN();

    String GenerateRRN();
}