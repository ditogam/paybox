package com.pay.base;

import com.pay.base.enums.Currency;
import com.pay.base.enums.MoneyType;

import java.util.Date;

/**
 * Created by dito on 3/31/2017.
 */
public abstract class Money {
    private Date receivedTime;
    private Currency currency;
    private MoneyType type;

    public Date getReceivedTime() {
        return receivedTime;
    }

    public Currency getCurrency() {
        return currency;
    }

    public MoneyType getType() {
        return type;
    }

    public Money(Currency currency, MoneyType type) {
        this.receivedTime=new Date();
        this.currency = currency;
        this.type = type;
    }
}
