package com.pay.base;

import com.pay.base.devicestate.DeviceState;
import com.pay.base.devicestate.NetworkConnectionDeviceState;
import com.pay.base.devicestate.enums.NetworkConnectionState;
import com.pay.base.event.EventManager;
import com.pay.base.event.types.Delegates;
import com.pay.base.event.types.DeviceChangedEvent;
import com.pay.helper.RefObject;
import com.pay.helper.enums.LogType;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * Created by dito on 4/5/2017.
 */
public abstract class NetworkConnection implements IHardwareBase, java.io.Closeable {
    protected boolean configIsValid = false;
    protected Thread connectionStateMonitoringThread;
    protected NetworkConnectionDeviceState currentConnectionDeviceState;
    protected Object currentConnectionDeviceStateLock = new Object();

    protected HashMap<Integer, Error> errorDictionary = new HashMap<Integer, Error>();
    //<Key: Device Specific Code, Value: Error Info>

    protected IHardware hardware;
    protected boolean initialized;
    protected volatile boolean monitorConnectionState = true;
    protected AtomicInteger monitoringCycleCount = new AtomicInteger();
    protected AtomicInteger needReconnect = new AtomicInteger(0); //need - 1, not need - 0
    protected String payboxDeviceTypeName = "";
    protected AtomicInteger requestedState = new AtomicInteger(NetworkConnectionState.Uninitialized.getValue());

    protected int stateMonitoringIntervalInMiliSeconds;
    protected String typeName = "";

    private int UsagePriority;

    public final int getUsagePriority() {
        return UsagePriority;
    }

    protected final void setUsagePriority(int value) {
        UsagePriority = value;
    }

    private String InternetConnection;

    public final String getInternetConnection() {
        return InternetConnection;
    }

    protected final void setInternetConnection(String value) {
        InternetConnection = value;
    }

    private String ProcessingAddress;

    public final String getProcessingAddress() {
        return ProcessingAddress;
    }

    protected final void setProcessingAddress(String value) {
        ProcessingAddress = value;
    }

    private String AppServerAddress;

    public final String getAppServerAddress() {
        return AppServerAddress;
    }

    protected final void setAppServerAddress(String value) {
        AppServerAddress = value;
    }

    private boolean IsActive;

    public final boolean getIsActive() {
        return IsActive;
    }

    public final void setIsActive(boolean value) {
        IsActive = value;
    }

    public abstract void Dispose();

    protected static native boolean InternetGetConnectedState(RefObject<Integer> lpSFlags, int dwReserved);

    static {
        System.loadLibrary("wininet.dll");
    }

    public final boolean Connect() {
        hardware.Log(typeName, "Connect(): requested...", LogType.Information);
        if (needReconnect.get() == 0l) {
            requestedState.set(NetworkConnectionState.Connected.getValue());
            return true;
        }
        return false;

    }

    public final boolean Disconnect() {
        hardware.Log(typeName, "Disconnect(): requested... ", LogType.Information);
        if (needReconnect.get() == 0l) {
            requestedState.set(NetworkConnectionState.Disconnected.getValue());
            return true;
        }
        return false;
    }

    public final boolean Reconnect() {
        hardware.Log(typeName, "Reconnect(): requested... ", LogType.Information);
        if (needReconnect.get() == 0l) {
            needReconnect.set(1);
            requestedState.set(NetworkConnectionState.Connected.getValue());
            return true;
        }
        return false;
    }

    protected abstract boolean ConnectSync();

    protected abstract boolean DisconnectSync();

    protected final void ConnectionStateMonitoring()  {
        while (monitorConnectionState) {
            NetworkConnectionDeviceState stateArgs = (NetworkConnectionDeviceState) GetStateForced();

            synchronized (currentConnectionDeviceStateLock) {
                if (!stateArgs.equals(currentConnectionDeviceState)) {
                    OnStateChanged(null);

                    currentConnectionDeviceState = stateArgs;
                }
            }


            NetworkConnectionState requestedStateTemp = NetworkConnectionState.forValue(requestedState.get());
            boolean needReconnectTemp = needReconnect.get() == 1 ? true : false;

            if (!requestedStateTemp.equals(NetworkConnectionState.Uninitialized) && (!requestedStateTemp.equals(stateArgs.getNetworkConnectionState()) || needReconnectTemp)) {
                if ((requestedStateTemp.equals(NetworkConnectionState.Connected) && !needReconnectTemp)

                        || (needReconnectTemp && !stateArgs.getNetworkConnectionState().equals(NetworkConnectionState.Connected))) {
                    hardware.Log(typeName, "ConnectionStateMonitoring(): Start Connect Sync", LogType.Information);
                    ConnectSync();
                    needReconnect.set(0);
                } else if (requestedStateTemp == NetworkConnectionState.Disconnected && !needReconnectTemp || needReconnectTemp && stateArgs.getNetworkConnectionState() != NetworkConnectionState.Disconnected) {
                    hardware.Log(typeName, "ConnectionStateMonitoring(): Start Disconnect Sync", LogType.Information);
                    DisconnectSync();
                }
            }
            monitoringCycleCount.incrementAndGet();

            try {
                Thread.sleep(stateMonitoringIntervalInMiliSeconds);
            } catch (Exception e) {
            }
        }
    }


    public boolean Initialize() {
        try {
            if (configIsValid && !initialized) {
                synchronized (currentConnectionDeviceStateLock) {
                    currentConnectionDeviceState = new NetworkConnectionDeviceState(this.getClass(), payboxDeviceTypeName, new HashMap<>(), NetworkConnectionState.Uninitialized, getInternetConnection(), getProcessingAddress(), getAppServerAddress(), getIsActive());
                }

                monitorConnectionState = true;
                connectionStateMonitoringThread = new Thread(() -> ConnectionStateMonitoring());
                connectionStateMonitoringThread.start();

                initialized = true;
            }

            return initialized;
        } catch (Exception ex) {
            hardware.Log(typeName, "Initialize()" + ex.getMessage(), LogType.Error);
            hardware.LogException(ex);
            return false;
        }
    }

    public boolean Deinitialize() {
        if (initialized) {
            monitorConnectionState = false;

            while (connectionStateMonitoringThread.isAlive()) ;

            synchronized (currentConnectionDeviceStateLock) {
                currentConnectionDeviceState = new NetworkConnectionDeviceState(this.getClass(), payboxDeviceTypeName, new HashMap<Integer, Error>(), NetworkConnectionState.Uninitialized, getInternetConnection(), getProcessingAddress(), getAppServerAddress(), getIsActive());
            }

            try {
                Dispose();
            } catch (RuntimeException ex) {
                hardware.LogException(ex);
            }

            initialized = false;
        }

        return true;
    }

    public boolean Reset() {
        if (Deinitialize()) {
            return Initialize();
        }
        return false;
    }

    public final DeviceState GetState(boolean force) {
        //if (force)
        //{
        //    int countTemp = this.monitoringCycleCount;

        //    while (this.monitoringCycleCount == countTemp && this.connectionStateMonitoringThread != null && this.connectionStateMonitoringThread.IsAlive)
        //    {
        //        Thread.Sleep(1000);
        //    }
        //}

        synchronized (currentConnectionDeviceStateLock) {
            return currentConnectionDeviceState;
        }
    }

    protected DeviceState GetStateForced() {
        HashMap<Integer, Error> errors = new HashMap<Integer, Error>();
        Error errorTemp = null;
        NetworkConnectionState networkConnectionState = NetworkConnectionState.Disconnected;

        try {
            int lngFlags = 0x20;

            RefObject<Integer> tempRef_lngFlags = new RefObject<Integer>(lngFlags);
            if (InternetGetConnectedState(tempRef_lngFlags, 0)) {
                lngFlags = tempRef_lngFlags.argValue;
                errorTemp = errorDictionary.get(0); //Connected Error Code
                networkConnectionState = NetworkConnectionState.Connected;
            } else {
                lngFlags = tempRef_lngFlags.argValue;
                errorTemp = errorDictionary.get(1); //Disconnected Error Code
                networkConnectionState = NetworkConnectionState.Disconnected;
            }
        } catch (RuntimeException ex) {
            errorTemp = errorDictionary.get(-1); //Default Error Code
            networkConnectionState = NetworkConnectionState.Disconnected;

            hardware.Log(typeName, "GetStateForced()" + ex.getMessage(), LogType.Error);
            hardware.LogException(ex);
        }

        errors.put(errorTemp.getGeneralCode(), errorTemp);
        return new NetworkConnectionDeviceState(this.getClass(), payboxDeviceTypeName, errors, networkConnectionState, getInternetConnection(), getProcessingAddress(), getAppServerAddress(), getIsActive());
    }

    private EventManager<DeviceState> stateChangedEventHandler = new EventManager<>();


    public final void OnStateChanged(DeviceChangedEvent event) {
        if (stateChangedEventHandler != null) {
            stateChangedEventHandler.invoke(event);
        }
    }

    public final void SoftwareActionToDeviceHandler(Object sender, SoftwareActionToDevice actionArgs) {
    }

    public final HashMap<String, Integer> GetDeviceCountersDictionary() {
        return null;
    }


}