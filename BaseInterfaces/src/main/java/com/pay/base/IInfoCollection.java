package com.pay.base;

/**
 * Created by dito on 4/5/2017.
 */
public interface IInfoCollection
{
    IServerContainer getContainer();
    void setContainer(IServerContainer value);
    void Initialize();

    void CollectPaymentInfo(PaymentInfo pInfo);

    void CollectAdvertisementInfo(AdvertisementClick advClick);
}