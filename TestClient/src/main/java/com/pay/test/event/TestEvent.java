package com.pay.test.event;

import com.pay.base.devicestate.DeviceState;
import com.pay.base.event.EventManager;
import com.pay.base.event.types.Delegates;
import com.pay.base.event.types.DeviceChangedEvent;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

import java.net.URL;
import java.util.Date;

/**
 * Created by dito on 4/12/2017.
 */
public class TestEvent extends Application {

    private EventManager<DeviceState> stateChangedEventHandler = Delegates.getInstance().getStateChangedEventHandler();

    private boolean inited = false;


    private void initEvents() {
        if (inited)
            return;
        stateChangedEventHandler.registerListener(this::OnStateChangedFirst);
        stateChangedEventHandler.registerListener(this::OnStateChangedSecond);
        inited = true;
    }

    private void OnStateChangedFirst(DeviceChangedEvent event) {
        taFirst.setText(taFirst.getText() + "\n" + "Done " + Thread.currentThread().getId() + " " + (System.currentTimeMillis() - ((Date) event.getSender()).getTime()));
        int val = 25000;
        String valStr = "fff";
        for (int i = 0; i < val; i++) {
            valStr += i;
        }
    }

    private void OnStateChangedSecond(DeviceChangedEvent event) {
        taSecond.setText(taSecond.getText() + "\n" + "Done " + Thread.currentThread().getId() + " " + (System.currentTimeMillis() - ((Date) event.getSender()).getTime()));
    }

    @FXML
    private TextArea taFirst;
    @FXML
    private TextArea taSecond;
    @FXML
    private Button btnSame;
    @FXML
    private Button btnBegin;


    @FXML
    private void runInSameThread(ActionEvent event) {
        initEvents();
        btnSame.setDisable(true);
        stateChangedEventHandler.invoke(new DeviceChangedEvent(new Date(), null));
        btnSame.setDisable(false);
    }

    @FXML
    private void runInOtherThread(ActionEvent event) {
        initEvents();
        btnBegin.setDisable(true);
        stateChangedEventHandler.beginInvoke(new DeviceChangedEvent(new Date(), null));
        btnBegin.setDisable(false);
    }

    public static void main(String[] args) {
        Application.launch(args);
    }


    public static FXMLLoader getLoader(String resource) throws Exception {
        URL url = ClassLoader.getSystemClassLoader().getResource(resource);
        FXMLLoader loader = new FXMLLoader(url);
        return loader;
    }

    @Override
    public void start(Stage stage) throws Exception {
        // Create the FXMLLoader
        FXMLLoader loader = getLoader("forms/EventTest.fxml");

        Parent root = loader.load();

        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.show();

    }
}
